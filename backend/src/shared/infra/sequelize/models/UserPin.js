import Sequelize, { Model } from 'sequelize'

class UserPin extends Model {
  static init(sequelize) {
    super.init(
      {
        userId: Sequelize.BIGINT,
        pinId: Sequelize.BIGINT,
      },
      { sequelize, tableName: 'users_pins' }
    )

    return this
  }

  static associate(models) {
    this.belongsTo(models.Pin, {
      foreignKey: 'pinId',
      as: 'pin',
    })
  }
}
export default UserPin
