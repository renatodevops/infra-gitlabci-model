import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'
import CreateUserService from '../../signUp/services/CreateUserService'

import CreateVoucherService from './CreateVoucherService'

import ActivateVoucherService from './ActivateVoucherService'
import { APPLIED, VOUCHER } from '../../../util/cardsIdentifiers'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'

let user

const createUser = async (indicatorId = 1) =>
  CreateUserService.execute({
    name: 'parsedName',
    phone: '(84) 99999-9999',
    document: 'parsedDocument',
    email: `${crypto.randomBytes(6).toString('hex')}@${crypto
      .randomBytes(6)
      .toString('hex')}.com`,
    born: new Date(),
    terms: true,
    indicatorId,
    password: '12345678',
    selfieLink: 'selfieLink',
    documentFrontLink: 'selfieLink',
    documentBackLink: 'selfieLink',
  })

describe('ActivateVoucherService tests', () => {
  beforeAll(async () => {
    user = await createUser()
  })
  it('should activate a voucher to a user', async () => {
    const { vouchers } = await CreateVoucherService.execute({
      leaderId: user.id,
      quantity: 1,
      value: 100,
    })
    const [voucher] = vouchers

    const investor = await createUser()

    const result = await ActivateVoucherService.handle({
      data: {
        leaderId: user.id,
        userEmail: investor.email,
        voucherId: voucher.id,
      },
    })

    expect(result).toHaveProperty('value', voucher.value)
    expect(result).toHaveProperty('userId', investor.id)
    expect(result).toHaveProperty('cardIdentifier', VOUCHER)
  })
  it('should not be able to activate a voucher twice', async () => {
    const { vouchers } = await CreateVoucherService.execute({
      leaderId: user.id,
      quantity: 1,
      value: 100,
    })
    const [voucher] = vouchers

    const investor = await createUser()

    const result = await ActivateVoucherService.handle({
      data: {
        leaderId: user.id,
        userEmail: investor.email,
        voucherId: voucher.id,
      },
    })

    expect(result).toHaveProperty('value', voucher.value)
    expect(result).toHaveProperty('userId', investor.id)
    expect(result).toHaveProperty('cardIdentifier', VOUCHER)

    await expect(
      ActivateVoucherService.handle({
        data: {
          leaderId: user.id,
          userEmail: investor.email,
          voucherId: voucher.id,
        },
      })
    ).rejects.toBeInstanceOf(Error)
  })

  it('should not be able to activate a voucher to itself', async () => {
    const { vouchers } = await CreateVoucherService.execute({
      leaderId: user.id,
      quantity: 1,
      value: 100,
    })
    const [voucher] = vouchers
    await expect(
      ActivateVoucherService.handle({
        data: {
          leaderId: user.id,
          userEmail: user.email,
          voucherId: voucher.id,
        },
      })
    ).rejects.toBeInstanceOf(Error)
  })

  it('should not be able to activate a a user with applied balance', async () => {
    const { vouchers } = await CreateVoucherService.execute({
      leaderId: user.id,
      quantity: 1,
      value: 100,
    })
    const [voucher] = vouchers

    const investor = await createUser()

    await CreateCardTransactionService.execute({
      cardIdentifier: APPLIED,
      userId: investor.id,
      status: CONFIRMED,
      description: 'teste',
      value: 1.0,
    })

    await expect(
      ActivateVoucherService.handle({
        data: {
          leaderId: user.id,
          userEmail: investor.email,
          voucherId: voucher.id,
        },
      })
    ).rejects.toBeInstanceOf(Error)
  })
})
