module.exports = {
  up: async (queryInterface, DataTypes) => {
    return queryInterface.createTable('users_documents', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      user_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      document_link: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      document_type: {
        allowNull: false,
        type: DataTypes.ENUM(
          'SELFIE',
          'DOCUMENT FRONT',
          'DOCUMENT BACK',
          'ADDRESS PROOF'
        ),
        defaultValue: 'SELFIE',
      },
      evaluated: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      approved: {
        allowNull: true,
        type: DataTypes.BOOLEAN,
      },
      message: {
        allowNull: true,
        type: DataTypes.STRING,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('anunual_plan_rate')
  },
}
