module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'types_updates',
      [
        {
          // 1
          name: 'solicitation_creation_financial_password',
          description: 'solicitation for creation',
          active: true,
        },
        {
          // 2
          name: 'solicitation_recovery_financial_password',
          description: 'solicitation for recovery',
          active: true,
        },
        {
          // 3
          name: 'solicitation_recovery_password',
          description: 'solicitation for recovery',
          active: true,
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('types_updates', null, {})
  },
}
