import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'
import CreateUserService from '../../signUp/services/CreateUserService'
import Bitcoin from '../../../shared/providers/BtcProvider/bitcoinCore'
import CreateUserBtcWithdrawalService from './CreateUserBtcWithdrawalService'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import { AVAILABLE } from '../../../util/cardsIdentifiers'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import UserFinancialInfo from '../../../shared/infra/sequelize/models/UserFinancialInfo'
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'
import BtcTransaction from '../../../shared/infra/sequelize/models/BtcTransaction'
import { AGUARDANDO_PROCESSAMENTO } from '../../../util/btcTransactionsStatus'

let user

const testWallet = new Bitcoin('test.dat')

const createUser = async (indicatorId = 1) =>
  CreateUserService.execute({
    name: 'parsedName',
    phone: '(84) 99999-9999',
    document: 'parsedDocument',
    email: `${crypto.randomBytes(6).toString('hex')}@${crypto
      .randomBytes(6)
      .toString('hex')}.com`,
    born: new Date(),
    terms: true,
    indicatorId,
    password: '12345678',
    selfieLink: 'selfieLink',
    documentFrontLink: 'selfieLink',
    documentBackLink: 'selfieLink',
  })

describe('CreateUserBtcWithdrawalService tests', () => {
  beforeEach(async () => {
    user = await createUser()
    await testWallet.generate()
  })
  it('should return an error Fundos Insuficientes', async () => {
    const result = await CreateUserBtcWithdrawalService.execute({
      userId: user.id,
      withdrawalValue: 100,
    })
    expect(result).toBe('Fundos insuficientes')
  })
  it('should return an error O valor mínimo de retirada é LKT 100', async () => {
    await CreateCardTransactionService.execute({
      userId: user.id,
      cardIdentifier: AVAILABLE,
      value: 100,
      status: CONFIRMED,
      description: 'teste',
    })
    const result = await CreateUserBtcWithdrawalService.execute({
      userId: user.id,
      withdrawalValue: 99,
    })
    expect(result).toBe('O valor mínimo de retirada é LKT 100')
  })
  it('should return an error Cadastre seu endereço BTC na página de perfil', async () => {
    await CreateCardTransactionService.execute({
      userId: user.id,
      cardIdentifier: AVAILABLE,
      value: 100,
      status: CONFIRMED,
      description: 'teste',
    })
    const result = await CreateUserBtcWithdrawalService.execute({
      userId: user.id,
      withdrawalValue: 100,
    })
    expect(result).toBe('Cadastre seu endereço BTC na página de perfil')
  })
  it('should return Sua solicitação de retirada está aguardando processamento, o tempo estimado de pagamento é de 48 horas!', async () => {
    await CreateCardTransactionService.execute({
      userId: user.id,
      cardIdentifier: AVAILABLE,
      value: 100,
      status: CONFIRMED,
      description: 'teste',
    })
    await CreateCardTransactionService.execute({
      userId: 1,
      cardIdentifier: AVAILABLE,
      value: 10000,
      status: CONFIRMED,
      description: 'teste',
    })
    await UserFinancialInfo.create({
      value: 'btc-address',
      userId: user.id,
      typeId: 2,
    })
    const result = await CreateUserBtcWithdrawalService.execute({
      userId: user.id,
      withdrawalValue: 100,
    })
    expect(result).toBe(
      'Sua solicitação de retirada está aguardando processamento, o tempo estimado de pagamento é de 48 horas!'
    )

    const userAvailableBalance = await GetUserCardBalanceService.execute({
      userId: user.id,
      cardIdentifier: AVAILABLE,
    })
    expect(userAvailableBalance).toBe(0)

    const solicitation = await BtcTransaction.findOne({
      where: { userId: user.id },
    })
    expect(solicitation).toHaveProperty('status', AGUARDANDO_PROCESSAMENTO)
    expect(solicitation).toHaveProperty('category', 'send')
    expect(solicitation).toHaveProperty('value', '97.50')
  })
})
