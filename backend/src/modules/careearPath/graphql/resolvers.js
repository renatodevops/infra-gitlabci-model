import { addMonths, endOfMonth, startOfMonth } from 'date-fns'
import { Op } from 'sequelize'
import {
  authenticated,
  compose,
  hasAllowed,
} from '../../../shared/infra/http/composers/composables'
import CardTransaction from '../../../shared/infra/sequelize/models/CardTransaction'
import { CONFIRMED } from '../../../util/btcTransactionsStatus'
import { AVAILABLE } from '../../../util/cardsIdentifiers'
import convertUTCtoLocalTZ from '../../../util/convertUTCtoLOCALTZ'
import GetSystemInputsByMonthService from '../services/GetSystemInputsByMonthService'

export default {
  Query: {
    getCareearPlanMonthlyTransactions: compose(
      authenticated,
      hasAllowed([5])
    )(async (_, { month, year }) => {
      const monthDate = addMonths(new Date(year, month, 2), 1)

      const node = await CardTransaction.findAll({
        where: {
          cardIdentifier: [AVAILABLE],
          status: CONFIRMED,
          description: {
            [Op.or]: [
              { [Op.substring]: 'Bônus Supervisor' },
              { [Op.substring]: 'Bônus Gerente' },
            ],
          },
          createdAt: {
            [Op.and]: [
              { [Op.gte]: convertUTCtoLocalTZ(startOfMonth(monthDate)) },
              { [Op.lt]: convertUTCtoLocalTZ(endOfMonth(monthDate)) },
            ],
          },
        },
      })

      return {
        edge: {
          node,
        },
        pageInfo: {
          totalCount: 0,
          hasPrevPage: false,
          hasNextPage: true,
        },
      }
    }),
    getCareearPlanMonthlyValues: compose(
      authenticated,
      hasAllowed([5])
    )(async (_, { month, year }) => {
      const monthDate = new Date(year, month, 2)

      const systemInputs = await GetSystemInputsByMonthService.execute(
        monthDate
      )

      return {
        systemInputs,
        supervisorsBonus: systemInputs * 0.005,
        managersBonus: systemInputs * 0.015,
      }
    }),
  },
}
