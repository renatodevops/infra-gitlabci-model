import Sequelize, { Model } from 'sequelize'

class GroupTypeTransaction extends Model {
  static init(sequelize) {
    super.init(
      {
        groupId: {
          type: Sequelize.BIGINT,
          primaryKey: true,
        },
        typeId: {
          type: Sequelize.BIGINT,
          primaryKey: true,
        },
      },
      { sequelize, tableName: 'groups_types_transactions' }
    )
    return this
  }

  static associate(models) {
    this.belongsTo(models.TypeTransaction, {
      foreignKey: 'type_id',
      as: 'type',
    })
  }
}
export default GroupTypeTransaction
