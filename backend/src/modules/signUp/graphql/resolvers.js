import Country from '../../../shared/infra/sequelize/models/Country'
import User from '../../../shared/infra/sequelize/models/User'
import Role from '../../../shared/infra/sequelize/models/Role'

import { cacheProvider } from '../../../shared/container'
import GetValidIndicatorService from '../services/GetValidIndicatorService'
import CreateSignUpTokenService from '../services/CreateSignUpTokenService'
import CreateUserService from '../services/CreateUserService'
import CreateUserAddressService from '../services/CreateUserAddressService'
// import UploadFileService from '../../../shared/providers/StorageProvider/UploadFileService'
import CreateUserUnilevelNodeService from '../services/CreateUserUnilevelNodeService'

export default {
  UserRole: {
    role: obj => Role.findByPk(obj.roleId),
  },

  Query: {
    getCountries: () => Country.findAll(),

    GetValidIndicator: async (_, { indicatorCode }) => {
      const indicator = await GetValidIndicatorService.execute(indicatorCode)
      return indicator
    },

    GetSignInDataByEmailToken: async (_, { token }) => {
      const data = await cacheProvider.recover(`signup:${token}`)
      if (!data) throw new Error('Link de criação de conta inválido')
      const { name, indicatorId, email } = data
      const indicator = await User.findByPk(indicatorId)
      return { name, indicator, email }
    },
  },

  Mutation: {
    CreateUser: async (root, args) => {
      const {
        phone,
        born,
        document,

        countryId,
        state,
        city,
        neighborhood,
        street,
        number,
        zipCode,

        terms,
        banana,
        indicatorId,

        email,
        name,
        token,
      } = args

      // const [
      //   selfieLink,
      //   documentFrontLink,
      //   documentBackLink,
      //   addressProofLink,
      // ] = await Promise.all([
      //   UploadFileService.execute(selfie),
      //   UploadFileService.execute(documentFront),
      //   UploadFileService.execute(documentBack),
      //   UploadFileService.execute(addressProof),
      // ])

      const user = await CreateUserService.execute({
        phone,
        born,
        document,
        email,
        name,
        terms,
        password: banana,
        indicatorId,
      })

      await CreateUserAddressService.execute({
        userId: user.id,
        countryId,
        state,
        city,
        neighborhood,
        street,
        number,
        zipCode,
      })

      await CreateUserUnilevelNodeService.execute({
        userId: user.id,
        indicatorId,
      })

      await cacheProvider.invalidate(`signup:${token}`)

      return 'Conta criada com sucesso'
    },

    CreateSignUpToken: async (_, { name, indicatorId, email }) => {
      return CreateSignUpTokenService.execute({
        name,
        indicatorId,
        email,
      })
    },
  },
}
