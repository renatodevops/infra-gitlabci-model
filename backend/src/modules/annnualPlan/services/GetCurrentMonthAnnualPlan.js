import AnnualPlanRate from '../../../shared/infra/sequelize/models/AnnualPlanRate'
import convertUTCtoLocalTZ from '../../../util/convertUTCtoLOCALTZ'

class GetCurrentMonthAnnualPlan {
  async execute() {
    const now = convertUTCtoLocalTZ()

    let currentMonthRate = await AnnualPlanRate.findOne({
      where: {
        year: now.getUTCFullYear(),
        month: now.getUTCMonth(),
      },
    })

    if (!currentMonthRate)
      currentMonthRate = await AnnualPlanRate.create({
        year: now.getUTCFullYear(),
        month: now.getUTCMonth(),
        rate: 0.055,
      })

    return currentMonthRate
  }
}
export default new GetCurrentMonthAnnualPlan()
