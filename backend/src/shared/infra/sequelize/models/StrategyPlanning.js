import Sequelize, { Model, Op } from 'sequelize'

class StrategyPlanning extends Model {
  static init(sequelize) {
    super.init(
      {
        strategyId: {
          type: Sequelize.BIGINT,
          unique: 'uniqueStrategyPlan',
        },
        date: {
          type: Sequelize.DATEONLY,
          unique: 'uniqueStrategyPlan',
        },
        monthlyPlanned: {
          type: Sequelize.DECIMAL,
        },
        dailyPlanned: {
          type: Sequelize.DECIMAL,
        },
        isEditable: {
          type: Sequelize.BOOLEAN,
        },
        confirmed: {
          type: Sequelize.BOOLEAN,
        },
      },
      { sequelize, tableName: 'strategies_planning' }
    )
    return this
  }

  static associate(models) {
    this.belongsTo(models.Strategy, {
      foreignKey: 'strategyId',
      as: 'strategy',
    })
  }

  static async createForStrategyId({
    startDate,
    strategyId = 1,
    planned = 0.0,
  }) {
    // const MONDAY = 1
    // const FRIDAY = 5

    // GET THE MONTH AND YEAR OF THE SELECTED DATE.
    const month = startDate.getUTCMonth()
    const year = startDate.getUTCFullYear()

    // GET THE FIRST AND LAST DATE OF THE MONTH.
    const firstDayOfMonth = new Date(year, month, 1)
    const lastDayOfMonth = new Date(year, month + 1, 0)

    const workingDays = Array.from(
      Array(lastDayOfMonth.getUTCDate()),
      (__, i) => {
        const nextDate = new Date(firstDayOfMonth)
        return new Date(nextDate.setUTCDate(nextDate.getUTCDate() + i))
      }
    )
    // .filter(date => date.getUTCDay() >= MONDAY && date.getUTCDay() <= FRIDAY)

    const promisses = workingDays.map(async date => {
      const isEditable = date.setUTCHours(20, 0, 0, 0) >= new Date()
      return StrategyPlanning.create({
        strategyId,
        monthlyPlanned: 0.0,
        dailyPlanned: planned,
        isEditable,
        date,
        confirmed: false,
      })
    })
    return Promise.all(promisses)
  }

  static async getPlannings({ strategyId = 1, month, year }) {
    const startDate = new Date(year, month - 1, 1)
    const finalDate = new Date(year, month, 1)
    const where = [
      {
        date: { [Op.gte]: startDate },
      },
      {
        date: {
          [Op.lt]: finalDate,
        },
      },
      {
        strategyId,
      },
    ]
    const plannings = await StrategyPlanning.findAll({
      order: [['date', 'ASC']],
      where: {
        [Op.and]: where,
      },
    })

    return plannings.length > 0
      ? plannings
      : StrategyPlanning.createForStrategyId({
          startDate,
          strategyId,
        })
  }
}
export default StrategyPlanning
