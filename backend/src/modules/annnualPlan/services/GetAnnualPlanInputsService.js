import { subYears, startOfMonth } from 'date-fns'
import { Op } from 'sequelize'

import CardTransaction from '../../../shared/infra/sequelize/models/CardTransaction'
import { ANNUAL_PLAN } from '../../../util/cardsIdentifiers'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import convertUTCtoLocalTZ from '../../../util/convertUTCtoLOCALTZ'

class GetAnnualPlanInputsService {
  async execute({ userId, now = convertUTCtoLocalTZ(new Date()) }) {
    const [lastClosed] = await CardTransaction.findAll({
      where: {
        cardIdentifier: ANNUAL_PLAN,
        status: CONFIRMED,
        userId,
        value: { [Op.lt]: 0.0 },
      },
      order: [['id', 'desc']],
    })

    const lastClosedDate = lastClosed
      ? lastClosed.createdAt
      : new Date('01-01-2019')

    const cicleInit = subYears(now, 1)
    const startCurrentMonth = startOfMonth(now)

    return CardTransaction.findAll({
      where: {
        cardIdentifier: ANNUAL_PLAN,
        userId,
        status: CONFIRMED,
        createdAt: {
          [Op.and]: [
            { [Op.gt]: lastClosedDate },
            { [Op.gt]: cicleInit },
            { [Op.lt]: startCurrentMonth },
          ],
        },
      },
    })
  }
}
export default new GetAnnualPlanInputsService()
