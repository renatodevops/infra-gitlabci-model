import '../../../shared/infra/sequelize/index'
import crypto from 'crypto'

import User from '../../../shared/infra/sequelize/models/User'

import GetUserAutoRenovationSetting from './GetUserAutoRenovationSetting'

let user

describe('GetUserAutoRenovationSetting tests', () => {
  beforeEach(async () => {
    user = await User.create({
      name: 'parsedName',
      phone: 'parsedPhone',
      document: 'parsedDocument',
      email: `${crypto.randomBytes(6).toString('hex')}@${crypto
        .randomBytes(6)
        .toString('hex')}.com`,
      born: new Date(),
      terms: true,
      indicatorId: 1,
      passwordHash: crypto.randomBytes(6).toString('hex'),
      selfieLink: 'selfieLink',
      documentFrontLink: 'selfieLink',
      documentBackLink: 'selfieLink',
    })
  })
  it('should return false for 2 consecutive calls', async () => {
    let result = await GetUserAutoRenovationSetting.execute({
      userId: user.id,
    })
    expect(result).toBe(false)

    result = await GetUserAutoRenovationSetting.execute({
      userId: user.id,
    })
    expect(result).toBe(false)
  })
})
