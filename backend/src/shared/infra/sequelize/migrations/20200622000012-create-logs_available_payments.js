module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('logs_available_payments', {
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      user_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      applied_input: {
        allowNull: false,
        type: DataTypes.DECIMAL(10, 2),
      },
      available_input: {
        allowNull: false,
        type: DataTypes.DECIMAL(10, 2),
      },
      limit: {
        allowNull: false,
        type: DataTypes.DECIMAL(10, 2),
      },
      capacity: {
        allowNull: false,
        type: DataTypes.DECIMAL(10, 2),
      },
      bonus_value: {
        allowNull: false,
        type: DataTypes.DECIMAL(10, 2),
      },
      paied_value: {
        allowNull: false,
        type: DataTypes.DECIMAL(10, 2),
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('logs_available_payments')
  },
}
