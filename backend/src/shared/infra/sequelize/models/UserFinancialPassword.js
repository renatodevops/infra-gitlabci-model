import Sequelize, { Model } from 'sequelize'

class UserFinancialPassword extends Model {
  static init(sequelize) {
    super.init(
      {
        userId: {
          type: Sequelize.BIGINT,
          primaryKey: true,
        },
        financialPassword: Sequelize.STRING,
      },
      { sequelize, tableName: 'users_financial_passwords' }
    )
    return this
  }
}
export default UserFinancialPassword
