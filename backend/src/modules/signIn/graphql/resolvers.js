import User from '../../../shared/infra/sequelize/models/User'
import UserRole from '../../../shared/infra/sequelize/models/UserRole'
import CreateUserSessionService from '../services/CreateUserSessionService'

export default {
  Mutation: {
    CreateUserSession: async (_, { email, banana }) => {
      const result = await CreateUserSessionService.execute({
        email,
        password: banana,
      })

      return result
    },
    LoginAdmin: async (root, { email, password }) => {
      const user = await User.findOne({ where: { email } })
      if (!user) throw Error('Usuário ou senha incorretos.')
      const roles = await UserRole.findOne({
        where: { userId: user.id, roleId: 1, active: true },
      })
      if (roles) throw Error('Acesso não permitido')

      const result = await CreateUserSessionService.execute({
        email,
        password,
      })

      return result
    },
  },
}
