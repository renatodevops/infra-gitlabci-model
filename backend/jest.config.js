module.exports = {
  bail: true,

  clearMocks: true,

  collectCoverage: false,

  collectCoverageFrom: ['src/app/**'],

  coverageDirectory: '__tests__/coverage',

  coveragePathIgnorePatterns: ['/node_modules/'],

  testEnvironment: 'node',

  testMatch: ['**/src/modules/**/services/*.spec.js'],

  transform: {
    '.(js|jsx|ts|tsx)': '@sucrase/jest-plugin',
  },
}
