// import { cacheProvider } from '../../../shared/container'
import CardTransaction from '../../../shared/infra/sequelize/models/CardTransaction'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'

class GetUserCardBalanceService {
  async execute({ cardIdentifier, userId }) {
    // const key = `balance_card:${cardIdentifier}-${userId}`

    // let value = await cacheProvider.recover(key)
    let value = null

    if (!value) {
      value = await CardTransaction.sum('value', {
        where: {
          cardIdentifier,
          userId,
          status: CONFIRMED,
        },
      })
      // await cacheProvider.save(key, value)
    }
    return value
  }
}
export default new GetUserCardBalanceService()
