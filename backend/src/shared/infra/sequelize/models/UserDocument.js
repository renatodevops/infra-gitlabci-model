import Sequelize, { Model } from 'sequelize'

class UserDocument extends Model {
  static init(sequelize) {
    super.init(
      {
        userId: {
          type: Sequelize.BIGINT,
        },
        documentLink: {
          type: Sequelize.STRING,
        },
        documentType: {
          type: Sequelize.STRING,
          validate: {
            isIn: {
              args: [
                ['SELFIE', 'DOCUMENT FRONT', 'DOCUMENT BACK', 'ADDRESS PROOF'],
              ],
              msg: 'value must be some of the enums',
            },
          },
        },
        evaluated: {
          type: Sequelize.BOOLEAN,
        },
        approved: {
          type: Sequelize.BOOLEAN,
        },
        message: {
          type: Sequelize.STRING,
        },
      },
      { sequelize, tableName: 'users_documents' }
    )
    return this
  }

  static associate(models) {
    this.belongsTo(models.User)
  }
}
export default UserDocument
