import BtcTransaction from '../../../shared/infra/sequelize/models/BtcTransaction'

import { depositBtcWallet } from '../../../shared/container'

import {
  AGUARDANDO_PROCESSAMENTO,
  PROCESSADA,
} from '../../../util/btcTransactionsStatus'

class ProcessUsersBtcWithdrawalsService {
  get key() {
    return 'ProcessUsersBtcWithdrawalsService'
  }

  async handle({ data }) {
    const { transactionsIds } = data

    const transactions = await BtcTransaction.findAll({
      where: {
        id: transactionsIds,
        category: 'send',
        status: AGUARDANDO_PROCESSAMENTO,
      },
    })

    if (transactions.length === 0) return 'Transações não encontradas'

    const transactionsWithAmount = await Promise.all(
      transactions.map(async transaction => {
        const amount = await depositBtcWallet.ExchangeToBTC(
          Number(transaction.value),
          'USD'
        )
        await transaction.update({ amount })
        return {
          address: transaction.address,
          amount: Number(amount),
        }
      })
    )

    const addressValue = transactionsWithAmount.map(info => [
      info.address,
      info.amount,
    ])

    const { amounts, subtractFeeFrom } = addressValue.reduce(
      (acc, [address, value]) => {
        if (!acc.amounts[address]) acc.amounts[address] = 0.0
        acc.amounts[address] += value
        if (!acc.subtractFeeFrom.find(addr => addr === address))
          acc.subtractFeeFrom.push(address)
        return acc
      },
      {
        amounts: {},
        subtractFeeFrom: [],
      }
    )

    const serializedAmounts = Object.keys(amounts).reduce((result, key) => {
      result[key] = amounts[key].toFixed(8)
      return result
    }, {})

    try {
      const txid = await depositBtcWallet.sendMany({
        amounts: serializedAmounts,
        subtractFeeFrom,
      })
      await Promise.all(
        transactions.map(transaction =>
          transaction.update({ status: PROCESSADA, txid })
        )
      )
      return { txid, amounts: serializedAmounts, subtractFeeFrom }
    } catch (err) {
      console.log(err)
      return null
    }
  }
}
export default new ProcessUsersBtcWithdrawalsService()
