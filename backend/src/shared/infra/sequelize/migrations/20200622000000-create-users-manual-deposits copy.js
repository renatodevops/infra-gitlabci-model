module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('users_manual_deposits', {
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        allowNull: false,
      },
      user_id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      admin_id: {
        type: DataTypes.BIGINT,
        allowNull: true,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      file_link: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      user_comment: {
        allowNull: true,
        type: DataTypes.STRING,
      },
      declared_usd_value: {
        allowNull: false,
        type: DataTypes.DECIMAL(10, 2),
      },
      admin_answer: {
        allowNull: true,
        type: DataTypes.STRING,
      },
      fee: {
        allowNull: true,
        type: DataTypes.DECIMAL(10, 2),
      },
      net_usd_value: {
        allowNull: false,
        type: DataTypes.DECIMAL(10, 2),
      },
      status: {
        allowNull: false,
        type: DataTypes.STRING,
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('users_manual_deposits')
  },
}
