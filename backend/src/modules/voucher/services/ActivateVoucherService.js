import User from '../../../shared/infra/sequelize/models/User'
import Voucher from '../../../shared/infra/sequelize/models/Voucher'
import { CREATED, ACTIVATED } from '../../../util/VoucherStatus'
import { APPLIED, VOUCHER } from '../../../util/cardsIdentifiers'
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'

class ActivateVoucherService {
  get key() {
    return 'ActivateVoucherService'
  }

  async handle({ data: { voucherId, userEmail, leaderId } }) {
    const voucher = await Voucher.findOne({
      where: {
        leaderId,
        id: voucherId,
        activatedAt: null,
        status: CREATED,
      },
    })
    if (!voucher) throw new Error('Voucher não encontrado')

    const user = await User.findOne({ where: { email: userEmail } })
    if (!user) throw new Error('Usuário inválido')

    if (user.id === voucher.leaderId)
      throw new Error('não é possível ativar voucher para si mesmo')

    const alreadyHasVoucher = await Voucher.findOne({
      where: { userId: user.id },
    })
    if (alreadyHasVoucher) throw new Error('Usuário ja possui voucher ativado')

    const balance = await GetUserCardBalanceService.execute({
      cardIdentifier: APPLIED,
      userId: user.id,
    })
    if (balance > 0.0) throw new Error('Usuário ja possui investimento')

    await voucher.update({
      activatedAt: new Date(),
      userId: user.id,
      status: ACTIVATED,
    })

    return CreateCardTransactionService.execute({
      cardIdentifier: VOUCHER,
      userId: user.id,
      value: voucher.value,
      status: CONFIRMED,
      description: 'Ativação de voucher',
    })
  }
}
export default new ActivateVoucherService()
