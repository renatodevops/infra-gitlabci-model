import Sequelize, { Model } from 'sequelize'

class StatusTransaction extends Model {
  static init(sequelize) {
    super.init(
      {
        name: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'The name can not be empty',
            },
            len: {
              args: [1, 40],
              msg: 'The name field must contain less than 40 characters',
            },
          },
        },
        active: {
          type: Sequelize.BOOLEAN,
          validate: {
            notEmpty: {
              msg: 'The active field can not be empty',
            },
          },
        },
      },
      { sequelize, tableName: 'status_transactions' }
    )

    this.addHook('beforeSave', async ({ name }) => {
      const status = await StatusTransaction.findOne({ where: { name } })

      if (status) throw Error('Transaction status already exists')
    })

    return this
  }
}
export default StatusTransaction
