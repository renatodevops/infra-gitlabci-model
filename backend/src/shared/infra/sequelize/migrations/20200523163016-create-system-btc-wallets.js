module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('system_btc_wallets', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      name: {
        unique: true,
        allowNull: true,
        type: DataTypes.STRING(40),
      },
      label: {
        unique: true,
        allowNull: false,
        type: DataTypes.STRING(40),
      },
      is_operable: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: true,
      },
      active: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: true,
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('system_btc_wallets')
  },
}
