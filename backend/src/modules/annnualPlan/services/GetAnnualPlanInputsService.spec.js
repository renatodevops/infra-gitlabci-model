import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'
import { addDays, subDays } from 'date-fns'
import subYears from 'date-fns/subYears'
import truncate from '../../../../__tests__/utils/truncate'
import GetAnnualPlanInputsService from './GetAnnualPlanInputsService'
import User from '../../../shared/infra/sequelize/models/User'
import CardTransaction from '../../../shared/infra/sequelize/models/CardTransaction'
import { ANNUAL_PLAN } from '../../../util/cardsIdentifiers'
import { CONFIRMED } from '../../../util/btcTransactionsStatus'

let user
describe('GetAnnualPlanInputsService tests', () => {
  beforeEach(async () => {
    await truncate([CardTransaction])
    user = await User.create({
      name: 'parsedName',
      phone: 'parsedPhone',
      document: 'parsedDocument',
      email: `${crypto.randomBytes(6).toString('hex')}@${crypto
        .randomBytes(6)
        .toString('hex')}.com`,
      born: new Date(),
      terms: true,
      indicatorId: 1,
      passwordHash: crypto.randomBytes(6).toString('hex'),
      selfieLink: 'selfieLink',
      documentFrontLink: 'selfieLink',
      documentBackLink: 'selfieLink',
    })
  })
  it('should return an empty array', async () => {
    const result = await GetAnnualPlanInputsService.execute({ userId: user.id })
    expect(result).toHaveLength(0)
  })

  it('should return an empty array', async () => {
    await CardTransaction.create({
      userId: user.id,
      description: 'teste',
      value: 100,
      cardIdentifier: ANNUAL_PLAN,
      status: CONFIRMED,
      createdAt: subDays(subYears(new Date(), 1), 1),
    })
    const result = await GetAnnualPlanInputsService.execute({ userId: user.id })
    expect(result).toHaveLength(0)
    // expect(result[0]).toHaveProperty('userId', user.id)
  })

  it('should return an array with 1 transaction', async () => {
    await CardTransaction.create({
      userId: user.id,
      description: 'teste',
      value: 100,
      cardIdentifier: ANNUAL_PLAN,
      status: CONFIRMED,
      createdAt: addDays(subYears(new Date(), 1), 1),
    })
    const result = await GetAnnualPlanInputsService.execute({ userId: user.id })
    expect(result).toHaveLength(1)
    expect(result[0]).toHaveProperty('userId', user.id)
  })

  it('should return an array with 2 transaction', async () => {
    await CardTransaction.create({
      userId: user.id,
      description: 'teste',
      value: 100,
      cardIdentifier: ANNUAL_PLAN,
      status: CONFIRMED,
      createdAt: addDays(subYears(new Date(), 1), 1),
    })
    await CardTransaction.create({
      userId: user.id,
      description: 'teste',
      value: 100,
      cardIdentifier: ANNUAL_PLAN,
      status: CONFIRMED,
      createdAt: addDays(subYears(new Date(), 1), 30),
    })
    const result = await GetAnnualPlanInputsService.execute({ userId: user.id })
    expect(result).toHaveLength(2)
    expect(result[0]).toHaveProperty('userId', user.id)
    expect(result[1]).toHaveProperty('userId', user.id)
  })

  it('should return an array with 1 transaction', async () => {
    await CardTransaction.create({
      userId: user.id,
      description: 'teste',
      value: 100,
      cardIdentifier: ANNUAL_PLAN,
      status: CONFIRMED,
      createdAt: addDays(subYears(new Date(), 1), 30),
    })
    await CardTransaction.create({
      userId: user.id,
      description: 'teste',
      value: 100 * -1,
      cardIdentifier: ANNUAL_PLAN,
      status: CONFIRMED,
      createdAt: addDays(subYears(new Date(), 1), 60),
    })
    await CardTransaction.create({
      userId: user.id,
      description: 'teste',
      value: 200,
      cardIdentifier: ANNUAL_PLAN,
      status: CONFIRMED,
      createdAt: addDays(subYears(new Date(), 1), 90),
    })
    const result = await GetAnnualPlanInputsService.execute({ userId: user.id })
    expect(result).toHaveLength(1)
    expect(result[0]).toHaveProperty('userId', user.id)
    expect(result[0]).toHaveProperty('value', '200.00')
  })

  it('should return an array with 1 transaction', async () => {
    await CardTransaction.create({
      userId: user.id,
      description: 'teste',
      value: 100,
      cardIdentifier: ANNUAL_PLAN,
      status: CONFIRMED,
      createdAt: addDays(subYears(new Date(), 1), 30),
    })
    await CardTransaction.create({
      userId: user.id,
      description: 'teste',
      value: 100 * -1,
      cardIdentifier: ANNUAL_PLAN,
      status: CONFIRMED,
      createdAt: addDays(subYears(new Date(), 1), 60),
    })
    await CardTransaction.create({
      userId: user.id,
      description: 'teste',
      value: 200,
      cardIdentifier: ANNUAL_PLAN,
      status: CONFIRMED,
      createdAt: addDays(subYears(new Date(), 1), 90),
    })
    await CardTransaction.create({
      userId: user.id,
      description: 'teste',
      value: 200,
      cardIdentifier: ANNUAL_PLAN,
      status: CONFIRMED,
      createdAt: new Date(),
    })
    const result = await GetAnnualPlanInputsService.execute({ userId: user.id })
    expect(result).toHaveLength(1)
    expect(result[0]).toHaveProperty('userId', user.id)
    expect(result[0]).toHaveProperty('value', '200.00')
  })
})
