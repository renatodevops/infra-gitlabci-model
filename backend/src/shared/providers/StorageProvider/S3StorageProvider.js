import fs from 'fs'
import path from 'path'
import mime from 'mime'
import aws from 'aws-sdk'
import uploadConfig from '../../../config/upload'

class S3StorageProvider {
  constructor() {
    this.client = new aws.S3({
      region: uploadConfig.aws.region,
    })
  }

  async saveFile(file) {
    const originalPath = path.resolve(uploadConfig.tmpFolder, file)

    const ContentType = mime.getType(originalPath)

    if (!ContentType) throw new Error('File not found')

    const fileContent = await fs.promises.readFile(originalPath)

    await this.client
      .putObject({
        Bucket: uploadConfig.aws.bucket,
        Key: file,
        ACL: 'public-read',
        Body: fileContent,
        ContentType,
        ContentDisposition: `inline; filename=${file}`,
      })
      .promise()

    await fs.promises.unlink(originalPath)

    return `https://${uploadConfig.aws.bucket}.s3.${uploadConfig.aws.region}.amazonaws.com/${file}`
  }

  async deleteFile(file) {
    await this.client
      .deleteObject({
        Bucket: uploadConfig.aws.bucket,
        Key: file,
      })
      .promise()
  }
}
export default new S3StorageProvider()
