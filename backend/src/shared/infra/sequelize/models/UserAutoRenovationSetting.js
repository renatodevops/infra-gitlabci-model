import Sequelize, { Model } from 'sequelize'

class UserAutoRenovationSetting extends Model {
  static init(sequelize) {
    super.init(
      {
        userId: {
          type: Sequelize.BIGINT,
          primaryKey: true,
        },
        isActive: Sequelize.BOOLEAN,
      },
      { sequelize, tableName: 'users_auto_renovation_settings' }
    )
    return this
  }
}
export default UserAutoRenovationSetting
