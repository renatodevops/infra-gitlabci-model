module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('strategies', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      name: {
        unique: true,
        allowNull: true,
        type: DataTypes.STRING(40),
      },
      description: {
        unique: false,
        allowNull: false,
        type: DataTypes.STRING(80),
      },
      active: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: true,
      },
      type_income_transaction_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: 'types_transactions',
          key: 'id',
        },
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('strategies')
  },
}
