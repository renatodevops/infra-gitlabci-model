import Sequelize, { Model } from 'sequelize'

class SystemLog extends Model {
  static init(sequelize) {
    super.init(
      {
        userId: Sequelize.BIGINT,
        category: Sequelize.STRING,
        address: Sequelize.STRING,
        txid: Sequelize.STRING,
        amount: Sequelize.DECIMAL,
        message: Sequelize.STRING,
      },
      { sequelize, tableName: 'system_logs' }
    )
    return this
  }

  static associate(models) {
    this.belongsTo(models.Strategy, {
      foreignKey: 'userId',
      as: 'user',
    })
  }
}
export default SystemLog
