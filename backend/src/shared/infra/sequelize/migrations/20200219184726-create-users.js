module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      user_id: {
        allowNull: true,
        type: DataTypes.BIGINT,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      country_id: {
        allowNull: false,
        type: DataTypes.BIGINT,
        references: {
          model: 'countries',
          key: 'id',
        },
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      name: {
        allowNull: false,
        type: DataTypes.STRING(80),
      },
      phone: {
        allowNull: false,
        type: DataTypes.STRING(20),
      },
      username: {
        allowNull: false,
        type: DataTypes.STRING(24),
        unique: true,
      },
      user_path: {
        allowNull: true,
        type: DataTypes.STRING(5000),
      },
      document: {
        allowNull: true,
        type: DataTypes.STRING(200),
      },
      email: {
        allowNull: false,
        type: DataTypes.STRING(100),
        unique: true,
      },
      password_hash: {
        allowNull: false,
        type: DataTypes.STRING(60),
      },
      born: {
        allowNull: true,
        type: DataTypes.DATEONLY,
      },
      terms: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
      },
      facebook: {
        allowNull: true,
        type: DataTypes.BOOLEAN,
      },
      instagram: {
        allowNull: true,
        type: DataTypes.BOOLEAN,
      },
      twitter: {
        allowNull: true,
        type: DataTypes.BOOLEAN,
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('users')
  },
}
