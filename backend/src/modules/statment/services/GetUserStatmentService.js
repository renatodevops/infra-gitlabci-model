import Transaction from '../../../shared/infra/sequelize/models/Transaction'
import {
  TYPE_DEPOSIT_BTC,
  TYPE_CREDIT_OUT,
  TYPE_MANUAL_ACTIVATION,
  TYPE_APPLIED_IN,
  TYPE_APPLIED_OUT,
  TYPE_VOUCHER_ACTIVATION,
  TYPE_VOUCHER_CANCELLATION,
  TYPE_INCOME_IN,
  TYPE_INCOME_OUT,
  TYPE_RETURN_TO_INCOME,
  TYPE_BONUS_INDICATION_1_LEVEL,
  TYPE_BONUS_INDICATION_2_LEVEL,
  TYPE_BONUS_INDICATION_3_LEVEL,
  TYPE_BONUS_INDICATION_4_LEVEL,
  TYPE_BONUS_INDICATION_5_LEVEL,
  TYPE_BONUS_INDICATION_6_LEVEL,
  TYPE_BONUS_INDICATION_7_LEVEL,
  TYPE_BONUS_INDICATION_8_LEVEL,
  TYPE_BONUS_INDICATION_9_LEVEL,
  TYPE_BONUS_INDICATION_10_LEVEL,
  TYPE_PROFIT,
  TYPE_RESIDUAL_BONUS,
  TYPE_CARRER_PLAN,
  TYPE_WITHDRAWAL_BTC,
  TYPE_WITHDRAWAL_REAPLICATION,
  TYPE_RESIDUAL_BONUS_PAYMENT,
  TYPE_REAPLICATION_OUT,
  TYPE_REAPLICATION_IN,
  TYPE_TRANSFER_FROM_USER,
  TYPE_TRANSFER_TO_USER,
} from '../../../util/typesTransactions'

class GetUserStatmentService {
  async execute({ userId, limit, offset, order, cards }) {
    const cardsToTypesMap = {
      credit: [
        TYPE_DEPOSIT_BTC,
        TYPE_CREDIT_OUT,
        TYPE_MANUAL_ACTIVATION,
        TYPE_TRANSFER_FROM_USER,
      ],
      applied: [
        TYPE_APPLIED_IN,
        TYPE_APPLIED_OUT,
        TYPE_VOUCHER_ACTIVATION,
        TYPE_VOUCHER_CANCELLATION,
      ],
      income: [
        TYPE_INCOME_IN,
        TYPE_INCOME_OUT,
        TYPE_RETURN_TO_INCOME,
        TYPE_RESIDUAL_BONUS_PAYMENT,
      ],
      available: [
        TYPE_BONUS_INDICATION_1_LEVEL,
        TYPE_BONUS_INDICATION_2_LEVEL,
        TYPE_BONUS_INDICATION_3_LEVEL,
        TYPE_BONUS_INDICATION_4_LEVEL,
        TYPE_BONUS_INDICATION_5_LEVEL,
        TYPE_BONUS_INDICATION_6_LEVEL,
        TYPE_BONUS_INDICATION_7_LEVEL,
        TYPE_BONUS_INDICATION_8_LEVEL,
        TYPE_BONUS_INDICATION_9_LEVEL,
        TYPE_BONUS_INDICATION_10_LEVEL,
        TYPE_PROFIT,
        TYPE_RESIDUAL_BONUS,
        TYPE_CARRER_PLAN,
        TYPE_WITHDRAWAL_BTC,
        TYPE_WITHDRAWAL_REAPLICATION,
        TYPE_RETURN_TO_INCOME,
        TYPE_TRANSFER_TO_USER,
      ],
      reaplication: [TYPE_REAPLICATION_OUT, TYPE_REAPLICATION_IN],
      withdrawal: [TYPE_WITHDRAWAL_BTC],
      rents: [TYPE_APPLIED_IN],
      btcdeposit: [TYPE_DEPOSIT_BTC],
      user_transfer: [TYPE_TRANSFER_FROM_USER, TYPE_TRANSFER_TO_USER],
    }

    const typeId = cards.reduce((acc, card) => {
      acc.push(...cardsToTypesMap[card])
      return acc
    }, [])

    const {
      count: totalCount,
      rows: transactions,
    } = await Transaction.findAndCountAll({
      order,
      limit,
      offset: (offset - 1) * limit,
      where: {
        userId,
        typeId,
      },
    })

    const node = transactions.map(transaction => ({
      ...transaction.dataValues,
      card: Object.keys(cardsToTypesMap).filter(key =>
        cardsToTypesMap[key].includes(Number(transaction.typeId))
      )[0],
    }))

    return {
      edge: {
        node,
      },
      pageInfo: {
        totalCount,
        hasPrevPage: offset > 1,
        hasNextPage: totalCount > limit * offset,
      },
    }
  }
}
export default new GetUserStatmentService()
