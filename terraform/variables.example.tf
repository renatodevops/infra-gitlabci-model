variable "region" {
  type = string
  default = "nyc3"
}
variable "do_token" {
  type = string
  default = "seu token"
}
variable "access_id" {
  type = string
  default = "seu access"
}
variable "secret_key" {
  type = string
  default = "seu secret"
}
variable "pub_key" {
  type = string
  default = "./keys/id_rsa.pub"
}
variable "pvt_key" {
  type = string
  default = "./keys/id_rsa"
}
variable "machine" {
  description = "Ubuntu-LTS"
  default = "ubuntu-20-04-x64"
}
