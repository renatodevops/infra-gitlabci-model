import Sequelize from 'sequelize'

import databaseConfig from '../../../config/database'

import Country from './models/Country'
import GroupTransaction from './models/GroupTransaction'
import GroupTypeTransaction from './models/GroupTypeTransaction'
import Pin from './models/Pin'
import Role from './models/Role'
import Setting from './models/Setting'
import StatusTransaction from './models/StatusTransaction'
import Strategy from './models/Strategy'
import StrategyPlanning from './models/StrategyPlanning'
import SystemBtcWallet from './models/SystemBtcWallet'
import SystemLog from './models/SystemLog'
import Transaction from './models/Transaction'
import TransactionInfo from './models/TransactionInfo'
import TypefinancialInfo from './models/TypeFinancialInfo'
import TypeTransaction from './models/TypeTransaction'
import User from './models/User'
import UserAddress from './models/UserAddress'
import UserDataUpdate from './models/UserDataUpdate'
import UserFinancilInfo from './models/UserFinancialInfo'
import UserPin from './models/UserPin'
import UserRole from './models/UserRole'
import UserSetting from './models/UserSetting'
import VerifiedEmail from './models/VerifiedEmail'
import Voucher from './models/Voucher'
import UserManualDeposit from './models/UserManualDeposit'
import UserUnilevelNode from './models/UserUnilevelNode'
import UserFinancialPassword from './models/UserFinancialPassword'
import CardTransaction from './models/CardTransaction'
import Card from './models/Card'
import BtcTransaction from './models/BtcTransaction'
import UserAutoRenovationSetting from './models/UserAutoRenovationSetting'
import LogAvailableInput from './models/LogAvailableInput'
import AnnualPlanRate from './models/AnnualPlanRate'
import UserAnnualPlan from './models/UserAnnualPlan'
import UserDocument from './models/UserDocument'

const models = [
  UserAnnualPlan,
  AnnualPlanRate,
  LogAvailableInput,
  UserAutoRenovationSetting,
  BtcTransaction,
  CardTransaction,
  Card,
  UserFinancialPassword,
  UserManualDeposit,
  SystemLog,
  Voucher,
  UserPin,
  Pin,
  Strategy,
  StrategyPlanning,
  GroupTypeTransaction,
  GroupTransaction,
  TransactionInfo,
  StatusTransaction,
  SystemBtcWallet,
  Transaction,
  TypeTransaction,
  User,
  Country,
  VerifiedEmail,
  Role,
  UserRole,
  UserAddress,
  Setting,
  UserSetting,
  TypefinancialInfo,
  UserDataUpdate,
  UserFinancilInfo,
  UserUnilevelNode,
  UserDocument,
]

class Database {
  constructor() {
    this.init()
  }

  init() {
    this.connection = new Sequelize(databaseConfig)

    models
      .map(model => model.init(this.connection))
      .map(model => model.associate && model.associate(this.connection.models))
  }
}

export default new Database()
