/* eslint-disable import/no-cycle */

import User from '../../../shared/infra/sequelize/models/User'
import BeeQueue from '../../../shared/providers/QueueProvider/Bee'
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'
import { INCOME } from '../../../util/cardsIdentifiers'
import CreateTransferToAvailableCardService from './CreateTransferToAvailableCardService'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'

class CreateMonthlyIncomeCardTransactionService {
  get key() {
    return 'CreateMonthlyIncomeCardTransactionService'
  }

  get schedule() {
    return '0 0 1 * *'
  }

  async handle() {
    const users = await User.findAll()

    const balances = await Promise.all(
      users.map(async user => {
        const incomeBalance = await GetUserCardBalanceService.execute({
          userId: user.id,
          cardIdentifier: INCOME,
        })
        return { userId: user.id, incomeBalance }
      })
    )

    const balancesGreaterThanZero = balances.filter(
      user => user.incomeBalance > 0.0
    )

    const ADMINISTRATE_TAX = 0.1

    const result = await Promise.all(
      balancesGreaterThanZero.map(async ({ userId, incomeBalance }) => {
        const fee = incomeBalance * ADMINISTRATE_TAX * -1
        await CreateCardTransactionService.execute({
          value: fee,
          userId,
          status: CONFIRMED,
          description: `Taxa Administrativa de Rendimento Mensal`,
          cardIdentifier: INCOME,
        })

        const userIncomeValue = incomeBalance + fee

        await BeeQueue.add(CreateTransferToAvailableCardService.key, {
          userId,
          transferValue: userIncomeValue,
          description: 'Rendimento Mensal',
        })

        await CreateCardTransactionService.execute({
          value: userIncomeValue * -1,
          userId,
          status: CONFIRMED,
          description: 'Transferência de Rendimento para Disponível',
          cardIdentifier: INCOME,
        })

        return { userIncomeValue, userId }
      })
    )

    return result
  }
}
export default new CreateMonthlyIncomeCardTransactionService()
