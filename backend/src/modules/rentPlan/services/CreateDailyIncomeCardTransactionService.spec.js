import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'
import truncate from '../../../../__tests__/utils/truncate'
// import User from '../../../shared/infra/sequelize/models/User'
import StrategyPlanning from '../../../shared/infra/sequelize/models/StrategyPlanning'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import { APPLIED, INCOME } from '../../../util/cardsIdentifiers'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'
import CreateDailyIncomeCardTransactionService from './CreateDailyIncomeCardTransactionService'
import CreateUserService from '../../signUp/services/CreateUserService'
import convertUTCtoLocalTZ from '../../../util/convertUTCtoLOCALTZ'
import CardTransaction from '../../../shared/infra/sequelize/models/CardTransaction'

let user

const createUser = async (indicatorId = 1) =>
  CreateUserService.execute({
    name: 'parsedName',
    phone: '(84) 99999-9999',
    document: 'parsedDocument',
    email: `${crypto.randomBytes(6).toString('hex')}@${crypto
      .randomBytes(6)
      .toString('hex')}.com`,
    born: new Date(),
    terms: true,
    indicatorId,
    password: '12345678',
    selfieLink: 'selfieLink',
    documentFrontLink: 'selfieLink',
    documentBackLink: 'selfieLink',
  })

describe('CreateDailyIncomeCardTransactionService tests', () => {
  beforeEach(async () => {
    user = await createUser()
    await truncate([StrategyPlanning, CardTransaction])
  })
  it('should update the income balance to 0.96', async () => {
    await CreateCardTransactionService.execute({
      cardIdentifier: APPLIED,
      userId: user.id,
      value: 100.0,
      status: CONFIRMED,
      description: 'teste',
    })
    await StrategyPlanning.createForStrategyId({
      startDate: convertUTCtoLocalTZ(),
      planned: 0.01,
    })
    await CreateDailyIncomeCardTransactionService.handle()

    const incomeBalance = await GetUserCardBalanceService.execute({
      userId: user.id,
      cardIdentifier: INCOME,
    })

    expect(incomeBalance).toBe(0.96)
  })

  it('should update the income balance to 8.0', async () => {
    const upline5 = await createUser(user.id)
    const upline4 = await createUser(upline5.id)
    const upline3 = await createUser(upline4.id)
    const upline2 = await createUser(upline3.id)
    const upline1 = await createUser(upline2.id)
    const direct = await createUser(upline1.id)
    const investor = await createUser(direct.id)

    await CreateCardTransactionService.execute({
      cardIdentifier: APPLIED,
      userId: investor.id,
      value: 100.0,
      status: CONFIRMED,
      description: 'teste',
    })
    await StrategyPlanning.createForStrategyId({
      startDate: convertUTCtoLocalTZ(),
      planned: 0.1,
    })
    await CreateDailyIncomeCardTransactionService.handle()

    const incomeBalance = await GetUserCardBalanceService.execute({
      userId: investor.id,
      cardIdentifier: INCOME,
    })

    expect(incomeBalance).toBe(8.0)
  })
})
