module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'status_transactions',
      [
        {
          // 1
          name: 'Pendente',
          active: true,
        },
        {
          // 2
          name: 'Confirmada',
          active: true,
        },
        {
          // 3
          name: 'Cancelada',
          active: true,
        },
        {
          // 4
          name: 'Recusou',
          active: true,
        },
        {
          // 5
          name: 'Aguardando pagamento',
          active: true,
        },
        {
          // 6
          name: 'Aguardando primeira confirmação',
          active: true,
        },
        {
          // 7
          name: 'Aguardando terceira confirmação',
          active: true,
        },
        {
          // 8
          name: 'Enviei',
          active: true,
        },
        {
          // 9
          name: 'Verificado',
          active: true,
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('status_transactions', null, {})
  },
}
