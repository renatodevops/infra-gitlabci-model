module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('users_financial_infos', {
      user_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.BIGINT,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      type_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.BIGINT,
        references: {
          model: 'types_financial_infos',
          key: 'id',
        },
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      value: {
        allowNull: false,
        type: DataTypes.STRING(100),
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('users_financial_infos')
  },
}
