import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'
import User from '../../../shared/infra/sequelize/models/User'
import CreateAnnualPlanWithdrawalService from './CreateAnnualPlanWithdrawalService'
import { ANNUAL_PLAN, AVAILABLE } from '../../../util/cardsIdentifiers'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'

let user

describe('CreateAnnualPlanWithdrawalService tests', () => {
  beforeEach(async () => {
    user = await User.create({
      name: 'parsedName',
      phone: 'parsedPhone',
      document: 'parsedDocument',
      email: `${crypto.randomBytes(6).toString('hex')}@${crypto
        .randomBytes(6)
        .toString('hex')}.com`,
      born: new Date(),
      terms: true,
      indicatorId: 1,
      passwordHash: crypto.randomBytes(6).toString('hex'),
      selfieLink: 'selfieLink',
      documentFrontLink: 'selfieLink',
      documentBackLink: 'selfieLink',
    })
  })
  it('should return 0 0 0', async () => {
    const result = await CreateAnnualPlanWithdrawalService.handle({
      data: { userId: user.id },
    })
    expect(result).toHaveProperty('gains', 0.0)
    expect(result).toHaveProperty('feeValue', 0.0)
    expect(result).toHaveProperty('withdrawalValue', 0.0)
  })

  it('should return 0 25 75 and update the user cads balance', async () => {
    await CreateCardTransactionService.execute({
      cardIdentifier: ANNUAL_PLAN,
      userId: user.id,
      status: CONFIRMED,
      description: 'teste',
      value: 100,
    })

    const result = await CreateAnnualPlanWithdrawalService.handle({
      data: { userId: user.id },
    })
    expect(result).toHaveProperty('gains', 0.0)
    expect(result).toHaveProperty('feeValue', 25)
    expect(result).toHaveProperty('withdrawalValue', 75)

    const [availableBalance, annualPlanBalance] = await Promise.all([
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: AVAILABLE,
      }),
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: ANNUAL_PLAN,
      }),
    ])

    expect(availableBalance).toBe(75)
    expect(annualPlanBalance).toBe(0)
  })
})
