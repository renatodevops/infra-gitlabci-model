
module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface
      .createTable('strategies_planning', {
        id: {
          autoIncrement: true,
          allowNull: false,
          primaryKey: true,
          type: DataTypes.BIGINT,
        },
        strategy_id: {
          allowNull: false,
          type: DataTypes.BIGINT,
          references: {
            model: 'strategies',
            key: 'id',
          },
        },
        created_at: {
          allowNull: false,
          type: DataTypes.DATE,
          defaultValue: new Date(),
        },
        updated_at: {
          allowNull: false,
          type: DataTypes.DATE,
          defaultValue: new Date(),
        },
        deleted_at: {
          allowNull: true,
          type: DataTypes.DATE,
        },
        date: {
          allowNull: false,
          type: DataTypes.DATEONLY,
        },
        monthly_planned: {
          allowNull: false,
          type: DataTypes.DECIMAL(7, 2),
        },
        daily_planned: {
          allowNull: false,
          type: DataTypes.DECIMAL(12, 10),
        },
        daily_performed: {
          allowNull: false,
          type: DataTypes.DECIMAL(12, 10),
          defaultValue: 0,
        },
        is_editable: {
          allowNull: false,
          type: DataTypes.BOOLEAN,
          defaultValue: true,
        },
        confirmed: {
          allowNull: false,
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
      })
      .then(() => {
        return queryInterface.addConstraint(
          'strategies_planning',
          ['date', 'strategy_id'],
          {
            type: 'unique',
            name: 'uniqueStrategyPlan',
          }
        )
      })
  },
  down: queryInterface => {
    return queryInterface.dropTable('strategies_planning')
  },
}
