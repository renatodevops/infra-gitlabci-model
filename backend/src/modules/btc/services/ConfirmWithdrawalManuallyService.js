/**
 * REALIZA O PAGAMENTO DE UM SAQUE BTC DE FORMA MANUAL
 *
 * RN:
 *  - Transação deve existir
 *  - Transação deve estar não confirmada
 *  - Transação deve ser do tipo saque btc
 */

import Transaction from '../../../shared/infra/sequelize/models/Transaction'
import {
  STATUS_CONFIRMED,
  STATUS_UNCONFIRMED,
  TYPE_WITHDRAWAL_BTC,
} from '../../../util/typesTransactions'

class ConfirmWithdrawalManually {
  async execute(transactionId) {
    if (!transactionId) throw Error('Identificador de transação inválido')
    const transaction = await Transaction.findOne({
      where: {
        id: transactionId,
        typeId: TYPE_WITHDRAWAL_BTC,
        statusId: STATUS_UNCONFIRMED,
      },
    })
    if (!transaction) throw Error('Transação não encontrada')
    await transaction.update({ statusId: STATUS_CONFIRMED })
    return `Saque #${transactionId} confirmado manualmente`
  }
}
export default new ConfirmWithdrawalManually()
