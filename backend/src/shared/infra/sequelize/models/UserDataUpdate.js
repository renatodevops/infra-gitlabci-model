import Sequelize, { Model } from 'sequelize'

class UserDataUpdate extends Model {
  static init(sequelize) {
    super.init(
      {
        code: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'The code can not be empty',
            },
            len: {
              args: [1, 60],
              msg: 'The code must contain less than 60 characters',
            },
          },
        },
        userId: {
          type: Sequelize.BIGINT,
        },
        typeId: {
          type: Sequelize.BIGINT,
        },
        expiration: Sequelize.DATE,
        active: Sequelize.BOOLEAN,
      },
      { sequelize, tableName: 'users_data_updates' }
    )

    this.addHook('beforeSave', async update => {
      const now = new Date()
      update.expiration = now.setHours(now.getHours() + 6)
    })

    return this
  }
}
export default UserDataUpdate
