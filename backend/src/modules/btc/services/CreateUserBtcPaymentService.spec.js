import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'
import CreateUserService from '../../signUp/services/CreateUserService'
import CreateUserBtcPaymentService from './CreateUserBtcPaymentService'
import BtcTransaction from '../../../shared/infra/sequelize/models/BtcTransaction'
import { AWAITING_FOR_PAYMENT } from '../../../util/btcTransactionsStatus'

let user

const createUser = async (indicatorId = 1) =>
  CreateUserService.execute({
    name: 'parsedName',
    phone: '(84) 99999-9999',
    document: 'parsedDocument',
    email: `${crypto.randomBytes(6).toString('hex')}@${crypto
      .randomBytes(6)
      .toString('hex')}.com`,
    born: new Date(),
    terms: true,
    indicatorId,
    password: '12345678',
    selfieLink: 'selfieLink',
    documentFrontLink: 'selfieLink',
    documentBackLink: 'selfieLink',
  })

describe('CreateUserBtcPaymentService tests', () => {
  beforeAll(async () => {
    user = await createUser()
  })
  it('should create a user btc payment', async () => {
    const result = await CreateUserBtcPaymentService.execute({
      userId: user.id,
      value: 100,
    })
    expect(result).toHaveProperty('address')
    expect(result).toHaveProperty('uri')
    expect(result).toHaveProperty('value', 100.0)

    const btcPayment = await BtcTransaction.findOne({
      where: { address: result.address },
    })
    expect(btcPayment).toHaveProperty('userId', user.id)
    expect(btcPayment).toHaveProperty('status', AWAITING_FOR_PAYMENT)
  })
})
