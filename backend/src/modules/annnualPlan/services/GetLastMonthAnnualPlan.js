import { subMonths } from 'date-fns'
import AnnualPlanRate from '../../../shared/infra/sequelize/models/AnnualPlanRate'
import convertUTCtoLocalTZ from '../../../util/convertUTCtoLOCALTZ'

class GetLastMonthAnnualPlan {
  async execute() {
    const now = convertUTCtoLocalTZ()
    const lastMonth = subMonths(now, 1)

    let lastMonthRate = await AnnualPlanRate.findOne({
      where: {
        year: lastMonth.getUTCFullYear(),
        month: lastMonth.getUTCMonth(),
      },
    })

    if (!lastMonthRate)
      lastMonthRate = await AnnualPlanRate.create({
        year: lastMonth.getUTCFullYear(),
        month: lastMonth.getUTCMonth(),
        rate: 0.055,
      })

    return lastMonthRate
  }
}
export default new GetLastMonthAnnualPlan()
