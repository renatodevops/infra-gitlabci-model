import Sequelize, { Model } from 'sequelize'

class Card extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        identifier: Sequelize.STRING,
        isVisible: Sequelize.BOOLEAN,
      },
      { sequelize, tableName: 'cards' }
    )

    return this
  }
}
export default Card
