import Sequelize, { Model } from 'sequelize'

class Role extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        level: Sequelize.INTEGER,
        label: Sequelize.STRING,
        action: Sequelize.STRING,
        description: Sequelize.STRING,
        active: Sequelize.BOOLEAN,
      },
      { sequelize, tableName: 'roles' }
    )
    return this
  }

  static associate(models) {
    this.hasMany(models.UserRole, { as: 'userRole', foreignKey: 'roleId' })
  }
}
export default Role
