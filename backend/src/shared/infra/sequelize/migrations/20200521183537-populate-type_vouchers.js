module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'types_voucher',
      [
        {
          // 1
          name: 'Voucher de Bonificação',
          description: 'Voucher de Bonificação',
          active: true,
        },
        {
          // 1
          name: 'Voucher de Ativação manual',
          description: 'Voucher de Ativação manual',
          active: true,
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('types_voucher', null, {})
  },
}
