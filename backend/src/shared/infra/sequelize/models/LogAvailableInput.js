import Sequelize, { Model } from 'sequelize'

class LogAvailableInput extends Model {
  static init(sequelize) {
    super.init(
      {
        userId: Sequelize.BIGINT,
        appliedInput: Sequelize.DECIMAL,
        availableInput: Sequelize.DECIMAL,
        limit: Sequelize.DECIMAL,
        capacity: Sequelize.DECIMAL,
        bonusValue: Sequelize.DECIMAL,
        paiedValue: Sequelize.DECIMAL,
      },
      { sequelize, tableName: 'logs_available_payments' }
    )

    return this
  }
}
export default LogAvailableInput
