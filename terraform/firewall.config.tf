resource "digitalocean_firewall" "ssh" {
  name = "ssh-access-firewall"

  droplet_ids = [
    digitalocean_droplet.gitlabci-Frontend.id,
    # digitalocean_droplet.gitlabci-Backend.id,
    # digitalocean_droplet.gitlabci-Database.id,
    # digitalocean_droplet.gitlabci-BitcoinCore.id,
  ]

  inbound_rule {
    protocol = "tcp"
    port_range = "22"
    source_addresses = [
      "192.168.10.0/24", 
      "187.61.218.15/32"
    ]
  }

  inbound_rule {
    protocol = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol = "tcp"
    port_range = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol = "udp"
    port_range = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}

resource "digitalocean_firewall" "frontend" {
  name = "frontend-access-firewall"

  droplet_ids = [
    digitalocean_droplet.gitlabci-Frontend.id,
    # digitalocean_droplet.gitlabci-Backend.id,
    # digitalocean_droplet.gitlabci-Database.id
  ]

  inbound_rule {
    protocol = "tcp"
    port_range = "80"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol = "tcp"
    port_range = "443"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol = "tcp"
    port_range = "8080"
    source_addresses = ["192.168.10.0/24", "187.61.218.15/32", "177.89.119.65/32"]
  }

  inbound_rule {
    protocol = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol = "tcp"
    port_range = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol = "udp"
    port_range = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}

# resource "digitalocean_firewall" "database" {
#   name = "database-access-firewall"

#   droplet_ids = [digitalocean_droplet.gitlabci-Database.id]

#   inbound_rule {
#     protocol = "tcp"
#     port_range = "5432"
#     source_addresses = [
#       "192.168.10.0/24", 
#       "187.61.218.15/32"
#     ]
#   }

#   inbound_rule {
#     protocol = "icmp"
#     source_addresses = ["0.0.0.0/0", "::/0"]
#   }

#   outbound_rule {
#     protocol = "tcp"
#     port_range = "1-65535"
#     destination_addresses = ["0.0.0.0/0", "::/0"]
#   }

#   outbound_rule {
#     protocol = "udp"
#     port_range = "1-65535"
#     destination_addresses = ["0.0.0.0/0", "::/0"]
#   }

#   outbound_rule {
#     protocol = "icmp"
#     destination_addresses = ["0.0.0.0/0", "::/0"]
#   }
# }

# resource "digitalocean_firewall" "bitcoincore" {
#   name = "bitcoincore-access-firewall"

#   droplet_ids = [digitalocean_droplet.gitlabci-BitcoinCore.id]

#   inbound_rule {
#     protocol = "tcp"
#     port_range = "18332"
#     source_addresses = [
#       "192.168.10.0/24", 
#       "187.61.218.15/32"
#     ]
#   }

#   inbound_rule {
#     protocol = "tcp"
#     port_range = "8332"
#     source_addresses = ["192.168.10.0/24", "187.61.218.15/32"]
#   }

#   inbound_rule {
#     protocol = "tcp"
#     port_range = "8333"
#     source_addresses = ["192.168.10.0/24", "187.61.218.15/32"]
#   }

#   inbound_rule {
#     protocol = "tcp"
#     port_range = "18444"
#     source_addresses = ["192.168.10.0/24", "187.61.218.15/32"]
#   }

#   inbound_rule {
#     protocol = "tcp"
#     port_range = "18443"
#     source_addresses = ["192.168.10.0/24", "187.61.218.15/32"]
#   }

#   inbound_rule {
#     protocol = "icmp"
#     source_addresses = ["192.168.10.0/24", "187.61.218.15/32"]
#   }

#   outbound_rule {
#     protocol = "tcp"
#     port_range = "1-65535"
#     destination_addresses = ["0.0.0.0/0", "::/0"]
#   }

#   outbound_rule {
#     protocol = "udp"
#     port_range = "1-65535"
#     destination_addresses = ["0.0.0.0/0", "::/0"]
#   }

#   outbound_rule {
#     protocol = "icmp"
#     destination_addresses = ["0.0.0.0/0", "::/0"]
#   }
# }
