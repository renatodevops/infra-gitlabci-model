import { createWriteStream } from 'fs'
import crypto from 'crypto'
import {
  authenticated,
  compose,
  hasAllowed,
} from '../../../shared/infra/http/composers/composables'
import uploadConfig from '../../../config/upload'
import S3StorageProvider from '../../../shared/providers/StorageProvider/S3StorageProvider'
import UserManualDeposit from '../../../shared/infra/sequelize/models/UserManualDeposit'

export default {
  // Query: {},

  Mutation: {
    CreateUserManualDeposit: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, args, ctx) => {
      const { userComment, declaredUsdValue, file } = args

      const { filename, mimetype, createReadStream } = await file

      if (
        !mimetype.split('/').includes('image') &&
        !mimetype.split('/').includes('pdf')
      )
        throw new Error('Formato inválido de arquivo')

      const fileHash = crypto.randomBytes(10).toString('hex')
      const fileName = `${fileHash}-${filename}`
      await new Promise(res => {
        createReadStream()
          .pipe(createWriteStream(`${uploadConfig.tmpFolder}/${fileName}`))
          .on('close', res)
      })
      const fileLink = await S3StorageProvider.saveFile(fileName)

      await UserManualDeposit.create({
        userId: ctx.user.id,
        fileLink,
        userComment,
        declaredUsdValue,
        status: 'Em análise',
      })

      return `Depósito informado com sucesso`
    }),
  },
}
