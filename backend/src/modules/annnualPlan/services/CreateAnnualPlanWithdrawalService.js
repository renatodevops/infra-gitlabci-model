/* eslint-disable import/no-cycle */
import { ANNUAL_PLAN, AVAILABLE } from '../../../util/cardsIdentifiers'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import GetAnnualPlanBalanceService from './GetAnnualPlanBalanceService'
import GetAnnualPlanGainsService from './GetAnnualPlanGainsService'

class CreateAnnualPlanWithdrawalService {
  get key() {
    return 'CreateAnnualPlanWithdrawalService'
  }

  async handle({ data }) {
    const { userId } = data

    const balance = await GetAnnualPlanBalanceService.execute({ userId })

    const gains = await GetAnnualPlanGainsService.execute({ userId })

    const FEE_RATE = 0.25

    const feeValue = balance * FEE_RATE

    const withdrawalValue = balance - feeValue - gains

    await Promise.all([
      CreateCardTransactionService.execute({
        userId,
        cardIdentifier: ANNUAL_PLAN,
        description: 'Reembolso',
        status: CONFIRMED,
        value: withdrawalValue * -1,
      }),
      CreateCardTransactionService.execute({
        userId,
        cardIdentifier: AVAILABLE,
        description: 'Reembolso de Plano anual',
        status: CONFIRMED,
        value: withdrawalValue,
      }),
      CreateCardTransactionService.execute({
        userId,
        cardIdentifier: ANNUAL_PLAN,
        description: 'Taxa de Reembolso de Aplicação',
        status: CONFIRMED,
        value: feeValue * -1,
      }),
      CreateCardTransactionService.execute({
        userId,
        cardIdentifier: ANNUAL_PLAN,
        description: 'Ganhos com plano anual',
        status: CONFIRMED,
        value: gains * -1,
      }),
    ])

    return { gains, feeValue, withdrawalValue }
  }
}

export default new CreateAnnualPlanWithdrawalService()
