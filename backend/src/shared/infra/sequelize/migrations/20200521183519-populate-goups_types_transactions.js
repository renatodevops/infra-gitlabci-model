module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'groups_types_transactions',
      [
        {
          group_id: 1, // credit
          type_id: 2, // deposito btc
        },

        {
          group_id: 4, // available
          type_id: 3, // bonus indicação
        },
        {
          group_id: 4, // available
          type_id: 4, // bonus indicação
        },
        {
          group_id: 4, // available
          type_id: 5, // bonus indicação
        },
        {
          group_id: 4, // available
          type_id: 6, // bonus indicação
        },
        {
          group_id: 4, // available
          type_id: 7, // bonus indicação
        },
        {
          group_id: 4, // available
          type_id: 8, // bonus indicação
        },
        {
          group_id: 4, // available
          type_id: 9, // bonus indicação
        },
        {
          group_id: 4, // available
          type_id: 10, // bonus indicação
        },
        {
          group_id: 4, // available
          type_id: 11, // bonus indicação
        },
        {
          group_id: 4, // available
          type_id: 12, // bonus indicação
        },
        {
          group_id: 4, // available
          type_id: 16, // Pagamento de Rendimento
        },
        {
          group_id: 4, // applied
          type_id: 19, // Premiação Plano de Carreira
        },

        {
          group_id: 2, // income
          type_id: 20, // ativação de voucher
        },
        {
          group_id: 2, // applied
          type_id: 13, // Finalização de Aplicação
        },

        {
          group_id: 3, // income
          type_id: 14, // Rendimento
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('groups_types_transactions', null, {})
  },
}
