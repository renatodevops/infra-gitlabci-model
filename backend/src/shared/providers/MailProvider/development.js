import { resolve } from 'path'
import nodemailer from 'nodemailer'

import handlebars from 'nodemailer-express-handlebars'
import expressHandlebars from 'express-handlebars'

import mailConfig from '../../../config/mail'

class Mail {
  constructor() {
    nodemailer.createTestAccount().then(account => {
      this.client = nodemailer.createTransport({
        host: account.smtp.host,
        port: account.smtp.port,
        secure: account.smtp.secure,
        auth: {
          user: account.user,
          pass: account.pass,
        },
      })
      this.configureTemplates()
    })
  }

  configureTemplates() {
    const viewPath = resolve(
      __dirname,
      '..',
      '..',
      '..',
      'resources',
      'views',
      'emails'
    )

    this.client.use(
      'compile',
      handlebars({
        viewEngine: expressHandlebars.create({
          layoutsDir: resolve(viewPath, 'layouts'),
          partialsDir: resolve(viewPath, 'partials'),
          defaultLayout: 'default',
          extname: '.hbs',
        }),
        viewPath,
        extName: '.hbs',
      })
    )
  }

  async sendMail(message) {
    const info = await this.client.sendMail({
      ...mailConfig.default,
      ...message,
    })

    console.log('Message sent: %s', info.messageId)
    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info))
  }
}

export default new Mail()
