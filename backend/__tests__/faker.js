import faker from 'faker'

export function userFaker(indicatorId = 1, email = faker.internet.email()) {
  return {
    code: null,
    name: faker.name.firstName(),
    document: faker.random.uuid(),
    email,
    passwordHash: '12345678',
    phone: '(84) 99999-9999',
    born: '2019-01-01',
    terms: true,
    countryId: 1,
    indicatorId,
  }
}

export function financialInfoFaker(
  userId,
  typeId = 2,
  value = faker.random.uuid()
) {
  return {
    userId,
    typeId,
    value,
  }
}

export function addressFaker() {
  return {
    state: faker.address.state(),
    city: faker.address.city(),
    neighborhood: 'test',
    street: 'test',
    number: '0000',
    zipCode: '00000000000',
  }
}
