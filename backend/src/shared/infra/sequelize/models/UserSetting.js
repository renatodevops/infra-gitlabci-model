import Sequelize, { Model } from 'sequelize'

class UserSettings extends Model {
  static init(sequelize) {
    super.init(
      {
        userId: {
          type: Sequelize.BIGINT,
          primaryKey: true,
        },
        typeId: {
          type: Sequelize.BIGINT,
          primaryKey: true,
        },
        value: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'The value can not be empty',
            },
          },
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
      },
      { sequelize, tableName: 'users_settings' }
    )

    this.addHook('beforeCreate', async ({ userId, typeId }) => {
      const userSetting = await UserSettings.findOne({
        where: { userId, typeId },
      })

      if (userSetting) throw Error('Settings already exists')
    })

    return this
  }

  associate(models) {
    UserSettings.belongsTo(models.User, { foreginKey: 'userId', as: 'user' })
    UserSettings.belongsTo(models.Setting, {
      foreginKey: 'typeId',
      as: 'type',
    })
  }
}
export default UserSettings
