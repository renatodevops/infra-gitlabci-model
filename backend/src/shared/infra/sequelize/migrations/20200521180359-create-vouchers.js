module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('vouchers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      creator_id: {
        allowNull: false,
        type: DataTypes.BIGINT,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      owner_id: {
        allowNull: true,
        type: DataTypes.BIGINT,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      leader_id: {
        allowNull: true,
        type: DataTypes.BIGINT,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      type_id: {
        allowNull: false,
        type: DataTypes.BIGINT,
        references: {
          model: 'types_voucher',
          key: 'id',
        },
      },
      status_id: {
        allowNull: false,
        type: DataTypes.BIGINT,
        references: {
          model: 'status_voucher',
          key: 'id',
        },
        defaultValue: 1,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      value: {
        allowNull: false,
        type: DataTypes.DECIMAL(10, 2),
      },
      hash: {
        allowNull: false,
        type: DataTypes.STRING,
        unique: true,
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('vouchers')
  },
}
