import { addMonths } from 'date-fns'
import AnnualPlanRate from '../../../shared/infra/sequelize/models/AnnualPlanRate'
import convertUTCtoLocalTZ from '../../../util/convertUTCtoLOCALTZ'

class GetNextMonthAnnualPlan {
  async execute() {
    const now = convertUTCtoLocalTZ()

    const nextMonth = addMonths(now, 1)

    let nextMonthRate = await AnnualPlanRate.findOne({
      where: {
        year: nextMonth.getUTCFullYear(),
        month: nextMonth.getUTCMonth(),
      },
    })

    if (!nextMonthRate)
      nextMonthRate = await AnnualPlanRate.create({
        year: nextMonth.getUTCFullYear(),
        month: nextMonth.getUTCMonth(),
        rate: 0.055,
      })

    return nextMonthRate
  }
}
export default new GetNextMonthAnnualPlan()
