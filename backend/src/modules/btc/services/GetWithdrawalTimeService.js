import { startOfMonth, isAfter, isBefore, addHours } from 'date-fns'
import convertUTCtoLocalTZ from '../../../util/convertUTCtoLOCALTZ'

class GetWithdrawalTimeService {
  execute(now = new Date()) {
    const nowNatal = convertUTCtoLocalTZ(now)
    const startOfPeriod = startOfMonth(nowNatal)
    const endOfPeriod = addHours(startOfPeriod, 48)

    return isAfter(nowNatal, startOfPeriod) && isBefore(nowNatal, endOfPeriod)
  }
}
export default new GetWithdrawalTimeService()
