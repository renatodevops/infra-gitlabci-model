/* eslint-disable no-console */
import { AVAILABLE } from '../../../util/cardsIdentifiers'
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'
import UserFinancialInfo from '../../../shared/infra/sequelize/models/UserFinancialInfo'
import GetUserBtcWithdrawalFeeRateService from './GetUserBtcWithdrawalFeeRateService'
import { depositBtcWallet } from '../../../shared/container'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import {
  AGUARDANDO_PROCESSAMENTO,
  CONFIRMED,
} from '../../../util/btcTransactionsStatus'
import BtcTransaction from '../../../shared/infra/sequelize/models/BtcTransaction'

class CreateUserBtcWithdrawal {
  async execute({ withdrawalValue, userId }) {
    const availableBalance = await GetUserCardBalanceService.execute({
      userId,
      cardIdentifier: AVAILABLE,
    })
    if (availableBalance < withdrawalValue) return 'Fundos insuficientes'

    const minWithdrawalValue = 20.0
    if (withdrawalValue < minWithdrawalValue)
      return `O valor mínimo de retirada é LKT ${minWithdrawalValue}`

    const TYPE_FINANCIAL_INFO_BITCOIN_WALLET = 2
    const financialInfo = await UserFinancialInfo.findOne({
      where: {
        userId,
        typeId: TYPE_FINANCIAL_INFO_BITCOIN_WALLET,
      },
    })
    if (!financialInfo) return 'Cadastre seu endereço BTC na página de perfil'

    const withdrawalFeeRate = GetUserBtcWithdrawalFeeRateService.execute()
    const fee = withdrawalValue * withdrawalFeeRate

    const userWithdrawalValue = withdrawalValue - fee

    try {
      const amount = await depositBtcWallet.ExchangeToBTC(
        userWithdrawalValue,
        'USD'
      )
      await CreateCardTransactionService.execute({
        cardIdentifier: AVAILABLE,
        userId,
        value: userWithdrawalValue * -1,
        status: CONFIRMED,
        description: 'Saque em bitcoins',
      })
      await CreateCardTransactionService.execute({
        cardIdentifier: AVAILABLE,
        userId,
        value: fee * -1,
        status: CONFIRMED,
        description: 'Taxa de Saque em bitcoins',
      })
      await BtcTransaction.create({
        userId,
        address: financialInfo.value,
        category: 'send',
        status: AGUARDANDO_PROCESSAMENTO,
        value: userWithdrawalValue,
        amount,
      })
      return 'Sua solicitação de retirada está aguardando processamento, o tempo estimado de pagamento é de 48 horas!'
    } catch (err) {
      console.log(err)
      throw new Error(
        'Não foi possível criar sua solicitação agora, tente novamente mais tarde'
      )
    }
  }
}
export default new CreateUserBtcWithdrawal()
