module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('users_roles', {
      user_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.BIGINT,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      role_id: {
        primaryKey: true,
        allowNull: false,
        type: DataTypes.BIGINT,
        onDelete: 'CASCADE',
        references: {
          model: 'roles',
          key: 'id',
        },
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      active: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: true,
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('users_roles')
  },
}
