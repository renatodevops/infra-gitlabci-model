import { Op } from 'sequelize'

import {
  authenticated,
  compose,
  hasAllowed,
} from '../../../shared/infra/http/composers/composables'

import User from '../../../shared/infra/sequelize/models/User'

export default {
  Query: {
    getUnilevelNetwork: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, { indicatorId }) => {
      const indicateds = await User.findAll({
        where: { indicatorId },
        order: [['id', 'ASC']],
      })

      return Promise.all(
        indicateds.map(async indicated => {
          const [user] = await User.findAll({
            where: {
              userPath: { [Op.substring]: `.${indicated.id}.` },
            },
            order: [['userPath', 'DESC']],
            limit: 1,
          })
          if (!user) return { indicated, depth: 0 }
          const { path } = user
          const index = path.indexOf(`.${indicated.id}.`)
          const depth = index === -1 ? 0 : path.slice(index).length
          return { indicated, depth }
        })
      )
    }),
  },
}
