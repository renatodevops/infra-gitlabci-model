module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('transactions_infos', {
      transaction_id: {
        primaryKey: true,
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: 'transactions',
          key: 'id',
        },
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      confirmations: {
        allowNull: false,
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      category: {
        allowNull: true,
        type: DataTypes.STRING,
      },
      address: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      amount: {
        allowNull: true,
        type: DataTypes.DECIMAL(10, 8),
      },
      block_height: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('transactions_infos')
  },
}
