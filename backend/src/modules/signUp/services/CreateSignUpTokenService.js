import { v4 } from 'uuid'
import { cacheProvider } from '../../../shared/container'
import MailProduction from '../../../shared/providers/MailProvider/production'
import MailDevelopment from '../../../shared/providers/MailProvider/development'
import User from '../../../shared/infra/sequelize/models/User'

class CreateSignUpTokenService {
  async execute({ name, indicatorId, email }) {
    const parsedEmail = email.trim().toLowerCase()

    const emailExists = await User.findOne({ where: { email: parsedEmail } })
    if (emailExists) throw new Error('E-mail em uso')
    const emailVerificationToken = v4()

    await cacheProvider.save(`signup:${emailVerificationToken}`, {
      name,
      indicatorId,
      email: parsedEmail,
    })

    const data = {
      to: `${name} <${email}>`,
      subject: `[${process.env.APP_NAME}] Criação de Conta`,
      template: 'check_email',
      context: {
        name,
        link: `${process.env.FRONT_URL}/signup/${emailVerificationToken}`,
      },
    }

    if (process.env.DEVELOPMENT === 'true') await MailDevelopment.sendMail(data)
    else await MailProduction.sendMail(data)

    return `Acesse seu email para concluir seu cadastro`
  }
}
export default new CreateSignUpTokenService()
