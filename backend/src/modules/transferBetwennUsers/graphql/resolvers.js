import { Op } from 'sequelize'
import {
  authenticated,
  compose,
  hasAllowed,
  verifyFinancialPassword,
} from '../../../shared/infra/http/composers/composables'
import User from '../../../shared/infra/sequelize/models/User'
import CreateTransferBetweenUsersService from '../services/CreateTransferBetweenUsers'
import GetUserStatmentService from '../../statment/services/GetUserStatmentService'

export default {
  Query: {
    getUsersToSendTransfer: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) => {
      return User.findAll({
        where: {
          id: { [Op.ne]: ctx.user.id },
        },
        order: [['name', 'ASC']],
      })
    }),
    getMyTransfersList: compose(
      authenticated,
      hasAllowed([1, 60])
    )(async (parent, args, ctx) => {
      const {
        sortField = 'created',
        sortDirection = 'DESC',
        limit = 10,
        offset = 1,
      } = args
      const userId = ctx.user.id

      const sortFieldMap = {
        created: 'createdAt',
        value: 'value',
      }

      const cards = ['user_transfer']

      return GetUserStatmentService.execute({
        userId,
        limit,
        offset,
        order: [[sortFieldMap[sortField], sortDirection]],
        cards,
      })
    }),
  },

  Mutation: {
    CreateTransferBetweenUsers: compose(
      authenticated,
      verifyFinancialPassword,
      hasAllowed([1])
    )(async (_, { receivingUserId, value }, ctx) => {
      await CreateTransferBetweenUsersService.execute({
        sendingUserId: Number(ctx.user.id),
        receivingUserId: Number(receivingUserId),
        value: Number(value),
      })

      return 'Transferência realizada com sucesso, verifique seu extrato'
    }),
  },
}
