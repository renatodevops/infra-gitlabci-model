module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('users_addresses', {
      user_id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      state: {
        allowNull: true,
        type: DataTypes.STRING(300),
      },
      city: {
        allowNull: true,
        type: DataTypes.STRING(300),
      },
      neighborhood: {
        allowNull: true,
        type: DataTypes.STRING(300),
      },
      street: {
        allowNull: true,
        type: DataTypes.STRING(300),
      },
      number: {
        allowNull: true,
        type: DataTypes.STRING(300),
      },
      zip_code: {
        allowNull: true,
        type: DataTypes.STRING(300),
      },
      active: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: true,
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('users_addresses')
  },
}
