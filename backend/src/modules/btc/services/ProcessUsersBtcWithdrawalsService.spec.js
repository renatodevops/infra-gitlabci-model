import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'
import truncate from '../../../../__tests__/utils/truncate'
import CreateUserService from '../../signUp/services/CreateUserService'
import {
  AGUARDANDO_PROCESSAMENTO,
  PROCESSADA,
} from '../../../util/btcTransactionsStatus'
import Bitcoin from '../../../shared/providers/BtcProvider/bitcoinCore'
import BtcTransaction from '../../../shared/infra/sequelize/models/BtcTransaction'
import { depositBtcWallet } from '../../../shared/container'
import ProcessUsersBtcWithdrawalsService from './ProcessUsersBtcWithdrawalsService'

let user

const testWallet = new Bitcoin('test.dat')

const createUser = async (indicatorId = 1) =>
  CreateUserService.execute({
    name: 'parsedName',
    phone: '(84) 99999-9999',
    document: 'parsedDocument',
    email: `${crypto.randomBytes(6).toString('hex')}@${crypto
      .randomBytes(6)
      .toString('hex')}.com`,
    born: new Date(),
    terms: true,
    indicatorId,
    password: '12345678',
    selfieLink: 'selfieLink',
    documentFrontLink: 'selfieLink',
    documentBackLink: 'selfieLink',
  })

describe('ProcessUsersBtcWithdrawalsService tests', () => {
  beforeEach(async () => {
    user = await createUser()
    await testWallet.sendToAddress({
      address: await depositBtcWallet.getNewAddress(),
      amount: 0.5,
    })
    await testWallet.generate()
    await truncate([BtcTransaction])
  })
  it('should process the user btc withdrawal', async () => {
    const address1 = await testWallet.getNewAddress()
    const address2 = await testWallet.getNewAddress()
    const [withdrawal1, withdrawal2, withdrawal3] = await Promise.all([
      BtcTransaction.create({
        userId: user.id,
        address: address1,
        category: 'send',
        status: AGUARDANDO_PROCESSAMENTO,
        value: 100,
        amount: await testWallet.ExchangeToBTC(100),
      }),
      BtcTransaction.create({
        userId: user.id,
        address: address2,
        category: 'send',
        status: AGUARDANDO_PROCESSAMENTO,
        value: 200,
        amount: await testWallet.ExchangeToBTC(200),
      }),
      BtcTransaction.create({
        userId: user.id,
        address: address2,
        category: 'send',
        status: AGUARDANDO_PROCESSAMENTO,
        value: 300,
        amount: await testWallet.ExchangeToBTC(300),
      }),
    ])

    const transactionsIds = [withdrawal1.id, withdrawal2.id, withdrawal3.id]

    const result = await ProcessUsersBtcWithdrawalsService.handle({
      data: { transactionsIds },
    })

    expect(result).toHaveProperty('txid')
    expect(result).toHaveProperty('amounts')
    expect(result).toHaveProperty('subtractFeeFrom', [address1, address2])

    let updatedWithdrawal = await BtcTransaction.findByPk(withdrawal1.id)
    expect(updatedWithdrawal).toHaveProperty('txid', result.txid)
    expect(updatedWithdrawal).toHaveProperty('status', PROCESSADA)

    updatedWithdrawal = await BtcTransaction.findByPk(withdrawal2.id)
    expect(updatedWithdrawal).toHaveProperty('txid', result.txid)
    expect(updatedWithdrawal).toHaveProperty('status', PROCESSADA)

    updatedWithdrawal = await BtcTransaction.findByPk(withdrawal3.id)
    expect(updatedWithdrawal).toHaveProperty('txid', result.txid)
    expect(updatedWithdrawal).toHaveProperty('status', PROCESSADA)
  })
  it('should return transações não encontradas', async () => {
    const address1 = await testWallet.getNewAddress()
    const address2 = await testWallet.getNewAddress()
    const [withdrawal1, withdrawal2, withdrawal3] = await Promise.all([
      BtcTransaction.create({
        userId: user.id,
        address: address1,
        category: 'send',
        status: PROCESSADA,
        value: 100,
        amount: await testWallet.ExchangeToBTC(100),
      }),
      BtcTransaction.create({
        userId: user.id,
        address: address2,
        category: 'send',
        status: PROCESSADA,
        value: 200,
        amount: await testWallet.ExchangeToBTC(200),
      }),
      BtcTransaction.create({
        userId: user.id,
        address: address2,
        category: 'send',
        status: PROCESSADA,
        value: 300,
        amount: await testWallet.ExchangeToBTC(300),
      }),
    ])

    const transactionsIds = [withdrawal1.id, withdrawal2.id, withdrawal3.id]

    const result = await ProcessUsersBtcWithdrawalsService.handle({
      data: { transactionsIds },
    })

    expect(result).toBe('Transações não encontradas')
  })
})
