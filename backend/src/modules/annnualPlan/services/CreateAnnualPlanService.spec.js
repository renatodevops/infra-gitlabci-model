import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'
import User from '../../../shared/infra/sequelize/models/User'
import CreateAnnualPlanService from './CreateAnnualPlanService'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import { ANNUAL_PLAN, CREDIT } from '../../../util/cardsIdentifiers'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'
import CreateUserAnnualPlanService from './CreateUserAnnualPlanService'

let user
let userAnnualPlan

describe('CreateAnnualPlanService tests', () => {
  beforeEach(async () => {
    user = await User.create({
      name: 'parsedName',
      phone: 'parsedPhone',
      document: 'parsedDocument',
      email: `${crypto.randomBytes(6).toString('hex')}@${crypto
        .randomBytes(6)
        .toString('hex')}.com`,
      born: new Date(),
      terms: true,
      indicatorId: 1,
      passwordHash: crypto.randomBytes(6).toString('hex'),
      selfieLink: 'selfieLink',
      documentFrontLink: 'selfieLink',
      documentBackLink: 'selfieLink',
    })
    userAnnualPlan = await CreateUserAnnualPlanService.execute({
      userId: user.id,
      value: 100,
    })
  })
  it('should return Valor mínimo não alcançado', async () => {
    await userAnnualPlan.update({ status: 'AGUARDANDO ATIVAÇÂO' })
    const result = await CreateAnnualPlanService.handle({
      data: { userAnnualPlanId: userAnnualPlan.id },
    })
    expect(result).toBe('Saldo insuficiente')
  })
  it('should return Ativação não pode ser realizada', async () => {
    const result = await CreateAnnualPlanService.handle({
      data: { userAnnualPlanId: userAnnualPlan.id },
    })
    expect(result).toBe('Ativação não pode ser realizada')
  })

  it('should return Aporte realizado com sucesso and update the user cads balance', async () => {
    await CreateCardTransactionService.execute({
      cardIdentifier: CREDIT,
      userId: user.id,
      status: CONFIRMED,
      description: 'teste',
      value: 100,
    })
    await userAnnualPlan.update({ status: 'AGUARDANDO ATIVAÇÂO' })
    const result = await CreateAnnualPlanService.handle({
      data: { userAnnualPlanId: userAnnualPlan.id },
    })
    expect(result).toBe('Aporte realizado com sucesso')

    const [creditBalance, appliedBalance] = await Promise.all([
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: CREDIT,
      }),
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: ANNUAL_PLAN,
      }),
    ])

    expect(creditBalance).toBe(0)
    expect(appliedBalance).toBe(100)
  })
})
