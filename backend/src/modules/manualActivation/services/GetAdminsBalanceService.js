import { fn, col } from 'sequelize'
import Voucher from '../../../shared/infra/sequelize/models/Voucher'
import Transaction from '../../../shared/infra/sequelize/models/Transaction'
import { cacheProvider } from '../../../shared/container'

import {
  STATUS_CONFIRMED,
  TYPE_MANUAL_ACTIVATION,
} from '../../../util/typesTransactions'

class GetAdminsBalanceService {
  async execute() {
    const key = `admins-balance`

    const cacheData = await cacheProvider.recover(key)

    if (cacheData) return cacheData

    const [vouchers, activations] = await Promise.all([
      Voucher.findAll({
        where: {
          typeId: 2,
        },
        attributes: ['ownerId', [fn('sum', col('value')), 'total']],
        group: ['ownerId'],
      }),
      Transaction.findAll({
        where: {
          typeId: TYPE_MANUAL_ACTIVATION,
          statusId: STATUS_CONFIRMED,
        },
      }),
    ])

    const vouchersBalance = vouchers.map(voucher => {
      const inputs = Number(voucher.get('total'))
      const outputs = activations.reduce((acc, transaction) => {
        if (
          transaction.txid &&
          transaction.txid.includes(`activador_id=${voucher.ownerId}`)
        )
          acc += Number(transaction.value)
        return acc
      }, 0.0)
      return {
        ownerId: voucher.ownerId,
        balance: inputs - outputs,
      }
    })

    const usersBalance = vouchersBalance.reduce((acc, curr) => {
      if (!acc[curr.ownerId]) acc[curr.ownerId] = 0.0
      acc[curr.ownerId] += curr.balance
      return acc
    }, {})

    const value = Object.keys(usersBalance).map(k => ({
      ownerId: k,
      balance: usersBalance[k],
    }))

    await cacheProvider.save({ key, value })

    return value
  }
}
export default new GetAdminsBalanceService()
