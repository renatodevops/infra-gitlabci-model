import Sequelize, { Model } from 'sequelize'

class Pin extends Model {
  static init(sequelize) {
    super.init(
      {
        name: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'The name can not be empty',
            },
            len: {
              args: [1, 40],
              msg: 'The name must contain less than 40 characters',
            },
          },
        },
        imageLink: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'The image_link can not be empty',
            },
            len: {
              args: [1, 255],
              msg: 'The image_link must contain less than 255 characters',
            },
          },
        },
        pontuation: {
          type: Sequelize.INTEGER,
        },
        premiation: {
          type: Sequelize.DECIMAL,
        },
      },
      { sequelize, tableName: 'pins' }
    )

    return this
  }
}
export default Pin
