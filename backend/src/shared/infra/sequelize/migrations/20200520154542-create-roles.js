module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('roles', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      name: {
        allowNull: false,
        type: DataTypes.STRING(40),
      },
      level: {
        allowNull: false,
        type: DataTypes.INTEGER,
        unique: true,
      },
      description: {
        allowNull: true,
        type: DataTypes.STRING(200),
      },
      active: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      action: { type: DataTypes.STRING, allowNull: true },
      label: { type: DataTypes.STRING, allowNull: true },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('roles')
  },
}
