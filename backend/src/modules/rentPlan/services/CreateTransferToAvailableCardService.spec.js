import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'
import User from '../../../shared/infra/sequelize/models/User'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import { APPLIED, AVAILABLE, VOUCHER } from '../../../util/cardsIdentifiers'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'
import CreateTransferToAvailableCardService from './CreateTransferToAvailableCardService'

let user

describe('CreateTransferToAvailableCardService tests', () => {
  beforeEach(async () => {
    user = await User.create({
      name: 'parsedName',
      phone: 'parsedPhone',
      document: 'parsedDocument',
      email: `${crypto.randomBytes(6).toString('hex')}@${crypto
        .randomBytes(6)
        .toString('hex')}.com`,
      born: new Date(),
      terms: true,
      indicatorId: 1,
      passwordHash: crypto.randomBytes(6).toString('hex'),
      selfieLink: 'selfieLink',
      documentFrontLink: 'selfieLink',
      documentBackLink: 'selfieLink',
    })
  })
  it('should return Limite de recebimento excedido', async () => {
    const result = await CreateTransferToAvailableCardService.handle({
      data: { userId: user.id, transferValue: 100.0, description: 'teste' },
    })
    expect(result).toBe('Limite de recebimento excedido')
  })

  it('should return 100.0', async () => {
    await CreateCardTransactionService.execute({
      cardIdentifier: APPLIED,
      userId: user.id,
      description: 'teste',
      status: CONFIRMED,
      value: 100.0,
    })
    const result = await CreateTransferToAvailableCardService.handle({
      data: { userId: user.id, transferValue: 100.0, description: 'teste' },
    })
    expect(result).toBe(100.0)
  })

  it('should return 100.0 and update applied balance to 0.0', async () => {
    await Promise.all([
      CreateCardTransactionService.execute({
        cardIdentifier: APPLIED,
        userId: user.id,
        description: 'teste',
        status: CONFIRMED,
        value: 100.0,
      }),
      CreateCardTransactionService.execute({
        cardIdentifier: AVAILABLE,
        userId: user.id,
        description: 'teste',
        status: CONFIRMED,
        value: 150.0,
      }),
    ])

    const result = await CreateTransferToAvailableCardService.handle({
      data: { userId: user.id, transferValue: 100.0, description: 'teste' },
    })
    expect(result).toBe(100.0)

    const [appliedBalance, availableBalance] = await Promise.all([
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: APPLIED,
      }),
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: AVAILABLE,
      }),
    ])
    expect(appliedBalance).toBe(0.0)
    expect(availableBalance).toBe(250.0)
  })

  it('should return 100.0 and update voucher balance to 0.0', async () => {
    await Promise.all([
      CreateCardTransactionService.execute({
        cardIdentifier: VOUCHER,
        userId: user.id,
        description: 'teste',
        status: CONFIRMED,
        value: 100.0,
      }),
      CreateCardTransactionService.execute({
        cardIdentifier: AVAILABLE,
        userId: user.id,
        description: 'teste',
        status: CONFIRMED,
        value: 150.0,
      }),
    ])

    let result = await CreateTransferToAvailableCardService.handle({
      data: { userId: user.id, transferValue: 100.0, description: 'teste' },
    })
    expect(result).toBe(100.0)

    const [voucherBalance, availableBalance] = await Promise.all([
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: VOUCHER,
      }),
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: AVAILABLE,
      }),
    ])
    expect(voucherBalance).toBe(0.0)
    expect(availableBalance).toBe(250.0)

    await Promise.all([
      CreateCardTransactionService.execute({
        cardIdentifier: APPLIED,
        userId: user.id,
        description: 'teste',
        status: CONFIRMED,
        value: 100.0,
      }),
      CreateCardTransactionService.execute({
        cardIdentifier: AVAILABLE,
        userId: user.id,
        description: 'teste',
        status: CONFIRMED,
        value: 150.0,
      }),
    ])

    result = await CreateTransferToAvailableCardService.handle({
      data: { userId: user.id, transferValue: 500.0, description: 'teste' },
    })
    expect(result).toBe(100.0)
  })
})
