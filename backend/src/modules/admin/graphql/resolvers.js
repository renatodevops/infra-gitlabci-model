import { Op } from 'sequelize'
import {
  authenticated,
  compose,
  hasAllowed,
} from '../../../shared/infra/http/composers/composables'

import User from '../../../shared/infra/sequelize/models/User'

// import CreditCardBalanceService from '../../balanceCards/services/CreditCardBalanceService'
// import AvailableCardBalanceService from '../../balanceCards/services/AvailableCardBalanceService'
// import AppliedCardBalanceService from '../../balanceCards/services/AppliedCardBalanceService'
// import IncomeCardBalanceService from '../../balanceCards/services/IncomeCardBalanceService'

export default {
  UserWithBalance: {
    deposited: async () => 0.0,
    // (await CreditCardBalanceService.execute({ userId: obj.id })).input,

    withdrawn: async () => 0.0,
    // (await AvailableCardBalanceService.execute({ userId: obj.id })).output,

    indicator: obj => User.findByPk(obj.indicatorId),
  },

  Query: {
    getUser: compose(
      authenticated,
      hasAllowed([3, 4, 5])
    )((parent, args) => User.findByPk(args.userId)),

    getUsers: compose(
      authenticated,
      hasAllowed([3, 4, 5])
    )(async (parent, args) => {
      const {
        limit = 10,
        offset = 1,

        sortField,
        sortDirection,
        where,
      } = args

      const { count: totalCount, rows: node } = await User.findAndCountAll({
        limit,
        offset: (offset - 1) * limit,
        order: [[sortField, sortDirection]],
        where,
      })

      return {
        edge: {
          node,
        },
        pageInfo: {
          totalCount,
          hasPrevPage: offset > 1,
          hasNextPage: totalCount > limit * offset,
        },
      }
    }),

    getAllUsers: compose(
      authenticated,
      hasAllowed([3, 4, 5])
    )(async (_, __, ctx) => {
      return User.findAll({
        where: {
          id: { [Op.ne]: ctx.user.id },
        },
        order: [['name', 'ASC']],
      })
    }),

    getUserWallet: compose(
      authenticated,
      hasAllowed([3, 4, 5])
    )(async () => {
      const [credit, applied, income, available] = await Promise.all([
        // CreditCardBalanceService.execute({ userId }),
        // AppliedCardBalanceService.execute({ userId }),
        // IncomeCardBalanceService.execute({ userId }),
        // AvailableCardBalanceService.execute({ userId }),
      ])

      return {
        available: available.balance,
        credit: credit.balance,
        applied: applied.balance,
        income: income.balance + income.returnedFromAvailable,
      }
    }),
  },
}
