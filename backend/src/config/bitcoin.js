require('dotenv').config({
  path: process.env.NODE_ENV === 'test' ? '.env.testing' : '.env',
})

export default {
  application: process.env.APP_NAME,
  name: process.env.BITCOIN_ACCOUNT_NAME,
  apikey: process.env.BITCOIN_API_KEY,
  connection: {
    version: '0.17.0',
    host: process.env.BITCOIN_HOST,
    port: process.env.BITCOIN_PORT,
    network: process.env.BITCOIN_NETWORK,
    username: process.env.BITCOIN_USERNAME,
    password: process.env.BITCOIN_PASS,
  },
}
