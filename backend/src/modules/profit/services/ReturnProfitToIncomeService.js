// /* eslint-disable import/no-cycle */
// import { Op } from 'sequelize'
// import { subDays } from 'date-fns'
// import Transaction from '../../../shared/infra/sequelize/models/Transaction'
// import User from '../../../shared/infra/sequelize/models/User'
// import AvailableCardBalanceService from '../../balanceCards/services/AvailableCardBalanceService'

// import {
//   STATUS_CONFIRMED,
//   TYPE_PROFIT,
//   TYPE_RETURN_TO_INCOME,
// } from '../../../util/typesTransactions'

// // import { cacheProvider } from '../../../shared/container'
// import IncomeCardBalanceService from '../../balanceCards/services/IncomeCardBalanceService'

class ReturnProfitToIncome {
  get key() {
    return 'ReturnProfitToIncome'
  }

  get schedule() {
    return '0 20 1 * *'
  }

  async handle() {
    // const users = await User.findAll()
    // const usersBalance = await Promise.all(
    //   users.map(async user => {
    //     const { balance } = await AvailableCardBalanceService.execute({
    //       userId: user.id,
    //     })
    //     return {
    //       userId: user.id,
    //       balance,
    //     }
    //   })
    // )
    // const validUsersBalance = usersBalance.filter(ub => ub.balance > 0.0)
    // await Promise.all(
    //   validUsersBalance.map(async ({ userId, balance }) => {
    //     const [lastProfit] = await Transaction.findAll({
    //       where: {
    //         userId,
    //         typeId: TYPE_PROFIT,
    //         statusId: STATUS_CONFIRMED,
    //         createdAt: { [Op.gt]: subDays(new Date(), 10) },
    //       },
    //       limit: 1,
    //       order: [['id', 'DESC']],
    //     })
    //     const valueToReturn = lastProfit
    //       ? Math.min(balance, Number(lastProfit.value))
    //       : 0.0
    //     if (valueToReturn > 0.0) {
    //       await Transaction.create({
    //         userId,
    //         value: valueToReturn,
    //         statusId: STATUS_CONFIRMED,
    //         typeId: TYPE_RETURN_TO_INCOME,
    //       })
    //       await IncomeCardBalanceService.updateCache(userId)
    //       await AvailableCardBalanceService.updateCache(userId)
    //     }
    //   })
    // )
  }
}
export default new ReturnProfitToIncome()
