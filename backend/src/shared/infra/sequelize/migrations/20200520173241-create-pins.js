module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('pins', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      name: {
        unique: true,
        allowNull: false,
        type: DataTypes.STRING(40),
      },
      image_link: {
        allowNull: false,
        type: DataTypes.STRING(255),
      },
      pontuation: {
        allowNull: false,
        type: DataTypes.BIGINT,
      },
      premiation: {
        allowNull: true,
        type: DataTypes.FLOAT,
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('pins')
  },
}
