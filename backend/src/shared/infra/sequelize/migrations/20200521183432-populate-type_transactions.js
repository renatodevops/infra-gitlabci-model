module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'types_transactions',
      [
        {
          // 1
          name: 'Solicitação de depósito BTC',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 2
          name: 'Depósito BTC',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 3
          name: 'Bônus indicação 1º nível',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 4
          name: 'Bônus indicação 2º nível',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 5
          name: 'Bônus indicação 3º nível',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 6
          name: 'Bônus indicação 4º nível',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 7
          name: 'Bônus indicação 5º nível',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 8
          name: 'Bônus indicação 6º nível',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 9
          name: 'Bônus indicação 7º nível',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 10
          name: 'Bônus indicação 8º nível',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 11
          name: 'Bônus indicação 9º nível',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 12
          name: 'Bônus indicação 10º nível',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 13
          name: 'Finalização de Aplicação',
          description: 'some description',
          active: true,
          is_input: false,
        },
        {
          // 14
          name: 'Entrada de Rendimento',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 15
          name: 'Saída de rendimento',
          description: 'income out',
          active: true,
          is_input: false,
        },
        {
          // 16
          name: 'Pagamento de Rendimento',
          description: 'some description',
          active: true,
          is_input: true,
        },

        {
          // 17
          name: 'Bônus residual',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 18
          name: 'Saque BTC',
          description: 'some description',
          active: true,
          is_input: false,
        },
        {
          // 19
          name: 'Premiação Plano de Carreira',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 20
          name: 'Ativação de voucher',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 21
          name: 'Ativação de saldo manual',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 22
          name: 'Saída do crédito',
          description: 'some description',
          active: true,
          is_input: false,
        },
        {
          // 23
          name: 'Entrada do Applicado',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 24
          name: 'Cancelamento de voucher',
          description: 'some description',
          active: true,
          is_input: false,
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('types_transactions', null, {})
  },
}
