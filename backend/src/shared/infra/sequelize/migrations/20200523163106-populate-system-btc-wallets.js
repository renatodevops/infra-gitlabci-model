module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'system_btc_wallets',
      [
        {
          // 1
          name: 'deposit.dat',
          label: 'depositBtcWallet',
          is_operable: true,
          active: true,
        },
        {
          // 2
          name: 'payment.dat',
          label: 'paymentBtcWallet',
          is_operable: true,
          active: true,
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('system_btc_wallets', null, {})
  },
}
