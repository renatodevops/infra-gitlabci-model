import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'
import User from '../../../shared/infra/sequelize/models/User'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import { AVAILABLE } from '../../../util/cardsIdentifiers'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import CreateUserAutoRenovationService from './CreateUserAutoRenovationService'
import UserAutoRenovationSetting from '../../../shared/infra/sequelize/models/UserAutoRenovationSetting'
import BCryptHashProvider from '../../../shared/providers/HashProvider/BCryptHashProvider'
import UserRole from '../../../shared/infra/sequelize/models/UserRole'

let user

describe('CreateUserAutoRenovationService tests', () => {
  beforeEach(async () => {
    user = await User.create({
      name: 'parsedName',
      phone: 'parsedPhone',
      document: 'parsedDocument',
      email: `${crypto.randomBytes(6).toString('hex')}@${crypto
        .randomBytes(6)
        .toString('hex')}.com`,
      born: new Date(),
      terms: true,
      indicatorId: 1,
      passwordHash: await BCryptHashProvider.generateHash('12345678'),
      selfieLink: 'selfieLink',
      documentFrontLink: 'selfieLink',
      documentBackLink: 'selfieLink',
    })
    await UserRole.create({
      userId: user.id,
      roleId: 1,
      active: true,
    })
  })
  it('should return Renovação automatica desativada', async () => {
    const result = await CreateUserAutoRenovationService.execute({
      userId: user.id,
      surplusValue: 100,
      description: 'teste',
    })
    expect(result).toBe('Renovação automatica desativada')
  })
  it('should create an auto renovation', async () => {
    await UserAutoRenovationSetting.create({ userId: user.id, isActive: true })
    await CreateCardTransactionService.execute({
      cardIdentifier: AVAILABLE,
      userId: user.id,
      description: 'teste',
      status: CONFIRMED,
      value: 100.0,
    })
    const {
      isAutoRenovationActive,
      availableBalance,
    } = await CreateUserAutoRenovationService.execute({
      userId: user.id,
      surplusValue: 100,
      description: 'teste',
    })
    expect(isAutoRenovationActive).toBe(true)
    expect(availableBalance).toBe(100.0)
  })
})
