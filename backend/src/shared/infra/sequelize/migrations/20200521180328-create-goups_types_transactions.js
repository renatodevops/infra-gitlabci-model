module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('groups_types_transactions', {
      group_id: {
        primaryKey: true,
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: 'groups_transactions',
          key: 'id',
        },
      },
      type_id: {
        primaryKey: true,
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: 'types_transactions',
          key: 'id',
        },
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('groups_types_transactions')
  },
}
