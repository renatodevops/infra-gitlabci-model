/* eslint-disable import/no-cycle */
import { isAfter, startOfMonth, format, subMonths } from 'date-fns'
import User from '../../../shared/infra/sequelize/models/User'
import { AVAILABLE } from '../../../util/cardsIdentifiers'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import GetAnnualPlanInputsService from './GetAnnualPlanInputsService'
// import AnnualPlanRate from '../../../shared/infra/sequelize/models/AnnualPlanRate'
import convertUTCtoLocalTZ from '../../../util/convertUTCtoLOCALTZ'
import UserAnnualPlan from '../../../shared/infra/sequelize/models/UserAnnualPlan'

class CreateMonthlyIncomeAnnualPlanService {
  get key() {
    return 'CreateMonthlyIncomeAnnualPlanService'
  }

  get schedule() {
    return '20 0 1 * *'
  }

  async handle() {
    const users = await User.findAll()

    const ids = users.map(user => user.id)

    const annualPlanInputs = await Promise.all(
      ids.map(id => GetAnnualPlanInputsService.execute({ userId: id }))
    )

    const annualPlanInputsSerialized = annualPlanInputs.reduce((acc, curr) => {
      acc.push(...curr)
      return acc
    }, [])

    // const monthRates = await AnnualPlanRate.findAll()

    const lastMonth = subMonths(convertUTCtoLocalTZ(), 1)

    const startLastMonth = startOfMonth(lastMonth)

    const incomeValues = await Promise.all(
      annualPlanInputsSerialized.map(async transaction => {
        const created = convertUTCtoLocalTZ(transaction.createdAt)

        const userAnnualPlan = await UserAnnualPlan.findOne({
          where: {
            cardTransactionId: transaction.id,
          },
        })

        const userFullRate = userAnnualPlan
          ? Number(userAnnualPlan.userRate)
          : 0.0

        const indicatorFullRateFullRate = userAnnualPlan
          ? Number(userAnnualPlan.indicatorRate)
          : 0.0

        const isCreatedLastMonth = isAfter(created, startLastMonth)

        const proportionalRate = isCreatedLastMonth
          ? (31 - created.getDate()) / 31
          : 1.0

        const userIncomeValue =
          userFullRate * proportionalRate * Number(transaction.value)

        const indicatorIncomeValue =
          indicatorFullRateFullRate *
          proportionalRate *
          Number(transaction.value)

        return {
          userIncomeValue,
          indicatorIncomeValue,
          userId: transaction.userId,
          indicatorId: userAnnualPlan.indicatorId,
          created,
        }
      })
    )

    return Promise.all(
      incomeValues.map(
        async ({
          userId,
          userIncomeValue,
          created,
          indicatorId,
          indicatorIncomeValue,
        }) => {
          const userIncome = await CreateCardTransactionService.execute({
            userId,
            value: userIncomeValue,
            status: CONFIRMED,
            cardIdentifier: AVAILABLE,
            description: `Rendimento Plano Anual - ${format(
              created,
              'MM/yyyy'
            )}`,
          })
          const indicatorComission = await CreateCardTransactionService.execute(
            {
              userId: indicatorId,
              value: indicatorIncomeValue,
              status: CONFIRMED,
              cardIdentifier: AVAILABLE,
              description: `Comissão de ${
                users.find(u => u.id === userId).name
              } - ${format(created, 'MM/yyyy')}`,
            }
          )

          return {
            userIncome,
            indicatorComission,
          }
        }
      )
    )
  }
}
export default new CreateMonthlyIncomeAnnualPlanService()
