// import { Op } from 'sequelize'
// import { format } from 'date-fns-tz'

// import Transaction from '../../../shared/infra/sequelize/models/Transaction'
// import StrategyPlanning from '../../../shared/infra/sequelize/models/StrategyPlanning'
// import User from '../../../shared/infra/sequelize/models/User'
// // import AppliedCardBalanceService from '../../balanceCards/services/AppliedCardBalanceService'

// import {
//   TYPE_INCOME_IN,
//   STATUS_CONFIRMED,
// } from '../../../util/typesTransactions'

// import scheduleConfig from '../../../config/schedule'

// import IncomeCardBalanceService from '../../balanceCards/services/IncomeCardBalanceService'

class AccumulateProfitService {
  get key() {
    return 'AccumulateProfitService'
  }

  get schedule() {
    return '0 20 * * *'
  }

  async handle() {
    // const todayPlannings = await StrategyPlanning.findAll({
    //   where: {
    //     date: format(new Date(), 'yyyy-MM-dd', {
    //       timeZone: scheduleConfig.timezone,
    //     }),
    //     confirmed: false,
    //     dailyPlanned: { [Op.gt]: 0.0 },
    //   },
    //   include: ['strategy'],
    // })
    // const users = await User.findAll()
    // const usersWithAppliedBalance = await Promise.all(
    //   users.map(async user => {
    //     const {
    //       balance,
    //       voucherBalance,
    //     } = await AppliedCardBalanceService.execute({
    //       userId: user.id,
    //     })
    //     return {
    //       id: user.id,
    //       applied: balance - voucherBalance,
    //     }
    //   })
    // )
    // const usersWithAppliedBalanceGreaterThanZero = usersWithAppliedBalance.filter(
    //   user => user.applied > 0.0
    // )
    // await Promise.all(
    //   todayPlannings.map(async plan => {
    //     const dailyPlanned = Number(plan.dailyPlanned)
    //     await Promise.all(
    //       usersWithAppliedBalanceGreaterThanZero.map(async user => {
    //         const income = dailyPlanned * user.applied
    //         if (income > 0.0) {
    //           await Transaction.create({
    //             value: income,
    //             typeId: TYPE_INCOME_IN,
    //             userId: user.id,
    //             statusId: STATUS_CONFIRMED,
    //           })
    //           await IncomeCardBalanceService.updateCache(user.id)
    //         }
    //       })
    //     )
    //     await plan.update({ isEditable: false, confirmed: true })
    //   })
    // )
  }
}

export default new AccumulateProfitService()
