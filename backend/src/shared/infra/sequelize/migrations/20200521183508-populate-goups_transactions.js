module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'groups_transactions',
      [
        {
          // 1
          name: 'Crédito',
        },
        {
          // 2
          name: 'Aplicado',
        },
        {
          // 3
          name: 'Renda',
        },
        {
          // 4
          name: 'Disponível',
        },
        {
          // 5
          name: 'Retirado',
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('groups_transactions', null, {})
  },
}
