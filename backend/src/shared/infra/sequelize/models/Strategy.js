import Sequelize, { Model } from 'sequelize'

class Strategy extends Model {
  static init(sequelize) {
    super.init(
      {
        name: {
          type: Sequelize.STRING,
        },
        description: {
          type: Sequelize.STRING,
        },
        active: Sequelize.BOOLEAN,
        typeIncomeTransactionId: Sequelize.BIGINT,
      },
      { sequelize, tableName: 'strategies' }
    )

    return this
  }
}
export default Strategy
