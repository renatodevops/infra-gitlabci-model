import { Op } from 'sequelize'
import { validate } from 'wallet-address-validator'
import { format } from 'date-fns'
import { depositBtcWallet } from '../../../shared/container'
import {
  authenticated,
  compose,
  hasAllowed,
  verifyFinancialPassword,
} from '../../../shared/infra/http/composers/composables'
import BtcTransaction from '../../../shared/infra/sequelize/models/BtcTransaction'
import User from '../../../shared/infra/sequelize/models/User'
import Bee from '../../../shared/providers/QueueProvider/Bee'
import {
  AGUARDANDO_PROCESSAMENTO,
  AWAITING_FOR_PAYMENT,
  CONFIRMED,
} from '../../../util/btcTransactionsStatus'

import CreateUserBtcPaymentService from '../services/CreateUserBtcPaymentService'
import CreateUserBtcWithdrawalService from '../services/CreateUserBtcWithdrawalService'
import GetWithdrawalTimeService from '../services/GetWithdrawalTimeService'
import ProcessBlocknotificationService from '../services/ProcessBlocknotificationService'
import ProcessTxidNotificationService from '../services/ProcessTxidNotificationService'
import ProcessUsersBtcWithdrawalsService from '../services/ProcessUsersBtcWithdrawalsService'

export default {
  BtcTransaction: {
    user: obj => User.findByPk(obj.userId),
    amount: obj => depositBtcWallet.ExchangeToBTC(obj.value),
  },
  Mutation: {
    CreateUserBtcPayment: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, { value }, ctx) => {
      const payment = await CreateUserBtcPaymentService.execute({
        value,
        userId: ctx.user.id,
      })
      return payment
    }),

    async CreateTxidNotification(parent, { txid }) {
      await Bee.add(ProcessTxidNotificationService.key, {
        txid,
      })
      return `${process.env.APP_NAME}-${txid}`
    },

    async CreateBlockNotification(parent, { blockHash }) {
      await Bee.add(ProcessBlocknotificationService.key, {
        blockhash: blockHash,
      })
      return `${process.env.APP_NAME}-${blockHash}`
    },

    CreateUserBtcWithdrawal: compose(
      authenticated,
      verifyFinancialPassword,
      hasAllowed([1])
    )(async (_, { value }, ctx) => {
      const result = await CreateUserBtcWithdrawalService.execute({
        userId: ctx.user.id,
        withdrawalValue: value,
      })
      return result
    }),

    ProcessUsersBtcWithdrawals: compose(
      authenticated,
      hasAllowed([5])
    )(async (_, { transactionsIds }) => {
      const data = { transactionsIds }

      const transactions = await BtcTransaction.findAll({
        where: {
          id: transactionsIds,
          category: 'send',
          status: AGUARDANDO_PROCESSAMENTO,
        },
      })

      transactions.forEach(transaction => {
        const valid = validate(transaction.address, 'BTC')
        if (!valid)
          throw Error(
            `Transação #${transaction.id} possui endereço inválido: ${transaction.address}`
          )
      })

      await Bee.add(ProcessUsersBtcWithdrawalsService.key, data)

      await new Promise(resolve => setTimeout(resolve, 2000))

      return 'Saques verificados com sucesso!'
    }),
  },

  Query: {
    getWithdrawalTime: () => {
      return GetWithdrawalTimeService.execute()
    },
    GetUserBtcPayments: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, { offset = 1, limit = 10 }, ctx) => {
      const {
        count: totalCount,
        rows: node,
      } = await BtcTransaction.findAndCountAll({
        where: {
          userId: ctx.user.id,
          status: { [Op.ne]: AWAITING_FOR_PAYMENT },
        },
      })

      return {
        edge: {
          node,
        },
        pageInfo: {
          totalCount,
          hasPrevPage: offset > 1,
          hasNextPage: totalCount > limit * offset,
        },
      }
    }),

    GetUserBtcWithdrawals: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, args, ctx) => {
      const node = await BtcTransaction.findAll({
        where: {
          userId: ctx.user.id,
          category: 'send',
        },
        order: [['id', 'desc']],
      })

      return {
        edge: {
          node,
        },
        pageInfo: {
          totalCount: 0,
          hasPrevPage: false,
          hasNextPage: false,
        },
      }
    }),

    GetUnprocessedUserBtcWithdrawals: compose(
      authenticated,
      hasAllowed([5])
    )(async () => {
      const node = await BtcTransaction.findAll({
        where: {
          category: 'send',
          status: AGUARDANDO_PROCESSAMENTO,
        },
        order: [['id', 'asc']],
      })

      return {
        edge: {
          node,
        },
        pageInfo: {
          totalCount: 0,
          hasPrevPage: false,
          hasNextPage: false,
        },
      }
    }),

    GetProcessedUserBtcWithdrawals: compose(
      authenticated,
      hasAllowed([5])
    )(async () => {
      const node = await BtcTransaction.findAll({
        where: {
          category: 'send',
          status: { [Op.ne]: AGUARDANDO_PROCESSAMENTO },
        },
        order: [['id', 'asc']],
      })

      return {
        edge: {
          node,
        },
        pageInfo: {
          totalCount: 0,
          hasPrevPage: false,
          hasNextPage: false,
        },
      }
    }),

    GetBtcInputsChart: compose(
      authenticated,
      hasAllowed([5])
    )(async () => {
      const transactions = await BtcTransaction.findAll({
        where: {
          category: 'receive',
          status: CONFIRMED,
        },
        order: [['createdAt', 'asc']],
      })

      const inputsSerialized = transactions.map(transaction => {
        return {
          id: transaction.id,
          value: Number(transaction.value),
          amount: Number(transaction.amount),
          date: format(transaction.createdAt, 'yyyy-MM-dd'),
        }
      })

      return inputsSerialized.reduce((acc, curr, index) => {
        if (index === 0) {
          acc.push(curr)
        } else {
          const onSameDay = curr.date === acc[acc.length - 1].date
          if (onSameDay) acc[acc.length - 1].value += curr.value
          else acc.push(curr)
        }
        return acc
      }, [])
    }),

    GetSystemWalletBalance: compose(
      authenticated,
      hasAllowed([5])
    )(async () => {
      const btcBalance = await depositBtcWallet.getBalance()
      const usdBalance = await depositBtcWallet.ExchangeFromBTC(
        Number(btcBalance).toFixed(8)
      )
      return {
        btcBalance: Number(btcBalance),
        usdBalance: Number(usdBalance),
      }
    }),
  },
}
