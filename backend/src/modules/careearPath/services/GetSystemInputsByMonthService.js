import { startOfMonth, endOfMonth } from 'date-fns'
import { Op } from 'sequelize'
import CardTransaction from '../../../shared/infra/sequelize/models/CardTransaction'
import { ANNUAL_PLAN, APPLIED } from '../../../util/cardsIdentifiers'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'

import convertUTCtoLocalTZ from '../../../util/convertUTCtoLOCALTZ'

class GetSystemInputsByMonthService {
  async execute(now = new Date()) {
    return CardTransaction.sum('value', {
      where: {
        value: { [Op.gt]: 0.0 },
        createdAt: {
          [Op.and]: [
            { [Op.gte]: convertUTCtoLocalTZ(startOfMonth(now)) },
            { [Op.lt]: convertUTCtoLocalTZ(endOfMonth(now)) },
          ],
        },
        cardIdentifier: [APPLIED, ANNUAL_PLAN],
        status: CONFIRMED,
      },
    })
  }
}
export default new GetSystemInputsByMonthService()
