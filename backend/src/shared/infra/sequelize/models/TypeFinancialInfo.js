import Sequelize, { Model } from 'sequelize'

class TypeFinancialInfo extends Model {
  static init(sequelize) {
    super.init(
      {
        name: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'The name can not be empty',
            },
            len: {
              args: [1, 45],
              msg: 'The name field must contain less than 45 characters',
            },
          },
        },
        description: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'The description can not be empty',
            },
            len: {
              args: [1, 90],
              msg: 'The description field must contain less than 90 characters',
            },
          },
        },
        active: {
          type: Sequelize.BOOLEAN,
          validate: {
            notEmpty: {
              msg: 'The value active can not be empty',
            },
          },
        },
      },
      { sequelize, tableName: 'types_financial_infos' }
    )
    return this
  }
}
export default TypeFinancialInfo
