import UserDocument from '../../../shared/infra/sequelize/models/UserDocument'
import { SELFIE, ADDRESS, DOCUMENTS } from '../../../util/DocumentTypes'

class CreateUserDocumentService {
  async execute({ userId, selfie, documentFront, documentBack, addressProof }) {
    const docArr = []

    const [selfieDoc, docFront, docBack, adProof] = await Promise.all([
      UserDocument.findOne({
        where: {
          userId,
          documentType: SELFIE,
        },
      }),
      UserDocument.findOne({
        where: {
          userId,
          documentType: DOCUMENTS.front,
        },
      }),
      UserDocument.findOne({
        where: {
          userId,
          documentType: DOCUMENTS.back,
        },
      }),
      UserDocument.findOne({
        where: {
          userId,
          documentType: ADDRESS,
        },
      }),
    ])

    if (selfie) {
      docArr.push(
        selfieDoc
          ? selfieDoc.evaluated && !selfieDoc.approved
            ? selfieDoc.update({
                documentLink: selfie,
                evaluated: false,
                approved: null,
                message: null,
              })
            : UserDocument.findOne({ where: { userId, documentType: SELFIE } })
          : UserDocument.create({
              userId,
              documentLink: selfie,
              documentType: SELFIE,
            })
      )
    }
    if (documentFront) {
      docArr.push(
        docFront
          ? docFront.evaluated && !docFront.approved
            ? docFront.docFront.update({
                documentLink: documentFront,
                evaluated: false,
                approved: null,
                message: null,
              })
            : UserDocument.findOne({
                where: { userId, documentType: DOCUMENTS.front },
              })
          : UserDocument.create({
              userId,
              documentLink: documentFront,
              documentType: DOCUMENTS.front,
            })
      )
    }
    if (documentBack) {
      docArr.push(
        docBack
          ? docBack.evaluated && !docBack.approved
            ? docFront.update({
                documentLink: documentBack,
                evaluated: false,
                approved: null,
                message: null,
              })
            : UserDocument.findOne({
                where: { userId, documentType: DOCUMENTS.back },
              })
          : UserDocument.create({
              userId,
              documentLink: documentBack,
              documentType: DOCUMENTS.back,
            })
      )
    }

    if (addressProof) {
      docArr.push(
        adProof
          ? adProof.evaluated && !adProof.approved
            ? adProof.update({
                documentLink: addressProof,
                evaluated: false,
                approved: null,
                message: null,
              })
            : UserDocument.findOne({
                where: { userId, documentType: ADDRESS },
              })
          : UserDocument.create({
              userId,
              documentLink: addressProof,
              documentType: ADDRESS,
            })
      )
    }

    const userDocuments = await Promise.all(docArr)

    return !!userDocuments
  }
}
export default new CreateUserDocumentService()
