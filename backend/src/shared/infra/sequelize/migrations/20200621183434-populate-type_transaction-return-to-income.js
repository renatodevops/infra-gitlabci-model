module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'types_transactions',
      [
        {
          // 27
          name: 'Retorno para Rendimento',
          description: 'some description',
          active: true,
          is_input: true,
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('types_transactions', null, {})
  },
}
