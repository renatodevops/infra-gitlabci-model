const bcrypt = require('bcryptjs')

module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'users',
      [
        {
          // 1
          name: 'locka',
          email: 'user@teste.com',
          username: 'locka@locka.site',
          password_hash: bcrypt.hashSync('12345678', 8),
          phone: '(84) 99999-9999',
          document: '(84) 99999-9999',
          terms: true,
          country_id: 30,
          user_id: null,
          user_path: '.1.',
        },
        {
          // 2
          name: 'admin',
          email: 'admin@teste.com',
          username: 'locka@locka.com',
          document: 'locka@locka.com',
          password_hash: bcrypt.hashSync('87654321', 8),
          phone: '(84) 99999-9999',
          terms: true,
          country_id: 30,
          user_id: 1,
          user_path: '.1.2.',
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('users', null, {})
  },
}
