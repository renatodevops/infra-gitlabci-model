import Redis from 'ioredis'

import redisConfig from '../../../config/redis'

export default class RedisCacheProvider {
  constructor() {
    this.client = new Redis(redisConfig)
  }

  async save(key, value) {
    await this.client.set(`locka:${key}`, JSON.stringify(value))
  }

  async recover(key) {
    const data = await this.client.get(`locka:${key}`)

    if (!data) return null

    const parsedData = JSON.parse(data)

    return parsedData
  }

  async invalidate(key) {
    await this.client.del(`locka:${key}`)
  }

  async invalidatePrefix(prefix) {
    const keys = await this.client.keys(`locka:${prefix}:*`)

    const pipeline = this.client.pipeline()

    keys.forEach(key => pipeline.del(key))

    await pipeline.exec()
  }

  async resetServerCache() {
    const keys = await this.client.keys(`locka:*`)

    const pipeline = this.client.pipeline()

    keys.forEach(key => pipeline.del(key))

    await pipeline.exec()
  }

  async getValuesByKeyPrefix(prefix) {
    const keys = await this.client.keys(`locka:${prefix}:*`)
    const values = await Promise.all(
      keys.map(async key => {
        const value = await this.recover(key)
        const keySuffix = key.split(':')[1]
        return {
          ...value,
          keySuffix,
        }
      })
    )
    return values
  }
}
// 1
