export default function truncate(models = []) {
  const arrayPromises = models.map(model => {
    return model.truncate({ cascade: true, force: true })
  })

  return Promise.all(arrayPromises)
}
