module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface
      .createTable('users_pins', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.BIGINT,
        },
        user_id: {
          type: DataTypes.BIGINT,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id',
          },
          unique: 'uniqueUserPinTag',
        },
        pin_id: {
          type: DataTypes.BIGINT,
          allowNull: false,
          references: {
            model: 'pins',
            key: 'id',
          },
          unique: 'uniqueUserPinTag',
        },
        created_at: {
          allowNull: false,
          type: DataTypes.DATE,
          defaultValue: new Date(),
        },
        updated_at: {
          allowNull: false,
          type: DataTypes.DATE,
          defaultValue: new Date(),
        },
        deleted_at: {
          allowNull: true,
          type: DataTypes.DATE,
        },
      })
      .then(() => {
        return queryInterface.addConstraint(
          'users_pins',
          ['user_id', 'pin_id'],
          {
            type: 'unique',
            name: 'uniqueUserPinTag',
          }
        )
      })
  },

  down: queryInterface => {
    return queryInterface.dropTable('users_pins')
  },
}
