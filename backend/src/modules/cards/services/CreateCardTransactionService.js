import { cacheProvider } from '../../../shared/container'
import CardTransaction from '../../../shared/infra/sequelize/models/CardTransaction'

class CreateCardTransactionService {
  async execute({ cardIdentifier, userId, status, description, value }) {
    if (!value || value === 0.0) return null
    const cardTransaction = await CardTransaction.create({
      cardIdentifier,
      userId,
      status,
      description,
      value,
    })
    const key = `balance_card:${cardIdentifier}-${userId}`
    const key2 = `user_card_transactions:${userId}`
    await Promise.all([
      cacheProvider.invalidate(key),
      cacheProvider.invalidate(key2),
    ])
    return cardTransaction
  }
}
export default new CreateCardTransactionService()
