module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'roles',
      [
        {
          // 1
          name: 'Usuário',
          level: 1,
          active: true,
        },
        {
          // 2
          name: 'Investidor',
          level: 2,
          active: true,
        },
        {
          // 3
          name: 'Líder',
          level: 3,
          active: true,
        },
        {
          // 4
          name: 'Empregado',
          level: 90,
          active: true,
        },
        {
          // 5
          name: 'Admin',
          level: 95,
          active: true,
        },
        {
          // 6
          name: 'Desenvolvedor',
          level: 99,
          active: true,
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('roles', null, {})
  },
}
