module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'cards',
      [
        {
          name: 'Crédito',
          identifier: 'CREDIT',
          is_visible: true,
        },
        {
          name: 'Locado',
          identifier: 'APPLIED',
          is_visible: true,
        },
        {
          name: 'Rendimento',
          identifier: 'INCOME',
          is_visible: true,
        },
        {
          name: 'Disponível',
          identifier: 'AVAILABLE',
          is_visible: true,
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('cards', null, {})
  },
}
