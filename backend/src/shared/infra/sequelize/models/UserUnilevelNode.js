import Sequelize, { Model } from 'sequelize'

class UserUnilevelNode extends Model {
  static init(sequelize) {
    super.init(
      {
        userId: {
          type: Sequelize.BIGINT,
          primaryKey: true,
        },
        indicatorId: {
          type: Sequelize.BIGINT,
        },
        userCode: Sequelize.STRING,
      },
      { sequelize, tableName: 'users_unilevel_nodes' }
    )
    return this
  }
}
export default UserUnilevelNode
