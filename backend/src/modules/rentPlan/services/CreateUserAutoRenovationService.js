/* eslint-disable import/no-cycle */
import Bee from '../../../shared/providers/QueueProvider/Bee'
import { APPLIED, AVAILABLE } from '../../../util/cardsIdentifiers'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import CreateTransferToAvailableCardService from './CreateTransferToAvailableCardService'
import GetUserAutoRenovationSetting from './GetUserAutoRenovationSetting'
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'

class CreateUserAutoRenovationService {
  async execute({ userId, surplusValue, description }) {
    const isAutoRenovationActive = await GetUserAutoRenovationSetting.execute({
      userId,
    })
    if (!isAutoRenovationActive) return 'Renovação automatica desativada'

    const availableBalance = await GetUserCardBalanceService.execute({
      userId,
      cardIdentifier: AVAILABLE,
    })

    if (availableBalance > 0.0) {
      await CreateCardTransactionService.execute({
        userId,
        cardIdentifier: AVAILABLE,
        status: CONFIRMED,
        description: 'Renovação automática de locação de criptoativo',
        value: availableBalance * -1,
      })
      await CreateCardTransactionService.execute({
        userId,
        cardIdentifier: APPLIED,
        status: CONFIRMED,
        description: 'Renovação automática de locação de criptoativo',
        value: availableBalance,
      })
      await Bee.add(CreateTransferToAvailableCardService.key, {
        userId,
        transferValue: surplusValue,
        description,
      })
    }

    return { isAutoRenovationActive, availableBalance }
  }
}
export default new CreateUserAutoRenovationService()
