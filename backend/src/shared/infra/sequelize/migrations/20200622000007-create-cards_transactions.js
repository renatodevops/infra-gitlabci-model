module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('cards_transactions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      user_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      card_identifier: {
        type: DataTypes.STRING,
        allowNull: false,
        references: {
          model: 'cards',
          key: 'identifier',
        },
      },
      description: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      status: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      value: {
        allowNull: false,
        type: DataTypes.DECIMAL(10, 2),
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('cards_transactions')
  },
}
