import UserFinancialPassword from '../../../shared/infra/sequelize/models/UserFinancialPassword'

class GetUserFinancialPasswordService {
  async execute({ userId }) {
    const userFinancialPassword = await UserFinancialPassword.findOne({
      where: {
        userId,
      },
    })

    return userFinancialPassword && userFinancialPassword.financialPassword
  }
}

export default new GetUserFinancialPasswordService()
