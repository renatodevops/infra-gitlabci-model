import User from '../../../shared/infra/sequelize/models/User'
import BCryptHashProvider from '../../../shared/providers/HashProvider/BCryptHashProvider'

class UpdateUserPasswordService {
  async execute({ userId, currentPassword, newPassword }) {
    const user = await User.findByPk(userId)
    if (!user) throw Error('E-mail ou senha incorretos.')

    const passwordMatched = await BCryptHashProvider.compareHash(
      currentPassword,
      user.passwordHash
    )
    if (!passwordMatched) throw Error('Senha incorreta.')

    if (newPassword.length < 8)
      throw new Error(`Senha deve ter no mínimo 8 caracteres`)

    const passwordHash = await BCryptHashProvider.generateHash(newPassword)

    await user.update({ passwordHash })

    return 'Senha alterada com sucesso'
  }
}
export default new UpdateUserPasswordService()
