import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import truncate from '../../../../__tests__/utils/truncate'

import AnnualPlanRate from '../../../shared/infra/sequelize/models/AnnualPlanRate'

import GetCurrentMonthAnnualPlan from './GetCurrentMonthAnnualPlan'

describe('GetCurrentMonthPercentage tests', () => {
  beforeEach(async () => {
    await truncate([AnnualPlanRate])
  })
  it('should return the object for the current month', async () => {
    const currentRate = await GetCurrentMonthAnnualPlan.execute()
    expect(currentRate).toHaveProperty('rate', '0.0550')
  })
})
