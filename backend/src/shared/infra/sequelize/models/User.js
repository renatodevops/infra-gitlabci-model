import Sequelize, { Model } from 'sequelize'
// import { hashSync, compareSync } from 'bcryptjs'
// import * as jwt from 'jsonwebtoken'

// import authConfig from '../../../../config/auth'

class User extends Model {
  static init(sequelize) {
    super.init(
      {
        code: Sequelize.VIRTUAL,
        name: {
          type: Sequelize.STRING,
        },
        phone: {
          type: Sequelize.STRING,
        },
        userPath: {
          type: Sequelize.STRING,
        },
        document: {
          type: Sequelize.STRING,
        },
        email: {
          type: Sequelize.STRING,
        },
        password: {
          type: Sequelize.VIRTUAL,
        },
        born: {
          type: Sequelize.DATEONLY,
        },
        terms: Sequelize.BOOLEAN,
        indicatorId: {
          field: 'user_id',
          type: Sequelize.BIGINT,
        },
        path: {
          type: Sequelize.VIRTUAL,
          get() {
            const dotExists = this.userPath && this.userPath.indexOf('.') >= 0
            const users = dotExists ? this.userPath.split('.') : [this.id]
            return users.filter(id => {
              return id !== this.id && id !== ''
            })
          },
        },
        passwordHash: Sequelize.STRING,
        selfieLink: Sequelize.STRING,
        documentFrontLink: Sequelize.STRING,
        documentBackLink: Sequelize.STRING,
      },
      {
        sequelize,
      }
    )

    return this
  }

  static associate(models) {
    this.hasMany(models.UserRole, { foreignKey: 'userId', as: 'userRole' })
    this.hasMany(models.User, { foreignKey: 'indicatorId', as: 'indicateds' })
    this.hasMany(models.UserDocument, {
      foreignKey: 'userId',
      as: 'userDocument',
    })
    this.hasOne(models.UserAddress, {
      foreignKey: 'userId',
      as: 'address',
    })
  }

  // checkPassword(password) {
  //   return compareSync(password, this.password_hash)
  // }

  // generateToken({ id = this.id }) {
  //   return jwt.sign({ id }, authConfig.secret)
  // }
}

export default User
