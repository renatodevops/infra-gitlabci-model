// import TypeTransaction from '../../../shared/infra/sequelize/models/TypeTransaction'
// import {
//   TYPE_DEPOSIT_BTC,
//   TYPE_CREDIT_OUT,
//   TYPE_MANUAL_ACTIVATION,
//   TYPE_APPLIED_IN,
//   TYPE_APPLIED_OUT,
//   TYPE_VOUCHER_ACTIVATION,
//   TYPE_VOUCHER_CANCELLATION,
//   TYPE_INCOME_IN,
//   TYPE_INCOME_OUT,
//   TYPE_RETURN_TO_INCOME,
//   TYPE_BONUS_INDICATION_1_LEVEL,
//   TYPE_BONUS_INDICATION_2_LEVEL,
//   TYPE_BONUS_INDICATION_3_LEVEL,
//   TYPE_BONUS_INDICATION_4_LEVEL,
//   TYPE_BONUS_INDICATION_5_LEVEL,
//   TYPE_BONUS_INDICATION_6_LEVEL,
//   TYPE_BONUS_INDICATION_7_LEVEL,
//   TYPE_BONUS_INDICATION_8_LEVEL,
//   TYPE_BONUS_INDICATION_9_LEVEL,
//   TYPE_BONUS_INDICATION_10_LEVEL,
//   TYPE_PROFIT,
//   TYPE_RESIDUAL_BONUS,
//   TYPE_CARRER_PLAN,
//   TYPE_WITHDRAWAL_BTC,
//   TYPE_WITHDRAWAL_REAPLICATION,
// } from '../../../util/typesTransactions'

class GetUserStatmentFilterService {
  async execute() {
    const cards = ['credit', 'applied', 'income', 'available']

    // const types = await TypeTransaction.findAll({
    //   where: {
    //     id: [
    //       TYPE_DEPOSIT_BTC,
    //       TYPE_CREDIT_OUT,
    //       TYPE_MANUAL_ACTIVATION,
    //       TYPE_APPLIED_IN,
    //       TYPE_APPLIED_OUT,
    //       TYPE_VOUCHER_ACTIVATION,
    //       TYPE_VOUCHER_CANCELLATION,
    //       TYPE_INCOME_IN,
    //       TYPE_INCOME_OUT,
    //       TYPE_RETURN_TO_INCOME,
    //       TYPE_BONUS_INDICATION_1_LEVEL,
    //       TYPE_BONUS_INDICATION_2_LEVEL,
    //       TYPE_BONUS_INDICATION_3_LEVEL,
    //       TYPE_BONUS_INDICATION_4_LEVEL,
    //       TYPE_BONUS_INDICATION_5_LEVEL,
    //       TYPE_BONUS_INDICATION_6_LEVEL,
    //       TYPE_BONUS_INDICATION_7_LEVEL,
    //       TYPE_BONUS_INDICATION_8_LEVEL,
    //       TYPE_BONUS_INDICATION_9_LEVEL,
    //       TYPE_BONUS_INDICATION_10_LEVEL,
    //       TYPE_PROFIT,
    //       TYPE_RESIDUAL_BONUS,
    //       TYPE_CARRER_PLAN,
    //       TYPE_WITHDRAWAL_BTC,
    //       TYPE_WITHDRAWAL_REAPLICATION,
    //     ],
    //   },
    // })

    return {
      cards,
      // types: types.map(type => type.name),
      // status: ['confirmed', 'unconfirmed'],
    }
  }
}
export default new GetUserStatmentFilterService()
