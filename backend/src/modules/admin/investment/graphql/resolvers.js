/* eslint-disable no-nested-ternary */
import { parseISO, format, startOfDay, endOfDay } from 'date-fns'
import { col, fn, Op } from 'sequelize'

import {
  authenticated,
  compose,
  hasAllowed,
} from '../../../../shared/infra/http/composers/composables'
import Transaction from '../../../../shared/infra/sequelize/models/Transaction'
import TypeTransaction from '../../../../shared/infra/sequelize/models/TypeTransaction'
import User from '../../../../shared/infra/sequelize/models/User'
import { paymentBtcWallet } from '../../../../shared/container'
// import User from '../../../../shared/infra/sequelize/models/User'
// import AppliedCardBalance from '../../../balanceCards/services/AppliedCardBalanceService'

import {
  TYPE_VOUCHER_ACTIVATION,
  STATUS_CONFIRMED,
  TYPE_DEPOSIT_BTC,
  TYPE_MANUAL_ACTIVATION,
} from '../../../../util/typesTransactions'

export default {
  Query: {
    getInvestmentsFilterOptions: compose(
      authenticated,
      hasAllowed([3, 4, 5])
    )(async () => {
      const transactions = await Transaction.findAll({
        where: {
          typeId: [
            TYPE_VOUCHER_ACTIVATION,
            TYPE_DEPOSIT_BTC,
            TYPE_MANUAL_ACTIVATION,
          ],
          statusId: STATUS_CONFIRMED,
        },
      })
      return transactions
    }),

    getInvestments: compose(
      authenticated,
      hasAllowed([3, 4, 5])
    )(async (_, args) => {
      const {
        limit = 10,
        offset = 1,
        sortField,
        sortDirection,
        where,
        initialDate = new Date(2020, 1, 1),
        finalDate = new Date(),
      } = args

      const [model, field] = sortField.split('.')

      const formattedWhere = JSON.parse(where)

      const serializedWhere = Object.keys(formattedWhere).reduce(
        (result, key) => {
          if (key.includes('.')) result[`$${key}$`] = formattedWhere[key]
          else result[key] = formattedWhere[key]

          return result
        },
        {}
      )

      const order =
        model === 'user'
          ? [[{ model: User, as: 'user' }, field, sortDirection]]
          : model === 'type'
          ? [[{ model: TypeTransaction, as: 'type' }, field, sortDirection]]
          : [[model, sortDirection]]

      const {
        count: totalCount,
        rows: investments,
      } = await Transaction.findAndCountAll({
        where: {
          ...serializedWhere,
          createdAt: {
            [Op.between]: [
              startOfDay(new Date(initialDate)),
              endOfDay(new Date(finalDate)),
            ],
          },
          typeId: [
            TYPE_VOUCHER_ACTIVATION,
            TYPE_DEPOSIT_BTC,
            TYPE_MANUAL_ACTIVATION,
          ],
          statusId: STATUS_CONFIRMED,
        },
        limit,
        offset: (offset - 1) * limit,
        include: [
          {
            model: User,
            as: 'user',
          },
          {
            model: TypeTransaction,
            as: 'type',
          },
        ],
        order,
      })

      const transactions = await Transaction.findAll({
        where: {
          ...serializedWhere,
          createdAt: {
            [Op.between]: [
              startOfDay(new Date(initialDate)),
              endOfDay(new Date(finalDate)),
            ],
          },
          typeId: [
            TYPE_VOUCHER_ACTIVATION,
            TYPE_DEPOSIT_BTC,
            TYPE_MANUAL_ACTIVATION,
          ],
          statusId: STATUS_CONFIRMED,
        },
        include: [
          {
            model: User,
            as: 'user',
          },
          {
            model: TypeTransaction,
            as: 'type',
          },
        ],
        order,
      })

      const totalValue = transactions.reduce((acc, curr) => {
        acc += Number(curr.value)
        return acc
      }, 0.0)

      const totalBtc = await paymentBtcWallet.ExchangeToBTC(
        Number(totalValue || 0),
        'USD'
      )

      const node = investments.map(transaction => ({
        id: transaction.id,
        name: transaction.user.name,
        type: transaction.type.name,
        invested: transaction.value,
        createdAt: transaction.createdAt,
      }))

      return {
        edge: { node },
        pageInfo: {
          totalCount,
          hasPrevPage: offset > 1,
          hasNextPage: totalCount > limit * offset,
        },
        totalValue: totalValue.toFixed(2),
        totalBtc: totalBtc.toFixed(8),
      }
    }),

    getActivationsByDate: compose(
      authenticated,
      hasAllowed([3, 4, 5])
    )(async () => {
      // const { month } = args

      const [
        investmentsByVoucher,
        investmentsByMoney,
        investmentsByManualActivation,
      ] = await Promise.all([
        Transaction.findAll({
          where: {
            typeId: TYPE_VOUCHER_ACTIVATION,
            statusId: STATUS_CONFIRMED,
          },
          attributes: [
            [fn('DATE_TRUNC', 'day', col('created_at')), 'date'],
            [fn('count', col('id')), 'total'],
          ],
          group: ['date'],
        }),
        Transaction.findAll({
          where: {
            typeId: [TYPE_DEPOSIT_BTC],
            statusId: STATUS_CONFIRMED,
          },
          attributes: [
            [fn('DATE_TRUNC', 'day', col('created_at')), 'date'],
            [fn('count', col('id')), 'total'],
          ],
          group: ['date'],
        }),
        Transaction.findAll({
          where: {
            typeId: [TYPE_MANUAL_ACTIVATION],
            statusId: STATUS_CONFIRMED,
          },
          attributes: [
            [fn('DATE_TRUNC', 'day', col('created_at')), 'date'],
            [fn('count', col('id')), 'total'],
          ],
          group: ['date'],
        }),
      ])

      const investmentsByVoucherMapped = investmentsByVoucher.map(
        investment => ({
          slug: 'investment-voucher',
          name: 'Investment Voucher',
          date: format(parseISO(investment.date.toISOString()), 'yyyy-MM-dd'),
          value: investment.total,
        })
      )

      const investmentsByMoneyMapped = investmentsByMoney.map(investment => ({
        slug: 'investment-money',
        name: 'Investment Money',
        date: format(parseISO(investment.date.toISOString()), 'yyyy-MM-dd'),
        value: investment.total,
      }))

      const investmentsByManualMapped = investmentsByManualActivation.map(
        investment => ({
          slug: 'investment-manual',
          name: 'Investment Manual',
          date: format(parseISO(investment.date.toISOString()), 'yyyy-MM-dd'),
          value: investment.total,
        })
      )

      const investmentsMerged = [
        ...investmentsByVoucherMapped,
        ...investmentsByMoneyMapped,
        ...investmentsByManualMapped,
      ]

      const investmentsMergedOrdered = investmentsMerged.sort(
        (a, b) => parseISO(a.date) > parseISO(b.date)
      )

      const usersIndexed = investmentsMergedOrdered.reduce((accu, curr) => {
        const { slug, name, value } = curr

        ;(accu[curr.date] = accu[curr.date] || []).push({ slug, name, value })

        return accu
      }, {})

      const usersFormatted = Object.keys(usersIndexed).map(date => ({
        date,
        data: usersIndexed[date],
      }))

      return usersFormatted
    }),
  },
}
