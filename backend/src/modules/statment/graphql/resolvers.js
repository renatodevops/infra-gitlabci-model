/* eslint-disable no-unused-vars */
import { Op } from 'sequelize'

// import { endOfDay, startOfDay } from 'date-fns'

import {
  authenticated,
  compose,
  hasAllowed,
} from '../../../shared/infra/http/composers/composables'

import User from '../../../shared/infra/sequelize/models/User'
import Transaction from '../../../shared/infra/sequelize/models/Transaction'
import TransactionInfo from '../../../shared/infra/sequelize/models/TransactionInfo'
import TypeTransaction from '../../../shared/infra/sequelize/models/TypeTransaction'
import StatusTransaction from '../../../shared/infra/sequelize/models/StatusTransaction'
import GroupTransaction from '../../../shared/infra/sequelize/models/GroupTransaction'

import {
  TYPE_BONUS_INDICATION_1_LEVEL,
  TYPE_BONUS_INDICATION_2_LEVEL,
  TYPE_BONUS_INDICATION_3_LEVEL,
  TYPE_BONUS_INDICATION_4_LEVEL,
  TYPE_BONUS_INDICATION_5_LEVEL,
  TYPE_BONUS_INDICATION_6_LEVEL,
  TYPE_BONUS_INDICATION_7_LEVEL,
  TYPE_BONUS_INDICATION_8_LEVEL,
  TYPE_BONUS_INDICATION_9_LEVEL,
  TYPE_BONUS_INDICATION_10_LEVEL,
  TYPE_PROFIT,
  TYPE_RESIDUAL_BONUS,
  TYPE_CARRER_PLAN,
  TYPE_WITHDRAWAL_BTC,
  TYPE_RETURN_TO_INCOME,
} from '../../../util/typesTransactions'
import GetUserStatmentFilterService from '../services/GetUserStatmentFilterService'
import GetUserStatmentService from '../services/GetUserStatmentService'
import { paymentBtcWallet } from '../../../shared/container'

export default {
  Transaction: {
    user: obj => User.findByPk(obj.userId),
    type: obj => TypeTransaction.findByPk(obj.typeId),
    status: obj => StatusTransaction.findByPk(obj.statusId),
    txid: obj => {
      const { txid } = obj
      if (!txid) return null
      return `https://www.blockchain.com/btc/tx/${txid.split('#')[0]}`
    },

    info: obj => TransactionInfo.findByPk(obj.id),
  },
  TransactionInfo: {
    amount: async obj => {
      const transaction = await Transaction.findByPk(obj.transactionId)
      if (Number(transaction.typeId) === TYPE_WITHDRAWAL_BTC) {
        const amount = await paymentBtcWallet.ExchangeToBTC(
          Number(transaction.value) - Number(transaction.fee),
          'USD'
        )
        return amount
      }
      return obj.amount
    },
  },

  Query: {
    getGroupsTransactions: compose(
      authenticated,
      hasAllowed([1])
    )(async () => GroupTransaction.findAll()),

    getUserStatmentFilter: compose(
      authenticated,
      hasAllowed([1, 60])
    )(async () => GetUserStatmentFilterService.execute()),

    getMyTransactions: compose(
      authenticated,
      hasAllowed([1, 60])
    )(async (_, args, ctx) => {
      const {
        sortField = 'created',
        sortDirection = 'DESC',
        limit = 10,
        offset = 1,
        where,
      } = args
      const userId = ctx.user.id

      const parsedWhere = JSON.parse(where)

      const sortFieldMap = {
        created: 'createdAt',
        value: 'value',
      }

      const allCards = ['credit', 'applied', 'income', 'available']

      const cards = parsedWhere.card ? [parsedWhere.card] : allCards

      return GetUserStatmentService.execute({
        userId,
        limit,
        offset,
        order: [[sortFieldMap[sortField], sortDirection]],
        cards,
      })
    }),

    getTransactionsByUser: compose(
      authenticated,
      hasAllowed([5])
    )(async (_, args, ctx) => {
      const {
        sortField = 'created',
        sortDirection = 'DESC',
        limit = 10,
        offset = 1,
        userId,
        where,
      } = args

      const parsedWhere = JSON.parse(where)

      const sortFieldMap = {
        created: 'createdAt',
        value: 'value',
      }

      const allCards = ['credit', 'applied', 'income', 'available']

      const cards = parsedWhere.card ? [parsedWhere.card] : allCards

      return GetUserStatmentService.execute({
        userId,
        limit,
        offset,
        order: [[sortFieldMap[sortField], sortDirection]],
        cards,
      })
    }),
  },
}
