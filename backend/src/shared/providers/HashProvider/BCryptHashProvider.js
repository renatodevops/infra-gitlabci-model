import { hash, compare } from 'bcryptjs'

class BCryptHashProvider {
  async generateHash(payload) {
    return hash(payload, 8)
  }

  async compareHash(payload, hashed) {
    return compare(payload, hashed)
  }
}

export default new BCryptHashProvider()
