/* eslint-disable no-unused-vars */
import crypto from 'crypto'
import bcrypt from 'bcryptjs'
import { Op } from 'sequelize'
import { validate } from 'wallet-address-validator'

import {
  authenticated,
  compose,
  hasAllowed,
  verifyFinancialPassword,
} from '../../../shared/infra/http/composers/composables'

import User from '../../../shared/infra/sequelize/models/User'
import UserDataUpdate from '../../../shared/infra/sequelize/models/UserDataUpdate'
import UserFinancialInfo from '../../../shared/infra/sequelize/models/UserFinancialInfo'
import TypeFinancialInfo from '../../../shared/infra/sequelize/models/TypeFinancialInfo'

import MailProduction from '../../../shared/providers/MailProvider/production'
import MailDevelopment from '../../../shared/providers/MailProvider/development'
import UpdateUserPasswordService from '../../users/services/UpdateUserPasswordService'

const TYPE_DATA_FINANCIAL_PASSWORD_CREATION_ID = 1
const TYPE_DATA_UPDATE_RECOVERY_PASSWORD_ID = 2

const TYPE_FINANCIAL_INFO_PASSWORD = 1
const TYPE_FINANCIAL_INFO_BTC_WALLET = 2

export default {
  FinancialInfo: {
    type: obj => TypeFinancialInfo.findByPk(obj.typeId),
    user: obj => User.findByPk(obj.userId),
  },

  Query: {
    hasBitcoinWallet: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) => {
      const TYPE_BTC_WALLET = 2
      return !!(await UserFinancialInfo.findOne({
        where: { userId: ctx.user.id, typeId: TYPE_BTC_WALLET },
      }))
    }),

    getMyFinancialInfos: compose(
      authenticated,
      hasAllowed([1])
    )((_, __, ctx) =>
      UserFinancialInfo.findAll({
        where: {
          userId: ctx.user.id,
          typeId: { [Op.ne]: 1 },
        },
      })
    ),

    getTypesFinancialInfos: compose(
      authenticated,
      hasAllowed([1])
    )((_, __, ctx) =>
      TypeFinancialInfo.findAll({
        where: { active: true },
      })
    ),
  },

  Mutation: {
    CreateBtcWallet: compose(
      authenticated,
      verifyFinancialPassword,
      hasAllowed([1])
    )(async (_, { input: { value } }, ctx) => {
      const valid = validate(value, 'BTC')
      if (!valid) throw Error('Não é uma carteira btc válida')

      return UserFinancialInfo.create({
        userId: ctx.user.id,
        value,
        typeId: TYPE_FINANCIAL_INFO_BTC_WALLET,
      })
    }),

    UpdateBtcWallet: compose(
      authenticated,
      verifyFinancialPassword,
      hasAllowed([1])
    )(async (_, { value }, ctx) => {
      const valid = validate(value, 'BTC')
      if (!valid) throw Error('Não é uma carteira btc válida')

      const btcWallet = await UserFinancialInfo.findOne({
        where: {
          typeId: TYPE_FINANCIAL_INFO_BTC_WALLET,
          userId: ctx.user.id,
        },
      })
      if (!btcWallet) throw Error('Carteira Btc não encontrada')
      await btcWallet.update({
        value,
      })
      return btcWallet
    }),

    RecoveryFinancialPassword: compose(authenticated)(async (_, __, ctx) => {
      const financialInfo = await UserFinancialInfo.findOne({
        where: { userId: ctx.user.id, typeId: TYPE_FINANCIAL_INFO_PASSWORD },
        include: ['user'],
      })

      if (!financialInfo) throw Error('Senha financeira não encontrada')

      const { user } = financialInfo

      const code = await crypto
        .randomBytes(16)
        .toString('hex')
        .slice(0, 10)

      await UserDataUpdate.create({
        code,
        active: true,
        userId: ctx.user.id,
        typeId: TYPE_DATA_UPDATE_RECOVERY_PASSWORD_ID,
      })

      // try {
      //   const data = {
      //     name: user.name,
      //     email: user.email,
      //     code,
      //   }
      //   if (process.env.REDIS_IN_USE === 'true')
      //     Queue.add(RecoveryFinancialPasswordMailJob.key, data)
      //   else RecoveryFinancialPasswordMailJob.handle({ data })
      //   return `Código enviado com sucesso ao email: ${user.email}`
      // } catch (err) {
      //   throw Error(
      //     'Serviço indisponível no momento, tente novamente mais tarde.'
      //   )
      // }
    }),

    setNewFinancialPassword: compose(authenticated)(
      async (_, { code, newPassword }, ctx) => {
        const dataUpdate = await UserDataUpdate.findOne({
          where: {
            userId: ctx.user.id,
            typeId: TYPE_DATA_UPDATE_RECOVERY_PASSWORD_ID,
            active: true,
            code,
          },
        })
        if (!dataUpdate) throw Error('Código não encontrado')

        if (dataUpdate.expiration < new Date())
          throw Error('Solicitação expirada, solicite uma nova')

        const financialInfo = await UserFinancialInfo.findOne({
          where: { userId: ctx.user.id, typeId: TYPE_FINANCIAL_INFO_PASSWORD },
        })

        if (!financialInfo) throw Error('Senha financeira não encontrada')

        const value = bcrypt.hashSync(newPassword, 8)

        await financialInfo.update({ value })

        await dataUpdate.update({ active: false })

        return 'Senha financeira atualizada com sucesso'
      }
    ),

    ChangeFinancialPassword: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, { currentPassword, newPassword }, ctx) => {
      const financialPass = await UserFinancialInfo.findOne({
        where: { userId: ctx.user.id, typeId: TYPE_FINANCIAL_INFO_PASSWORD },
      })

      if (!financialPass.checkFinancialPassword(currentPassword))
        throw Error('Senha financeira incorreta')

      const value = bcrypt.hashSync(newPassword, 8)
      await financialPass.update({ value })
      return 'Senha financeira atualizada com sucesso'
    }),

    ChangePassword: compose(
      authenticated,
      hasAllowed([1])
    )(async (root, { currentBanana, newBanana }, ctx) => {
      const userId = ctx.user.id
      return UpdateUserPasswordService.execute({
        userId,
        currentPassword: currentBanana,
        newPassword: newBanana,
      })
    }),

    RecoveryUserPassword: async (root, { email }) => {
      const user = await User.findOne({ where: { email } })
      if (!user) throw Error('User not found')

      const raw = await crypto.randomBytes(16)
      const code = raw.toString('hex').slice(0, 10)

      await UserDataUpdate.create({
        code,
        active: true,
        userId: user.id,
        typeId: TYPE_DATA_UPDATE_RECOVERY_PASSWORD_ID,
      })

      const data = {
        to: `${user.name} <${user.email}>`,
        subject: 'Solicitação de recuperação de senha',
        template: 'recovery_password',
        context: {
          name: user.name,
          code,
        },
      }

      if (process.env.DEVELOPMENT === 'true')
        await MailDevelopment.sendMail(data)
      else await MailProduction.sendMail(data)

      return `Código enviado com sucesso ao email: ${user.email}`
    },

    SetNewPassword: async (root, { email, code, newPassword }) => {
      const user = await User.findOne({ where: { email } })

      const dataUpdate = await UserDataUpdate.findOne({
        where: {
          userId: user.id,
          typeId: TYPE_DATA_UPDATE_RECOVERY_PASSWORD_ID,
          active: true,
          code,
        },
      })

      if (!dataUpdate) throw Error('Solicitação de recuperação não encontrada')

      if (dataUpdate.expiration < new Date())
        throw Error('Solicitação expirada, solicite uma nova')

      await user.update({ password: newPassword })

      await dataUpdate.update({ active: false })

      return 'Senha atualizada com sucesso'
    },
  },
}
