module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'types_transactions',
      [
        {
          // 25
          name: 'Saque para reaplicação',
          description: 'some description',
          active: true,
          is_input: false,
        },
        {
          // 26
          name: 'Saída de Reaplicação',
          description: 'some description',
          active: true,
          is_input: false,
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('types_transactions', null, {})
  },
}
