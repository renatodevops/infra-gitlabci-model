import { cacheProvider } from '../../../shared/container'
import UserAutoRenovationSetting from '../../../shared/infra/sequelize/models/UserAutoRenovationSetting'

class GetUserAutoRenovationSetting {
  async execute({ userId }) {
    const key = `user_renovation_setting:${userId}`

    let renovationSetting = await cacheProvider.recover(key)

    if (!renovationSetting) {
      renovationSetting = await UserAutoRenovationSetting.findOne({
        where: { userId },
      })
      if (!renovationSetting)
        renovationSetting = await UserAutoRenovationSetting.create({
          userId,
          isActive: false,
        })

      await cacheProvider.save(key, renovationSetting.dataValues)
    }

    return renovationSetting.isActive
  }
}
export default new GetUserAutoRenovationSetting()
