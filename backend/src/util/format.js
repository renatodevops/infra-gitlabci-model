export function formatSlug(string) {
  return String(string)
    .toLowerCase()
    .replace(/\s/g, '-')
}
