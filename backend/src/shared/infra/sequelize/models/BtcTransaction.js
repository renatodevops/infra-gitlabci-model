import Sequelize, { Model } from 'sequelize'

class BtcTransaction extends Model {
  static init(sequelize) {
    super.init(
      {
        userId: Sequelize.BIGINT,
        address: Sequelize.STRING,
        txid: Sequelize.STRING,
        category: Sequelize.STRING,
        status: Sequelize.STRING,
        value: Sequelize.DECIMAL,
        amount: Sequelize.DECIMAL,
        confirmations: Sequelize.INTEGER,
        blockHeight: Sequelize.INTEGER,
        createdAt: Sequelize.DATE,
      },
      { sequelize, tableName: 'btc_transactions' }
    )

    return this
  }
}
export default BtcTransaction
