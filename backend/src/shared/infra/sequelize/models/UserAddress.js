import Sequelize, { Model } from 'sequelize'

class UserAddress extends Model {
  static init(sequelize) {
    super.init(
      {
        userId: {
          primaryKey: true,
          type: Sequelize.BIGINT,
        },
        countryId: {
          type: Sequelize.BIGINT,
        },
        state: {
          type: Sequelize.STRING,
          validate: {
            len: {
              args: [0, 300],
              msg: 'The code must contain less than 300 characters',
            },
          },
        },
        city: {
          type: Sequelize.STRING,
          validate: {
            len: {
              args: [0, 300],
              msg: 'The code must contain less than 300 characters',
            },
          },
        },
        neighborhood: {
          type: Sequelize.STRING,
          validate: {
            len: {
              args: [0, 300],
              msg: 'The code must contain less than 300 characters',
            },
          },
        },
        street: {
          type: Sequelize.STRING,
          validate: {
            len: {
              args: [0, 300],
              msg: 'The code must contain less than 300 characters',
            },
          },
        },
        number: {
          type: Sequelize.STRING,
          validate: {
            len: {
              args: [0, 300],
              msg: 'The code must contain less than 300 characters',
            },
          },
        },
        zipCode: {
          type: Sequelize.STRING,
          validate: {
            len: {
              args: [0, 300],
              msg: 'The code must contain less than 300 characters',
            },
          },
        },
        active: Sequelize.BOOLEAN,
        addressProofLink: Sequelize.STRING,
      },
      { sequelize, tableName: 'users_addresses' }
    )
    return this
  }

  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'userId',
      as: 'user',
    })
  }
}
export default UserAddress
