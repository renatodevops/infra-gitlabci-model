import Sequelize, { Model } from 'sequelize'

class Voucher extends Model {
  static init(sequelize) {
    super.init(
      {
        leaderId: {
          type: Sequelize.BIGINT,
        },
        userId: {
          type: Sequelize.BIGINT,
        },
        status: {
          type: Sequelize.STRING,
        },
        value: {
          type: Sequelize.DECIMAL,
        },
        hash: {
          type: Sequelize.STRING,
        },
        activatedAt: {
          type: Sequelize.DATE,
        },
        createdAt: {
          type: Sequelize.DATE,
        },
        updatedAt: {
          type: Sequelize.DATE,
        },
      },
      { sequelize, tableName: 'vouchers' }
    )

    return this
  }
}
export default Voucher
