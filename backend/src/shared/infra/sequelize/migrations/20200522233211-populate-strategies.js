module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'strategies',
      [
        {
          // 1
          name: 'FOREX',
          description: 'Forex trader',
          active: true,
          type_income_transaction_id: 14,
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('strategies', null, {})
  },
}
