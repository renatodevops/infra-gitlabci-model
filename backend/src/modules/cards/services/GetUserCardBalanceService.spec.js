import '../../../shared/infra/sequelize/index'
import crypto from 'crypto'

import User from '../../../shared/infra/sequelize/models/User'
import GetUserCardBalanceService from './GetUserCardBalanceService'

import {
  CREDIT,
  APPLIED,
  AVAILABLE,
  INCOME,
  VOUCHER,
} from '../../../util/cardsIdentifiers'

let user

describe('GetUserCardBalanceService tests', () => {
  beforeAll(async () => {
    user = await User.create({
      name: 'parsedName',
      phone: 'parsedPhone',
      document: 'parsedDocument',
      email: `${crypto.randomBytes(6).toString('hex')}@${crypto
        .randomBytes(6)
        .toString('hex')}.com`,
      born: new Date(),
      terms: true,
      indicatorId: 1,
      passwordHash: crypto.randomBytes(6).toString('hex'),
      selfieLink: 'selfieLink',
      documentFrontLink: 'selfieLink',
      documentBackLink: 'selfieLink',
    })
  })
  it('should return 0.0 to every card for 2 consecutive calls', async () => {
    let result = await Promise.all([
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: CREDIT,
      }),
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: APPLIED,
      }),
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: INCOME,
      }),
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: AVAILABLE,
      }),
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: VOUCHER,
      }),
    ])

    expect(result[0]).toBe(0)
    expect(result[1]).toBe(0)
    expect(result[2]).toBe(0)
    expect(result[3]).toBe(0)
    expect(result[4]).toBe(0)

    result = await Promise.all([
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: CREDIT,
      }),
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: APPLIED,
      }),
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: INCOME,
      }),
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: AVAILABLE,
      }),
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: VOUCHER,
      }),
    ])

    expect(result[0]).toBe(0)
    expect(result[1]).toBe(0)
    expect(result[2]).toBe(0)
    expect(result[3]).toBe(0)
    expect(result[4]).toBe(0)
  })
})
