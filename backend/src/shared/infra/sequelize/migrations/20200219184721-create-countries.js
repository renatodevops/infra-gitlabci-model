module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('countries', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      ibge_code: {
        allowNull: false,
        type: DataTypes.STRING(3),
      },
      phone_code: {
        allowNull: false,
        type: DataTypes.STRING(4),
      },
      iso: {
        allowNull: false,
        type: DataTypes.STRING(2),
        unique: true,
      },
      iso3: {
        allowNull: false,
        type: DataTypes.STRING(3),
        unique: true,
      },
      lang: {
        allowNull: true,
        type: DataTypes.STRING(2),
      },
      name: {
        allowNull: false,
        type: DataTypes.STRING(45),
        unique: true,
      },
      name_formal: {
        allowNull: false,
        type: DataTypes.STRING(80),
        unique: true,
      },
      nationality: {
        allowNull: true,
        type: DataTypes.STRING(40),
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('countries')
  },
}
