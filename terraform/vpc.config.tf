resource "digitalocean_vpc" "vpcgitlabci" {
  name = "vpcgitlabci"
  region = var.region
  ip_range = "10.10.0.0/24"
}
