import User from '../../../shared/infra/sequelize/models/User'
import UserAnnualPlan from '../../../shared/infra/sequelize/models/UserAnnualPlan'

class CreateUserAnnualPlanService {
  async execute({ userId, value }) {
    const user = await User.findByPk(userId)
    if (!user) throw new Error('Usuário não encontrado')

    const hasPending = await UserAnnualPlan.findOne({
      where: {
        userId,
        status: 'AGUARDANDO ANÁLISE',
      },
    })
    if (hasPending)
      throw new Error(
        'Vocẽ ja possui uma solicitação criada, aguarde a análise do seu indicador para criar outra'
      )

    return UserAnnualPlan.create({
      userId,
      indicatorId: user.indicatorId,
      userRate: 0.055,
      indicatorRate: 0.0,
      value,
      status: 'AGUARDANDO ANÁLISE',
    })
  }
}
export default new CreateUserAnnualPlanService()
