module.exports = {
  up: async (queryInterface, DataTypes) => {
    return queryInterface.createTable('users_annual_plans', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      user_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      indicator_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      card_transaction_id: {
        type: DataTypes.BIGINT,
        allowNull: true,
        references: {
          model: 'cards_transactions',
          key: 'id',
        },
      },
      user_rate: {
        allowNull: false,
        type: DataTypes.DECIMAL(5, 4),
        defaultValue: 0.0,
      },
      indicator_rate: {
        allowNull: false,
        type: DataTypes.DECIMAL(5, 4),
        defaultValue: 0.0,
      },
      value: {
        allowNull: false,
        type: DataTypes.DECIMAL(10, 2),
      },
      status: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('users_annual_plans')
  },
}
