import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'
import { subMonths } from 'date-fns'

import CreateUserService from '../../signUp/services/CreateUserService'
import truncate from '../../../../__tests__/utils/truncate'
import CardTransaction from '../../../shared/infra/sequelize/models/CardTransaction'
import { CONFIRMED } from '../../../util/btcTransactionsStatus'
import { ANNUAL_PLAN, APPLIED, AVAILABLE } from '../../../util/cardsIdentifiers'
import CreateMonthlyCareearPathPaymentsService from './CreateMonthlyCareearPathPaymentsService'
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'

let user

const createUser = async (indicatorId = 1) =>
  CreateUserService.execute({
    name: 'parsedName',
    phone: '(84) 99999-9999',
    document: 'parsedDocument',
    email: `${crypto.randomBytes(6).toString('hex')}@${crypto
      .randomBytes(6)
      .toString('hex')}.com`,
    born: new Date(),
    terms: true,
    indicatorId,
    password: '12345678',
    selfieLink: 'selfieLink',
    documentFrontLink: 'selfieLink',
    documentBackLink: 'selfieLink',
  })

describe('CreateMonthlyCareearPathPaymentsService tests', () => {
  beforeEach(async () => {
    user = await createUser()
    await truncate([CardTransaction])
  })
  it('should return 0 for all values', async () => {
    const {
      supervisorsBonus,
      managersBonus,
      // supervisors1,
      // supervisors2,
      // supervisors3,
      // supervisors4,
      // managers1,
      // managers2,
      // managers3,
      // managers4,
      supervisors1BonusValue,
      supervisors2BonusValue,
      supervisors3BonusValue,
      supervisors4BonusValue,
      managers1BonusValue,
      managers2BonusValue,
      managers3BonusValue,
      managers4BonusValue,
      supervisors1Length,
      supervisors2Length,
      supervisors3Length,
      supervisors4Length,
      managers1Length,
      managers2Length,
      managers3Length,
      managers4Length,
    } = await CreateMonthlyCareearPathPaymentsService.handle()

    expect(supervisorsBonus).toBe(0)
    expect(managersBonus).toBe(0)
    expect(supervisors1BonusValue).toBe(0)
    expect(supervisors2BonusValue).toBe(0)
    expect(supervisors3BonusValue).toBe(0)
    expect(supervisors4BonusValue).toBe(0)
    expect(managers1BonusValue).toBe(0)
    expect(managers2BonusValue).toBe(0)
    expect(managers3BonusValue).toBe(0)
    expect(managers4BonusValue).toBe(0)
    expect(supervisors1Length).toBe(0)
    expect(supervisors2Length).toBe(0)
    expect(supervisors3Length).toBe(0)
    expect(supervisors4Length).toBe(0)
    expect(managers1Length).toBe(0)
    expect(managers2Length).toBe(0)
    expect(managers3Length).toBe(0)
    expect(managers4Length).toBe(0)
  })
  it('should pay a supervisor 1 bonus', async () => {
    const indicated1 = await createUser(user.id)
    await Promise.all([
      CardTransaction.create({
        value: 1000,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated1.id,
        createdAt: subMonths(new Date(), 1),
      }),
    ])

    const {
      supervisorsBonus,
      managersBonus,
      // supervisors1,
      // supervisors2,
      // supervisors3,
      // supervisors4,
      // managers1,
      // managers2,
      // managers3,
      // managers4,
      supervisors1BonusValue,
      supervisors2BonusValue,
      supervisors3BonusValue,
      supervisors4BonusValue,
      managers1BonusValue,
      managers2BonusValue,
      managers3BonusValue,
      managers4BonusValue,
      supervisors1Length,
      supervisors2Length,
      supervisors3Length,
      supervisors4Length,
      managers1Length,
      managers2Length,
      managers3Length,
      managers4Length,
    } = await CreateMonthlyCareearPathPaymentsService.handle()

    expect(supervisorsBonus).toBe(5.0)
    expect(managersBonus).toBe(15.0)
    expect(supervisors1BonusValue).toBe(5 * 0.1)
    expect(supervisors2BonusValue).toBe(5 * 0.2)
    expect(supervisors3BonusValue).toBe(5 * 0.3)
    expect(supervisors4BonusValue).toBe(5 * 0.4)
    expect(managers1BonusValue).toBe(15 * 0.1)
    expect(managers2BonusValue).toBe(15 * 0.2)
    expect(managers3BonusValue).toBe(15 * 0.3)
    expect(managers4BonusValue).toBe(15 * 0.4)
    expect(supervisors1Length).toBe(1)
    expect(supervisors2Length).toBe(0)
    expect(supervisors3Length).toBe(0)
    expect(supervisors4Length).toBe(0)
    expect(managers1Length).toBe(0)
    expect(managers2Length).toBe(0)
    expect(managers3Length).toBe(0)
    expect(managers4Length).toBe(0)

    const balance = await GetUserCardBalanceService.execute({
      userId: user.id,
      cardIdentifier: AVAILABLE,
    })
    expect(balance).toBe(0.5)
  })
  it('should pay a supervisor 2 bonus', async () => {
    const indicated1 = await createUser(user.id)
    await Promise.all([
      CardTransaction.create({
        value: 2000,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated1.id,
        createdAt: subMonths(new Date(), 1),
      }),
    ])

    const {
      supervisorsBonus,
      managersBonus,
      // supervisors1,
      // supervisors2,
      // supervisors3,
      // supervisors4,
      // managers1,
      // managers2,
      // managers3,
      // managers4,
      supervisors1BonusValue,
      supervisors2BonusValue,
      supervisors3BonusValue,
      supervisors4BonusValue,
      managers1BonusValue,
      managers2BonusValue,
      managers3BonusValue,
      managers4BonusValue,
      supervisors1Length,
      supervisors2Length,
      supervisors3Length,
      supervisors4Length,
      managers1Length,
      managers2Length,
      managers3Length,
      managers4Length,
    } = await CreateMonthlyCareearPathPaymentsService.handle()

    expect(supervisorsBonus).toBe(10.0)
    expect(managersBonus).toBe(30.0)
    expect(supervisors1BonusValue).toBe(10 * 0.1)
    expect(supervisors2BonusValue).toBe(10 * 0.2)
    expect(supervisors3BonusValue).toBe(10 * 0.3)
    expect(supervisors4BonusValue).toBe(10 * 0.4)
    expect(managers1BonusValue).toBe(30 * 0.1)
    expect(managers2BonusValue).toBe(30 * 0.2)
    expect(managers3BonusValue).toBe(30 * 0.3)
    expect(managers4BonusValue).toBe(30 * 0.4)
    expect(supervisors1Length).toBe(0)
    expect(supervisors2Length).toBe(1)
    expect(supervisors3Length).toBe(0)
    expect(supervisors4Length).toBe(0)
    expect(managers1Length).toBe(0)
    expect(managers2Length).toBe(0)
    expect(managers3Length).toBe(0)
    expect(managers4Length).toBe(0)

    const balance = await GetUserCardBalanceService.execute({
      userId: user.id,
      cardIdentifier: AVAILABLE,
    })
    expect(balance).toBe(2.0)
  })
  it('should pay a supervisor 3 bonus', async () => {
    const indicated1 = await createUser(user.id)
    await Promise.all([
      CardTransaction.create({
        value: 3000,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated1.id,
        createdAt: subMonths(new Date(), 1),
      }),
    ])

    const {
      supervisorsBonus,
      managersBonus,
      // supervisors1,
      // supervisors2,
      // supervisors3,
      // supervisors4,
      // managers1,
      // managers2,
      // managers3,
      // managers4,
      supervisors1BonusValue,
      supervisors2BonusValue,
      supervisors3BonusValue,
      supervisors4BonusValue,
      managers1BonusValue,
      managers2BonusValue,
      managers3BonusValue,
      managers4BonusValue,
      supervisors1Length,
      supervisors2Length,
      supervisors3Length,
      supervisors4Length,
      managers1Length,
      managers2Length,
      managers3Length,
      managers4Length,
    } = await CreateMonthlyCareearPathPaymentsService.handle()

    expect(supervisorsBonus).toBe(15.0)
    expect(managersBonus).toBe(45.0)
    expect(supervisors1BonusValue).toBe(15 * 0.1)
    expect(supervisors2BonusValue).toBe(15 * 0.2)
    expect(supervisors3BonusValue).toBe(15 * 0.3)
    expect(supervisors4BonusValue).toBe(15 * 0.4)
    expect(managers1BonusValue).toBe(45 * 0.1)
    expect(managers2BonusValue).toBe(45 * 0.2)
    expect(managers3BonusValue).toBe(45 * 0.3)
    expect(managers4BonusValue).toBe(45 * 0.4)
    expect(supervisors1Length).toBe(0)
    expect(supervisors2Length).toBe(0)
    expect(supervisors3Length).toBe(1)
    expect(supervisors4Length).toBe(0)
    expect(managers1Length).toBe(0)
    expect(managers2Length).toBe(0)
    expect(managers3Length).toBe(0)
    expect(managers4Length).toBe(0)

    const balance = await GetUserCardBalanceService.execute({
      userId: user.id,
      cardIdentifier: AVAILABLE,
    })
    expect(balance).toBe(4.5)
  })
  it('should pay a supervisor 3 bonus', async () => {
    const indicated1 = await createUser(user.id)
    await Promise.all([
      CardTransaction.create({
        value: 4000,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated1.id,
        createdAt: subMonths(new Date(), 1),
      }),
    ])

    const {
      supervisorsBonus,
      managersBonus,
      // supervisors1,
      // supervisors2,
      // supervisors3,
      // supervisors4,
      // managers1,
      // managers2,
      // managers3,
      // managers4,
      supervisors1BonusValue,
      supervisors2BonusValue,
      supervisors3BonusValue,
      supervisors4BonusValue,
      managers1BonusValue,
      managers2BonusValue,
      managers3BonusValue,
      managers4BonusValue,
      supervisors1Length,
      supervisors2Length,
      supervisors3Length,
      supervisors4Length,
      managers1Length,
      managers2Length,
      managers3Length,
      managers4Length,
    } = await CreateMonthlyCareearPathPaymentsService.handle()

    expect(supervisorsBonus).toBe(20.0)
    expect(managersBonus).toBe(60.0)
    expect(supervisors1BonusValue).toBe(20 * 0.1)
    expect(supervisors2BonusValue).toBe(20 * 0.2)
    expect(supervisors3BonusValue).toBe(20 * 0.3)
    expect(supervisors4BonusValue).toBe(20 * 0.4)
    expect(managers1BonusValue).toBe(60 * 0.1)
    expect(managers2BonusValue).toBe(60 * 0.2)
    expect(managers3BonusValue).toBe(60 * 0.3)
    expect(managers4BonusValue).toBe(60 * 0.4)
    expect(supervisors1Length).toBe(0)
    expect(supervisors2Length).toBe(0)
    expect(supervisors3Length).toBe(1)
    expect(supervisors4Length).toBe(0)
    expect(managers1Length).toBe(0)
    expect(managers2Length).toBe(0)
    expect(managers3Length).toBe(0)
    expect(managers4Length).toBe(0)

    const balance = await GetUserCardBalanceService.execute({
      userId: user.id,
      cardIdentifier: AVAILABLE,
    })
    expect(balance).toBe(6.0)
  })
  it('should pay a supervisor 4 bonus', async () => {
    const indicated1 = await createUser(user.id)
    await Promise.all([
      CardTransaction.create({
        value: 5000,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated1.id,
        createdAt: subMonths(new Date(), 1),
      }),
    ])

    const {
      supervisorsBonus,
      managersBonus,
      // supervisors1,
      // supervisors2,
      // supervisors3,
      // supervisors4,
      // managers1,
      // managers2,
      // managers3,
      // managers4,
      supervisors1BonusValue,
      supervisors2BonusValue,
      supervisors3BonusValue,
      supervisors4BonusValue,
      managers1BonusValue,
      managers2BonusValue,
      managers3BonusValue,
      managers4BonusValue,
      supervisors1Length,
      supervisors2Length,
      supervisors3Length,
      supervisors4Length,
      managers1Length,
      managers2Length,
      managers3Length,
      managers4Length,
    } = await CreateMonthlyCareearPathPaymentsService.handle()

    expect(supervisorsBonus).toBe(25.0)
    expect(managersBonus).toBe(75.0)
    expect(supervisors1BonusValue).toBe(25 * 0.1)
    expect(supervisors2BonusValue).toBe(25 * 0.2)
    expect(supervisors3BonusValue).toBe(25 * 0.3)
    expect(supervisors4BonusValue).toBe(25 * 0.4)
    expect(managers1BonusValue).toBe(75 * 0.1)
    expect(managers2BonusValue).toBe(75 * 0.2)
    expect(managers3BonusValue).toBe(75 * 0.3)
    expect(managers4BonusValue).toBe(75 * 0.4)
    expect(supervisors1Length).toBe(0)
    expect(supervisors2Length).toBe(0)
    expect(supervisors3Length).toBe(0)
    expect(supervisors4Length).toBe(1)
    expect(managers1Length).toBe(0)
    expect(managers2Length).toBe(0)
    expect(managers3Length).toBe(0)
    expect(managers4Length).toBe(0)

    const balance = await GetUserCardBalanceService.execute({
      userId: user.id,
      cardIdentifier: AVAILABLE,
    })
    expect(balance).toBe(10.0)
  })
  it('should pay a manager 1 bonus', async () => {
    const indicated1 = await createUser(user.id)
    await Promise.all([
      CardTransaction.create({
        value: 6000,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated1.id,
        createdAt: subMonths(new Date(), 1),
      }),
    ])

    const {
      supervisorsBonus,
      managersBonus,
      // supervisors1,
      // supervisors2,
      // supervisors3,
      // supervisors4,
      // managers1,
      // managers2,
      // managers3,
      // managers4,
      supervisors1BonusValue,
      supervisors2BonusValue,
      supervisors3BonusValue,
      supervisors4BonusValue,
      managers1BonusValue,
      managers2BonusValue,
      managers3BonusValue,
      managers4BonusValue,
      supervisors1Length,
      supervisors2Length,
      supervisors3Length,
      supervisors4Length,
      managers1Length,
      managers2Length,
      managers3Length,
      managers4Length,
    } = await CreateMonthlyCareearPathPaymentsService.handle()

    expect(supervisorsBonus).toBe(30.0)
    expect(managersBonus).toBe(90.0)
    expect(supervisors1BonusValue).toBe(30 * 0.1)
    expect(supervisors2BonusValue).toBe(30 * 0.2)
    expect(supervisors3BonusValue).toBe(30 * 0.3)
    expect(supervisors4BonusValue).toBe(30 * 0.4)
    expect(managers1BonusValue).toBe(90 * 0.1)
    expect(managers2BonusValue).toBe(90 * 0.2)
    expect(managers3BonusValue).toBe(90 * 0.3)
    expect(managers4BonusValue).toBe(90 * 0.4)
    expect(supervisors1Length).toBe(0)
    expect(supervisors2Length).toBe(0)
    expect(supervisors3Length).toBe(0)
    expect(supervisors4Length).toBe(0)
    expect(managers1Length).toBe(1)
    expect(managers2Length).toBe(0)
    expect(managers3Length).toBe(0)
    expect(managers4Length).toBe(0)

    const balance = await GetUserCardBalanceService.execute({
      userId: user.id,
      cardIdentifier: AVAILABLE,
    })
    expect(balance).toBe(9.0)
  })
  it('should pay a manager 2 bonus', async () => {
    const indicated1 = await createUser(user.id)
    await Promise.all([
      CardTransaction.create({
        value: 7000,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated1.id,
        createdAt: subMonths(new Date(), 1),
      }),
    ])

    const {
      supervisorsBonus,
      managersBonus,
      // supervisors1,
      // supervisors2,
      // supervisors3,
      // supervisors4,
      // managers1,
      // managers2,
      // managers3,
      // managers4,
      supervisors1BonusValue,
      supervisors2BonusValue,
      supervisors3BonusValue,
      supervisors4BonusValue,
      managers1BonusValue,
      managers2BonusValue,
      managers3BonusValue,
      managers4BonusValue,
      supervisors1Length,
      supervisors2Length,
      supervisors3Length,
      supervisors4Length,
      managers1Length,
      managers2Length,
      managers3Length,
      managers4Length,
    } = await CreateMonthlyCareearPathPaymentsService.handle()

    expect(supervisorsBonus).toBe(35.0)
    expect(managersBonus).toBe(105.0)
    expect(supervisors1BonusValue).toBe(35 * 0.1)
    expect(supervisors2BonusValue).toBe(35 * 0.2)
    expect(supervisors3BonusValue).toBe(35 * 0.3)
    expect(supervisors4BonusValue).toBe(35 * 0.4)
    expect(managers1BonusValue).toBe(105 * 0.1)
    expect(managers2BonusValue).toBe(105 * 0.2)
    expect(managers3BonusValue).toBe(105 * 0.3)
    expect(managers4BonusValue).toBe(105 * 0.4)
    expect(supervisors1Length).toBe(0)
    expect(supervisors2Length).toBe(0)
    expect(supervisors3Length).toBe(0)
    expect(supervisors4Length).toBe(0)
    expect(managers1Length).toBe(0)
    expect(managers2Length).toBe(1)
    expect(managers3Length).toBe(0)
    expect(managers4Length).toBe(0)

    const balance = await GetUserCardBalanceService.execute({
      userId: user.id,
      cardIdentifier: AVAILABLE,
    })
    expect(balance).toBe(21.0)
  })
  it('should pay a manager 3 bonus', async () => {
    const indicated1 = await createUser(user.id)
    await Promise.all([
      CardTransaction.create({
        value: 8000,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated1.id,
        createdAt: subMonths(new Date(), 1),
      }),
    ])

    const {
      supervisorsBonus,
      managersBonus,
      // supervisors1,
      // supervisors2,
      // supervisors3,
      // supervisors4,
      // managers1,
      // managers2,
      // managers3,
      // managers4,
      supervisors1BonusValue,
      supervisors2BonusValue,
      supervisors3BonusValue,
      supervisors4BonusValue,
      managers1BonusValue,
      managers2BonusValue,
      managers3BonusValue,
      managers4BonusValue,
      supervisors1Length,
      supervisors2Length,
      supervisors3Length,
      supervisors4Length,
      managers1Length,
      managers2Length,
      managers3Length,
      managers4Length,
    } = await CreateMonthlyCareearPathPaymentsService.handle()

    expect(supervisorsBonus).toBe(40.0)
    expect(managersBonus).toBe(120.0)
    expect(supervisors1BonusValue).toBe(40 * 0.1)
    expect(supervisors2BonusValue).toBe(40 * 0.2)
    expect(supervisors3BonusValue).toBe(40 * 0.3)
    expect(supervisors4BonusValue).toBe(40 * 0.4)
    expect(managers1BonusValue).toBe(120 * 0.1)
    expect(managers2BonusValue).toBe(120 * 0.2)
    expect(managers3BonusValue).toBe(120 * 0.3)
    expect(managers4BonusValue).toBe(120 * 0.4)
    expect(supervisors1Length).toBe(0)
    expect(supervisors2Length).toBe(0)
    expect(supervisors3Length).toBe(0)
    expect(supervisors4Length).toBe(0)
    expect(managers1Length).toBe(0)
    expect(managers2Length).toBe(0)
    expect(managers3Length).toBe(1)
    expect(managers4Length).toBe(0)

    const balance = await GetUserCardBalanceService.execute({
      userId: user.id,
      cardIdentifier: AVAILABLE,
    })
    expect(balance).toBe(36.0)
  })
  it('should pay a manager 3 bonus', async () => {
    const indicated1 = await createUser(user.id)
    await Promise.all([
      CardTransaction.create({
        value: 9000,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated1.id,
        createdAt: subMonths(new Date(), 1),
      }),
    ])

    const {
      supervisorsBonus,
      managersBonus,
      // supervisors1,
      // supervisors2,
      // supervisors3,
      // supervisors4,
      // managers1,
      // managers2,
      // managers3,
      // managers4,
      supervisors1BonusValue,
      supervisors2BonusValue,
      supervisors3BonusValue,
      supervisors4BonusValue,
      managers1BonusValue,
      managers2BonusValue,
      managers3BonusValue,
      managers4BonusValue,
      supervisors1Length,
      supervisors2Length,
      supervisors3Length,
      supervisors4Length,
      managers1Length,
      managers2Length,
      managers3Length,
      managers4Length,
    } = await CreateMonthlyCareearPathPaymentsService.handle()

    expect(supervisorsBonus).toBe(45.0)
    expect(managersBonus).toBe(135.0)
    expect(supervisors1BonusValue).toBe(45 * 0.1)
    expect(supervisors2BonusValue).toBe(45 * 0.2)
    expect(supervisors3BonusValue).toBe(45 * 0.3)
    expect(supervisors4BonusValue).toBe(45 * 0.4)
    expect(managers1BonusValue).toBe(135 * 0.1)
    expect(managers2BonusValue).toBe(135 * 0.2)
    expect(managers3BonusValue).toBe(135 * 0.3)
    expect(managers4BonusValue).toBe(135 * 0.4)
    expect(supervisors1Length).toBe(0)
    expect(supervisors2Length).toBe(0)
    expect(supervisors3Length).toBe(0)
    expect(supervisors4Length).toBe(0)
    expect(managers1Length).toBe(0)
    expect(managers2Length).toBe(0)
    expect(managers3Length).toBe(1)
    expect(managers4Length).toBe(0)

    const balance = await GetUserCardBalanceService.execute({
      userId: user.id,
      cardIdentifier: AVAILABLE,
    })
    expect(balance).toBe(40.5)
  })
  it('should pay a manager 4 bonus', async () => {
    const indicated1 = await createUser(user.id)
    await Promise.all([
      CardTransaction.create({
        value: 10000,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated1.id,
        createdAt: subMonths(new Date(), 1),
      }),
    ])

    const {
      supervisorsBonus,
      managersBonus,
      // supervisors1,
      // supervisors2,
      // supervisors3,
      // supervisors4,
      // managers1,
      // managers2,
      // managers3,
      // managers4,
      supervisors1BonusValue,
      supervisors2BonusValue,
      supervisors3BonusValue,
      supervisors4BonusValue,
      managers1BonusValue,
      managers2BonusValue,
      managers3BonusValue,
      managers4BonusValue,
      supervisors1Length,
      supervisors2Length,
      supervisors3Length,
      supervisors4Length,
      managers1Length,
      managers2Length,
      managers3Length,
      managers4Length,
    } = await CreateMonthlyCareearPathPaymentsService.handle()

    expect(supervisorsBonus).toBe(50.0)
    expect(managersBonus).toBe(150.0)
    expect(supervisors1BonusValue).toBe(50 * 0.1)
    expect(supervisors2BonusValue).toBe(50 * 0.2)
    expect(supervisors3BonusValue).toBe(50 * 0.3)
    expect(supervisors4BonusValue).toBe(50 * 0.4)
    expect(managers1BonusValue).toBe(150 * 0.1)
    expect(managers2BonusValue).toBe(150 * 0.2)
    expect(managers3BonusValue).toBe(150 * 0.3)
    expect(managers4BonusValue).toBe(150 * 0.4)
    expect(supervisors1Length).toBe(0)
    expect(supervisors2Length).toBe(0)
    expect(supervisors3Length).toBe(0)
    expect(supervisors4Length).toBe(0)
    expect(managers1Length).toBe(0)
    expect(managers2Length).toBe(0)
    expect(managers3Length).toBe(0)
    expect(managers4Length).toBe(1)

    const balance = await GetUserCardBalanceService.execute({
      userId: user.id,
      cardIdentifier: AVAILABLE,
    })
    expect(balance).toBe(60.0)
  })
  it('should pay a manager 4 bonus and a supervisor 1 bonus', async () => {
    const indicated1 = await createUser(user.id)
    const user2 = await createUser()
    const indicated2 = await createUser(user2.id)
    await Promise.all([
      CardTransaction.create({
        value: 10000,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated1.id,
        createdAt: subMonths(new Date(), 1),
      }),
      CardTransaction.create({
        value: 1000,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: ANNUAL_PLAN,
        userId: indicated2.id,
        createdAt: subMonths(new Date(), 1),
      }),
    ])

    const {
      supervisorsBonus,
      managersBonus,
      supervisors1BonusValue,
      supervisors2BonusValue,
      supervisors3BonusValue,
      supervisors4BonusValue,
      managers1BonusValue,
      managers2BonusValue,
      managers3BonusValue,
      managers4BonusValue,
      supervisors1Length,
      supervisors2Length,
      supervisors3Length,
      supervisors4Length,
      managers1Length,
      managers2Length,
      managers3Length,
      managers4Length,
    } = await CreateMonthlyCareearPathPaymentsService.handle()

    expect(supervisorsBonus).toBe(11000 * 0.005)
    expect(managersBonus).toBe(11000 * 0.015)
    expect(supervisors1BonusValue).toBe(11000 * 0.005 * 0.1)
    expect(supervisors2BonusValue).toBe(11000 * 0.005 * 0.2)
    expect(supervisors3BonusValue).toBe(11000 * 0.005 * 0.3)
    expect(supervisors4BonusValue).toBe(11000 * 0.005 * 0.4)
    expect(managers1BonusValue).toBe(11000 * 0.015 * 0.1)
    expect(managers2BonusValue).toBe(11000 * 0.015 * 0.2)
    expect(managers3BonusValue).toBe(11000 * 0.015 * 0.3)
    expect(managers4BonusValue).toBe(11000 * 0.015 * 0.4)
    expect(supervisors1Length).toBe(1)
    expect(supervisors2Length).toBe(0)
    expect(supervisors3Length).toBe(0)
    expect(supervisors4Length).toBe(0)
    expect(managers1Length).toBe(0)
    expect(managers2Length).toBe(0)
    expect(managers3Length).toBe(0)
    expect(managers4Length).toBe(1)

    let balance = await GetUserCardBalanceService.execute({
      userId: user.id,
      cardIdentifier: AVAILABLE,
    })
    expect(balance).toBe(11000 * 0.015 * 0.4)

    balance = await GetUserCardBalanceService.execute({
      userId: user2.id,
      cardIdentifier: AVAILABLE,
    })
    expect(balance).toBe(11000 * 0.005 * 0.1)
  })
  it('should pay a manager 4 bonus and a supervisor 1 bonus', async () => {
    const indicated1 = await createUser(user.id)
    const user2 = await createUser()
    const indicated2 = await createUser(user2.id)

    const user3 = await createUser()
    const indicated3 = await createUser(user3.id)

    const user4 = await createUser()
    const indicated4 = await createUser(user4.id)

    await Promise.all([
      CardTransaction.create({
        value: 10000,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated1.id,
        createdAt: subMonths(new Date(), 1),
      }),
      CardTransaction.create({
        value: 1000,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: ANNUAL_PLAN,
        userId: indicated2.id,
        createdAt: subMonths(new Date(), 1),
      }),
      CardTransaction.create({
        value: 10000,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: ANNUAL_PLAN,
        userId: indicated3.id,
        createdAt: subMonths(new Date(), 1),
      }),
      CardTransaction.create({
        value: 1000,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: ANNUAL_PLAN,
        userId: indicated4.id,
        createdAt: subMonths(new Date(), 1),
      }),
    ])

    const {
      supervisorsBonus,
      managersBonus,
      supervisors1BonusValue,
      supervisors2BonusValue,
      supervisors3BonusValue,
      supervisors4BonusValue,
      managers1BonusValue,
      managers2BonusValue,
      managers3BonusValue,
      managers4BonusValue,
      supervisors1Length,
      supervisors2Length,
      supervisors3Length,
      supervisors4Length,
      managers1Length,
      managers2Length,
      managers3Length,
      managers4Length,
    } = await CreateMonthlyCareearPathPaymentsService.handle()

    expect(supervisorsBonus).toBe(22000 * 0.005)
    expect(managersBonus).toBe(22000 * 0.015)
    expect(supervisors1BonusValue).toBe(22000 * 0.005 * 0.1)
    expect(supervisors2BonusValue).toBe(22000 * 0.005 * 0.2)
    expect(supervisors3BonusValue).toBe(22000 * 0.005 * 0.3)
    expect(supervisors4BonusValue).toBe(22000 * 0.005 * 0.4)
    expect(managers1BonusValue).toBe(22000 * 0.015 * 0.1)
    expect(managers2BonusValue).toBe(22000 * 0.015 * 0.2)
    expect(managers3BonusValue).toBe(22000 * 0.015 * 0.3)
    expect(managers4BonusValue).toBe(22000 * 0.015 * 0.4)
    expect(supervisors1Length).toBe(2)
    expect(supervisors2Length).toBe(0)
    expect(supervisors3Length).toBe(0)
    expect(supervisors4Length).toBe(0)
    expect(managers1Length).toBe(0)
    expect(managers2Length).toBe(0)
    expect(managers3Length).toBe(0)
    expect(managers4Length).toBe(2)

    let balance = await GetUserCardBalanceService.execute({
      userId: user.id,
      cardIdentifier: AVAILABLE,
    })
    expect(balance).toBe(22000 * 0.015 * 0.4 * 0.5)

    balance = await GetUserCardBalanceService.execute({
      userId: user2.id,
      cardIdentifier: AVAILABLE,
    })
    expect(balance).toBe(22000 * 0.005 * 0.1 * 0.5)

    balance = await GetUserCardBalanceService.execute({
      userId: user3.id,
      cardIdentifier: AVAILABLE,
    })
    expect(balance).toBe(22000 * 0.015 * 0.4 * 0.5)

    balance = await GetUserCardBalanceService.execute({
      userId: user4.id,
      cardIdentifier: AVAILABLE,
    })
    expect(balance).toBe(22000 * 0.005 * 0.1 * 0.5)
  })
})
