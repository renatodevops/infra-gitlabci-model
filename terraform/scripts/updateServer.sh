#!/bin/bash

echo "+---------------------------+"
echo "| update and upgrade server |"
echo "+---------------------------+"
sudo killall apt apt-get
sudo dpkg --configure -a
sudo apt update
echo "+---------------------------------+"
echo "| install dependencies on server  |"
echo "+---------------------------------+"
sudo apt install -y vim curl wget unzip zip build-essential git net-tools nmap apt-transport-https ca-certificates software-properties-common rsync
echo "+---------------------------+"
echo "| install docker on server  |"
echo "+---------------------------+"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt update
sudo apt install -y docker-ce
sudo usermod -aG docker ${USER} \
  && sudo chown ${USER}:docker /var/run/docker.sock
docker -v
sleep 2
echo "+--------------------------+"
echo "|  install docker compose  |"
echo "+--------------------------+"
sudo curl -L https://github.com/docker/compose/releases/download/1.26.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose -v
sleep 2
echo "+-----------------------+"
echo "| install node on server |"
echo "+-----------------------+"
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt update
sudo apt install -y nodejs
echo "node --version"
node -v
sleep 2
echo "+-------------------------+"
echo "| install yarn on server  |"
echo "+-------------------------+"
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update
sudo apt install -y yarn
sudo apt install --no-install-recommends yarn
yarn -v
