module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.dropTable('vouchers')
    await queryInterface.dropTable('types_vouchers')
    await queryInterface.dropTable('status_voucher')
    return queryInterface.createTable('vouchers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      user_id: {
        type: DataTypes.BIGINT,
        allowNull: true,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      leader_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      hash: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      status: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      value: {
        allowNull: false,
        type: DataTypes.DECIMAL(10, 2),
      },
      activated_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('vouchers')
  },
}
