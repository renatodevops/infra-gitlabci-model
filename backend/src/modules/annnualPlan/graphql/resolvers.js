import { Op } from 'sequelize'
import { subMonths, format } from 'date-fns'
import {
  authenticated,
  compose,
  hasAllowed,
  verifyFinancialPassword,
} from '../../../shared/infra/http/composers/composables'
import BeeQueue from '../../../shared/providers/QueueProvider/Bee'
import CreateAnnualPlanService from '../services/CreateAnnualPlanService'
import CreateAnnualPlanWithdrawalService from '../services/CreateAnnualPlanWithdrawalService'
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'
import GetAnnualPlanBalance from '../services/GetAnnualPlanBalanceService'
import { ANNUAL_PLAN, CREDIT } from '../../../util/cardsIdentifiers'
import GetUserCardTransactions from '../../cards/services/GetUserCardTransactions'
import CardTransaction from '../../../shared/infra/sequelize/models/CardTransaction'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import GetNextMonthAnnualPlan from '../services/GetNextMonthAnnualPlan'
import GetCurrentMonthAnnualPlan from '../services/GetCurrentMonthAnnualPlan'
import AnnualPlanRate from '../../../shared/infra/sequelize/models/AnnualPlanRate'
import GetAnnualPlanGainsService from '../services/GetAnnualPlanGainsService'
import CreateUserAnnualPlanService from '../services/CreateUserAnnualPlanService'
import User from '../../../shared/infra/sequelize/models/User'
import UserAnnualPlan from '../../../shared/infra/sequelize/models/UserAnnualPlan'

export default {
  UserAnnualPlan: {
    indicator: obj => User.findByPk(obj.indicatorId),
    user: obj => User.findByPk(obj.userId),
    maxUserRate: async () => {
      const currentMonthPlan = await GetCurrentMonthAnnualPlan.execute()
      return currentMonthPlan.rate
    },
    minUserRate: () => 0.055,
  },
  Query: {
    GetUserAnnualPlans: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) => {
      return UserAnnualPlan.findAll({
        where: {
          userId: ctx.user.id,
          status: ['AGUARDANDO ANÁLISE', 'AGUARDANDO ATIVAÇÂO'],
        },
        order: [['id', 'desc']],
      })
    }),

    GetMyClientsAnnualPlans: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) => {
      const myClients = await User.findAll({
        where: {
          indicatorId: ctx.user.id,
        },
      })
      return UserAnnualPlan.findAll({
        where: {
          userId: myClients.map(u => u.id),
          status: 'AGUARDANDO ANÁLISE',
        },
        order: [['id', 'desc']],
      })
    }),
    GetAnnualPlanBalance: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) => {
      return GetAnnualPlanBalance.execute({ userId: ctx.user.id })
    }),

    GetAnnualPlanGains: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) => {
      return GetAnnualPlanGainsService.execute({ userId: ctx.user.id })
    }),

    GetMyAnnualPlanTransactions: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) => {
      const userId = ctx.user.id

      const userCardsTransactions = await GetUserCardTransactions.execute({
        userId,
      })

      const node = userCardsTransactions.filter(
        transaction =>
          transaction.cardIdentifier === ANNUAL_PLAN ||
          transaction.description === `Rendimento mensal de plano anual`
      )

      return {
        edge: {
          node,
        },
        pageInfo: {
          totalCount: userCardsTransactions.length,
          hasPrevPage: false,
          hasNextPage: true,
        },
      }
    }),

    GetMyAnnualPlanGainsChart: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) => {
      const userCardsTransactions = await GetUserCardTransactions.execute({
        userId: ctx.user.id,
      })

      const [lastClosed] = await CardTransaction.findAll({
        where: {
          cardIdentifier: ANNUAL_PLAN,
          status: CONFIRMED,
          userId: ctx.user.id,
          value: { [Op.lt]: 0.0 },
        },
        order: [['id', 'desc']],
      })

      const lastClosedDate = lastClosed
        ? lastClosed.createdAt
        : new Date('01-01-2019')

      const gains = userCardsTransactions
        .filter(
          transaction =>
            transaction.description === `Rendimento mensal de plano anual` &&
            transaction.createdAt > lastClosedDate
        )
        .reverse()

      const gainsSerialized =
        gains.length > 0
          ? gains.reduce(
              (acc, curr, index) => {
                const data = {
                  date: format(new Date(curr.createdAt), 'MM-yyyy'),
                  value: Number(curr.value),
                  accumulated: Number(curr.value) + acc[index].accumulated,
                }
                acc.push(data)
                return acc
              },
              [
                {
                  date: format(
                    subMonths(
                      gains[0] ? new Date(gains[0].createdAt) : new Date(),
                      1
                    ),
                    'MM-yyyy'
                  ),
                  value: 0,
                  accumulated: 0,
                },
              ]
            )
          : []

      return gainsSerialized
    }),

    GetAnnualPlanChart: compose(
      authenticated,
      hasAllowed([5])
    )(async () => {
      const appliedMonths = await AnnualPlanRate.findAll({
        where: { hasBeenApplied: true },
        order: [['id', 'asc']],
      })

      const appliedMonthsSerialized =
        appliedMonths.length > 0
          ? appliedMonths.reduce((acc, curr) => {
              const data = {
                date: format(new Date(curr.year, curr.month), 'MM-yyyy'),
                rate: Number(curr.rate) * 100,
              }
              acc.push(data)
              return acc
            }, [])
          : []

      return appliedMonthsSerialized
    }),

    GetNextMonthAnnualPlanRate: compose(
      authenticated,
      hasAllowed([5])
    )(async () => {
      const nextMontPlan = await GetNextMonthAnnualPlan.execute()
      return {
        ...nextMontPlan.dataValues,
        rate: Number(nextMontPlan.rate) * 100,
      }
    }),

    GetCurrentMonthAnnualPlanRate: compose(
      authenticated,
      hasAllowed([5])
    )(async () => {
      const nextMontPlan = await GetCurrentMonthAnnualPlan.execute()
      return {
        ...nextMontPlan.dataValues,
        rate: Number(nextMontPlan.rate) * 100,
      }
    }),
  },
  Mutation: {
    CreateAnnualPlanAport: compose(
      authenticated,
      verifyFinancialPassword,
      hasAllowed([1])
    )(async (_, { userAnnualPlanId }, ctx) => {
      const userPlan = await UserAnnualPlan.findByPk(userAnnualPlanId)

      if (!userPlan) throw new Error('Solicitação não encontrada')

      if (userPlan.status !== 'AGUARDANDO ATIVAÇÂO')
        throw new Error('Ativação não pode ser realizada')

      if (userPlan.userId !== ctx.user.id)
        throw new Error('Não é possível ativa plano de outro usuário')

      const creditBalance = await GetUserCardBalanceService.execute({
        userId: ctx.user.id,
        cardIdentifier: CREDIT,
      })
      if (creditBalance < Number(userPlan.value))
        throw new Error('Saldo insuficiente')

      await BeeQueue.add(CreateAnnualPlanService.key, {
        userAnnualPlanId,
      })
      return 'Verifique seu extrato em instantes para confirmar a operação'
    }),

    UpdateUserAnnualPlan: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, { userAnnualPlanId, userRate, indicatorRate }, ctx) => {
      const userPlan = await UserAnnualPlan.findByPk(userAnnualPlanId)

      if (!userPlan) throw new Error('Solicitação não encontrada')

      if (userPlan.status !== 'AGUARDANDO ANÁLISE')
        throw new Error('Ativação não pode ser analisada')

      if (userPlan.indicatorId !== ctx.user.id)
        throw new Error('Não é possível ativa plano de outro usuário')

      await userPlan.update({
        status: 'AGUARDANDO ATIVAÇÂO',
        userRate,
        indicatorRate,
      })

      return 'Solicitação atualizada com sucesso'
    }),

    CreateUserAnnualPlan: compose(
      authenticated,
      verifyFinancialPassword,
      hasAllowed([1])
    )(async (_, { value }, ctx) => {
      await CreateUserAnnualPlanService.execute({
        value,
        userId: ctx.user.id,
      })
      return 'Solicitação criada com sucesso, aguarde a análise de seu indicador'
    }),

    CreateAnnualPlanWithdrawal: compose(
      authenticated,
      verifyFinancialPassword,
      hasAllowed([1])
    )(async (_, args, ctx) => {
      const balance = await GetAnnualPlanBalance.execute({
        userId: ctx.user.id,
      })
      if (balance <= 0) throw new Error('Saldo Insuficiente')

      await BeeQueue.add(CreateAnnualPlanWithdrawalService.key, {
        userId: ctx.user.id,
      })
      return 'Verifique seu extrato em instantes para confirmar a operação'
    }),

    UpdateNextMonthAnnualPlanRate: compose(
      authenticated,
      hasAllowed([5])
    )(async (_, { rate }) => {
      const nextMonthPlan = await GetNextMonthAnnualPlan.execute()
      const parsedRate =
        Number(rate) >= 1.0 ? Number(rate) * 0.01 : Number(rate)

      if (nextMonthPlan.hasBeenApplied)
        throw new Error('Planejamento não pode ser alterado')
      await nextMonthPlan.update({ rate: parsedRate })

      return `Porcentagem alterada com sucesso para ${parsedRate * 100}%`
    }),

    DeleteUserAnnualPlan: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, { id }, ctx) => {
      const userPlan = await UserAnnualPlan.findByPk(id)

      if (!userPlan) throw new Error('Solicitação não encontrada')

      if (userPlan.status === 'CONFIRMED')
        throw new Error('Solicitação não pode ser cancelada')

      if (userPlan.userId !== ctx.user.id)
        throw new Error('Solicitação não pode ser cancelada')

      await userPlan.destroy()

      return 'Solicitação Cancelada com sucesso'
    }),
  },
}
