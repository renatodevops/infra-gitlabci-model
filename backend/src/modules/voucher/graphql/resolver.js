/* eslint-disable no-empty */
/* eslint-disable no-unused-vars */
import { Op } from 'sequelize'
import crypto from 'crypto'

import {
  authenticated,
  compose,
  hasAllowed,
  verifyFinancialPassword,
} from '../../../shared/infra/http/composers/composables'

import Voucher from '../../../shared/infra/sequelize/models/Voucher'
import UserRole from '../../../shared/infra/sequelize/models/UserRole'
import User from '../../../shared/infra/sequelize/models/User'
import Transaction from '../../../shared/infra/sequelize/models/Transaction'

// import CreditCardBalanceService from '../../balanceCards/services/CreditCardBalanceService'

import {
  STATUS_CONFIRMED,
  TYPE_VOUCHER_ACTIVATION,
} from '../../../util/typesTransactions'
import { cacheProvider } from '../../../shared/container'
// import AppliedCardBalanceService from '../../balanceCards/services/AppliedCardBalanceService'
import CreateVoucherService from '../services/CreateVoucherService'
import Bee from '../../../shared/providers/QueueProvider/Bee'
import ActivateVoucherService from '../services/ActivateVoucherService'
import { CREATED } from '../../../util/VoucherStatus'

const TYPE_BONUS_ID = 1
const STATUS_VALID_ID = 1

export default {
  Voucher: {
    user: obj => User.findByPk(obj.userId),
    leader: obj => User.findByPk(obj.leaderId),
  },
  Query: {
    getNotSentLeaderVouchers: compose(
      authenticated,
      hasAllowed([3])
    )(async (parent, { limit = 10, offset = 1 }, ctx) => {
      const { count: totalCount, rows: node } = await Voucher.findAndCountAll({
        where: {
          leaderId: ctx.user.id,
          userId: null,
        },
        order: [['id', 'ASC']],
        limit,
        offset: (offset - 1) * limit,
      })

      return {
        edge: {
          node,
        },
        pageInfo: {
          totalCount,
          hasPrevPage: offset > 1,
          hasNextPage: totalCount > limit * offset,
        },
      }
    }),

    getNotUsedLeaderVouchers: compose(
      authenticated,
      hasAllowed([3])
    )(async (parent, { limit = 10, offset = 1 }, ctx) => {
      const { count: totalCount, rows: node } = await Voucher.findAndCountAll({
        where: {
          leaderId: ctx.user.id,
          status: CREATED,
          activatedAt: null,
        },
        order: [['id', 'ASC']],
        limit,
        offset: (offset - 1) * limit,
      })

      return {
        edge: {
          node,
        },
        pageInfo: {
          totalCount,
          hasPrevPage: offset > 1,
          hasNextPage: totalCount > limit * offset,
        },
      }
    }),

    getVouchers: compose(
      authenticated,
      hasAllowed([3, 5])
    )(async (_, { limit = 10, offset = 1 }) => {
      const { count: totalCount, rows: node } = await Voucher.findAndCountAll({
        order: [['id', 'ASC']],
        limit,
        offset: (offset - 1) * limit,
      })

      return {
        edge: {
          node,
        },
        pageInfo: {
          totalCount,
          hasPrevPage: offset > 1,
          hasNextPage: totalCount > limit * offset,
        },
      }
    }),
  },
  Mutation: {
    CreateBonusVouchers: compose(
      authenticated,
      hasAllowed([5, 39])
    )(async (parent, { quantity, leaderId, value }) => {
      await CreateVoucherService.execute({ value, quantity, leaderId })
      return 'Vouchers criados com sucesso'
    }),

    SendVoucher: compose(
      authenticated,
      verifyFinancialPassword,
      hasAllowed([3])
    )(async (parent, { voucherId, userEmail }, ctx) => {
      await Bee.add(ActivateVoucherService.key, {
        voucherId,
        userEmail,
        leaderId: ctx.user.id,
      })
      return 'Ativação solicitada, verifique seus vouchers em instantes para confirmar'
    }),
  },
}
