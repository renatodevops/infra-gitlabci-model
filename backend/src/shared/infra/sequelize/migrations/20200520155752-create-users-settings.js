module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('users_settings', {
      user_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.BIGINT,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      type_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.BIGINT,
        references: {
          model: 'settings',
          key: 'id',
        },
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      value: {
        allowNull: true,
        type: DataTypes.STRING(100),
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('users_settings')
  },
}
