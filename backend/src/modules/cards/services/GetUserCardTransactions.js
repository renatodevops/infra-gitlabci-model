import { cacheProvider } from '../../../shared/container'
import CardTransaction from '../../../shared/infra/sequelize/models/CardTransaction'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'

class GetUserCardTransactions {
  async execute({ userId }) {
    const key = `user_card_transactions:${userId}`
    let userCardTransactions = await cacheProvider.recover(key)
    if (!userCardTransactions) {
      userCardTransactions = await CardTransaction.findAll({
        where: {
          userId,
          status: CONFIRMED,
        },
        order: [['id', 'desc']],
      })
      await cacheProvider.save(key, userCardTransactions)
    }

    return userCardTransactions
  }
}
export default new GetUserCardTransactions()
