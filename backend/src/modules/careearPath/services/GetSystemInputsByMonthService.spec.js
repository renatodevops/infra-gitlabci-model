import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
// import crypto from 'crypto'
import { subMonths } from 'date-fns'
import truncate from '../../../../__tests__/utils/truncate'
import CardTransaction from '../../../shared/infra/sequelize/models/CardTransaction'
// import User from '../../../shared/infra/sequelize/models/User'
import GetSystemInputsByMonthService from './GetSystemInputsByMonthService'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import {
  ANNUAL_PLAN,
  APPLIED,
  AVAILABLE,
  // CREDIT,
} from '../../../util/cardsIdentifiers'
import { CONFIRMED, UNCONFIRMED } from '../../../util/cardsTransactionsStatus'
import convertUTCtoLocalTZ from '../../../util/convertUTCtoLOCALTZ'

describe('GetSystemInputsByMonthService tests', () => {
  beforeEach(async () => {
    await truncate([CardTransaction])
  })

  it('should return zero', async () => {
    const input = await GetSystemInputsByMonthService.execute()
    expect(input).toBe(0)
  })
  it('should return 100', async () => {
    await CreateCardTransactionService.execute({
      cardIdentifier: APPLIED,
      description: 'teste',
      status: CONFIRMED,
      userId: 1,
      value: 100,
    })
    await CreateCardTransactionService.execute({
      cardIdentifier: ANNUAL_PLAN,
      description: 'teste',
      status: UNCONFIRMED,
      userId: 1,
      value: 100,
    })
    await CreateCardTransactionService.execute({
      cardIdentifier: AVAILABLE,
      description: 'teste',
      status: CONFIRMED,
      userId: 1,
      value: 100,
    })
    const input = await GetSystemInputsByMonthService.execute()
    expect(input).toBe(100)
  })

  it('should return 0', async () => {
    await CardTransaction.create({
      cardIdentifier: APPLIED,
      description: 'teste',
      status: CONFIRMED,
      userId: 1,
      value: 100,
      createdAt: subMonths(convertUTCtoLocalTZ(new Date()), 1),
    })

    await CreateCardTransactionService.execute({
      cardIdentifier: ANNUAL_PLAN,
      description: 'teste',
      status: UNCONFIRMED,
      userId: 1,
      value: 100,
    })
    await CreateCardTransactionService.execute({
      cardIdentifier: AVAILABLE,
      description: 'teste',
      status: CONFIRMED,
      userId: 1,
      value: 100,
    })
    const input = await GetSystemInputsByMonthService.execute()
    expect(input).toBe(0)
  })
})
