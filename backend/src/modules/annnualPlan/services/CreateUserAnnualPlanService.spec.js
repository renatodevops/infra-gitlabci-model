import '../../../shared/infra/sequelize/index'
import crypto from 'crypto'
import User from '../../../shared/infra/sequelize/models/User'
import CreateUserAnnualPlanService from './CreateUserAnnualPlanService'

let user

describe('CreateUserAnnualPlanService tests', () => {
  beforeEach(async () => {
    user = await User.create({
      name: 'parsedName',
      phone: 'parsedPhone',
      document: 'parsedDocument',
      email: `${crypto.randomBytes(6).toString('hex')}@${crypto
        .randomBytes(6)
        .toString('hex')}.com`,
      born: new Date(),
      terms: true,
      indicatorId: 1,
      passwordHash: crypto.randomBytes(6).toString('hex'),
      selfieLink: 'selfieLink',
      documentFrontLink: 'selfieLink',
      documentBackLink: 'selfieLink',
    })
  })
  it('should return the object for the current month', async () => {
    const userAnnualplan = await CreateUserAnnualPlanService.execute({
      userId: user.id,
      value: 100,
    })

    expect(userAnnualplan).toHaveProperty('userRate', '0.0550')
  })
})
