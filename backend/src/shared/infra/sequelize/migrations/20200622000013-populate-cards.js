module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'cards',
      [
        {
          name: 'Plano anual',
          identifier: 'ANNUAL_PLAN',
          is_visible: true,
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('cards', null, {})
  },
}
