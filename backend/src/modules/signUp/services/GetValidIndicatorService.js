import User from '../../../shared/infra/sequelize/models/User'
import UserUnilevelNode from '../../../shared/infra/sequelize/models/UserUnilevelNode'
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'
import { APPLIED } from '../../../util/cardsIdentifiers'

class GetValidIndicatorService {
  async execute(indicatorCode) {
    const indicatorNode = await UserUnilevelNode.findOne({
      where: {
        userCode: indicatorCode,
      },
    })
    if (!indicatorNode) throw new Error('Indicador não encontrado')
    const indicator = await User.findByPk(indicatorNode.userId)
    if (!indicator) throw new Error('Indicador não encontrado')
    const indicatorBalance = GetUserCardBalanceService.execute({
      userId: indicator.id,
      cardIdentifier: APPLIED,
    })
    if (indicatorBalance < 100.0 && Number(indicator.id) !== 1)
      throw new Error('Indicador não habilitado')
    return indicator
  }
}
export default new GetValidIndicatorService()
