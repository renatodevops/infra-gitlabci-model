import {
  authenticated,
  compose,
  hasAllowed,
} from '../../../shared/infra/http/composers/composables'

import GetUserCardBalanceService from '../services/GetUserCardBalanceService'
import {
  ANNUAL_PLAN,
  APPLIED,
  AVAILABLE,
  CREDIT,
  INCOME,
  VOUCHER,
} from '../../../util/cardsIdentifiers'
import GetUserCardTransactions from '../services/GetUserCardTransactions'
import Card from '../../../shared/infra/sequelize/models/Card'
import User from '../../../shared/infra/sequelize/models/User'
import GetAllUsersService from '../services/GetAllUsersService'

export default {
  CardTransaction: {
    card: obj => Card.findOne({ where: { identifier: obj.cardIdentifier } }),
    user: obj => User.findByPk(obj.userId),
  },
  Query: {
    getAvaliable: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) =>
      GetUserCardBalanceService.execute({
        userId: ctx.user.id,
        cardIdentifier: AVAILABLE,
      })
    ),

    getAllUsersAvaliableBalance: compose(
      authenticated,
      hasAllowed([5])
    )(async () => {
      const allUsers = await GetAllUsersService.execute()
      const balances = await Promise.all(
        allUsers.map(user =>
          GetUserCardBalanceService.execute({
            userId: user.id,
            cardIdentifier: AVAILABLE,
          })
        )
      )
      return balances.reduce((acc, curr) => acc + curr, 0.0)
    }),

    getCredit: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) =>
      GetUserCardBalanceService.execute({
        userId: ctx.user.id,
        cardIdentifier: CREDIT,
      })
    ),

    getAllUsersCreditBalance: compose(
      authenticated,
      hasAllowed([5])
    )(async () => {
      const allUsers = await GetAllUsersService.execute()
      const balances = await Promise.all(
        allUsers.map(user =>
          GetUserCardBalanceService.execute({
            userId: user.id,
            cardIdentifier: CREDIT,
          })
        )
      )
      return balances.reduce((acc, curr) => acc + curr, 0.0)
    }),

    getAllUsersCreditBalanceDetails: compose(
      authenticated,
      hasAllowed([5])
    )(async () => {
      const allUsers = await GetAllUsersService.execute()
      const balances = await Promise.all(
        allUsers.map(async user => {
          const balance = await GetUserCardBalanceService.execute({
            userId: user.id,
            cardIdentifier: CREDIT,
          })
          return {
            name: user.name,
            email: user.email,
            id: user.id,
            balance,
          }
        })
      )
      return balances.filter(b => b.balance !== 0.0)
    }),

    getAllUsers250BalanceDetails: compose(
      authenticated,
      hasAllowed([5])
    )(async () => {
      const allUsers = await GetAllUsersService.execute()
      const balances = await Promise.all(
        allUsers.map(async user => {
          const balance = await GetUserCardBalanceService.execute({
            userId: user.id,
            cardIdentifier: APPLIED,
          })
          return {
            name: user.name,
            email: user.email,
            id: user.id,
            balance,
          }
        })
      )
      return balances.filter(b => b.balance !== 0.0)
    }),

    getAllUsersAnnualBalanceDetails: compose(
      authenticated,
      hasAllowed([5])
    )(async () => {
      const allUsers = await GetAllUsersService.execute()
      const balances = await Promise.all(
        allUsers.map(async user => {
          const balance = await GetUserCardBalanceService.execute({
            userId: user.id,
            cardIdentifier: ANNUAL_PLAN,
          })
          return {
            name: user.name,
            email: user.email,
            id: user.id,
            balance,
          }
        })
      )
      return balances.filter(b => b.balance !== 0.0)
    }),

    getAllUsersAvailableBalanceDetails: compose(
      authenticated,
      hasAllowed([5])
    )(async () => {
      const allUsers = await GetAllUsersService.execute()
      const balances = await Promise.all(
        allUsers.map(async user => {
          const balance = await GetUserCardBalanceService.execute({
            userId: user.id,
            cardIdentifier: AVAILABLE,
          })
          return {
            name: user.name,
            email: user.email,
            id: user.id,
            balance,
          }
        })
      )
      return balances.filter(b => b.balance !== 0.0)
    }),

    getApplied: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) => {
      const [applied, voucher] = await Promise.all([
        GetUserCardBalanceService.execute({
          userId: ctx.user.id,
          cardIdentifier: APPLIED,
        }),
        GetUserCardBalanceService.execute({
          userId: ctx.user.id,
          cardIdentifier: VOUCHER,
        }),
      ])
      return applied + voucher
    }),

    getAllUsers250Balance: compose(
      authenticated,
      hasAllowed([5])
    )(async () => {
      const allUsers = await GetAllUsersService.execute()
      const balances = await Promise.all(
        allUsers.map(user =>
          GetUserCardBalanceService.execute({
            userId: user.id,
            cardIdentifier: APPLIED,
          })
        )
      )
      return balances.reduce((acc, curr) => acc + curr, 0.0)
    }),

    getAllUsersAnnualBalance: compose(
      authenticated,
      hasAllowed([5])
    )(async () => {
      const allUsers = await GetAllUsersService.execute()
      const balances = await Promise.all(
        allUsers.map(user =>
          GetUserCardBalanceService.execute({
            userId: user.id,
            cardIdentifier: ANNUAL_PLAN,
          })
        )
      )
      return balances.reduce((acc, curr) => acc + curr, 0.0)
    }),

    getIncome: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) =>
      GetUserCardBalanceService.execute({
        userId: ctx.user.id,
        cardIdentifier: INCOME,
      })
    ),

    getMyCryptoRentsTransactions: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, args, ctx) => {
      const userId = ctx.user.id

      const userCardsTransactions = await GetUserCardTransactions.execute({
        userId,
      })

      const node = userCardsTransactions.filter(
        transaction => transaction.cardIdentifier === APPLIED
      )

      return {
        edge: {
          node,
        },
        pageInfo: {
          totalCount: userCardsTransactions.length,
          hasPrevPage: false,
          hasNextPage: true,
        },
      }
    }),

    getMyUsersTransfersTransactions: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, args, ctx) => {
      const userId = ctx.user.id

      const userCardsTransactions = await GetUserCardTransactions.execute({
        userId,
      })

      // console.log(userCardsTransactions)

      const node = userCardsTransactions.filter(
        transaction =>
          transaction.description.indexOf('Transferência recebida de') !== -1 ||
          transaction.description.indexOf('Transferência enviada para') !== -1
      )

      return {
        edge: {
          node,
        },
        pageInfo: {
          totalCount: userCardsTransactions.length,
          hasPrevPage: false,
          hasNextPage: true,
        },
      }
    }),

    GetMyCardsTransactions: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) => {
      const userId = ctx.user.id

      const userCardsTransactions = await GetUserCardTransactions.execute({
        userId,
      })

      return {
        edge: {
          node: userCardsTransactions,
        },
        pageInfo: {
          totalCount: userCardsTransactions.length,
          hasPrevPage: false,
          hasNextPage: true,
        },
      }
    }),
  },
}
