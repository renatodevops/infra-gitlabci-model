import User from '../../../shared/infra/sequelize/models/User'
import UserAddress from '../../../shared/infra/sequelize/models/UserAddress'
import UserDocument from '../../../shared/infra/sequelize/models/UserDocument'
import { ADDRESS, DOCUMENTS, SELFIE } from '../../../util/DocumentTypes'

class EvaluateUserDocumentService {
  async execute({ documentId, approved, message }) {
    const document = await UserDocument.findByPk(documentId)

    if (!document) {
      throw new Error('Documento não encontrado.')
    }

    const { documentType, userId, documentLink } = document

    if (approved) {
      const user = await User.findByPk(userId)

      if (!user) {
        throw new Error('Usuário não encontrado')
      }

      switch (documentType) {
        case SELFIE:
          await user.update({ selfieLink: documentLink })
          break
        case DOCUMENTS.front:
          await user.update({ documentFrontLink: documentLink })
          break

        case DOCUMENTS.back:
          await user.update({ documentBackLink: documentLink })
          break

        case ADDRESS:
          await UserAddress.update(
            { addressProofLink: documentLink },
            { where: { userId } }
          )
          break

        default:
          break
      }
    }

    await document.update({ evaluated: true, approved, message })

    return document
  }
}
export default new EvaluateUserDocumentService()
