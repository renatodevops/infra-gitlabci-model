import Sequelize, { Model } from 'sequelize'

class TypeTransaction extends Model {
  static init(sequelize) {
    super.init(
      {
        name: {
          type: Sequelize.STRING,
          validate: {
            isUnique: async name => {
              if (await TypeTransaction.findOne({ where: { name } })) {
                throw Error('Type Transaction already exists')
              }
            },
            notEmpty: {
              msg: 'The name can not be empty',
            },
            len: {
              args: [1, 40],
              msg: 'The name must contain less than 40 characters',
            },
          },
        },
        description: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'The description can not be empty',
            },
            len: {
              args: [1, 80],
              msg: 'The description field must contain less than 80 characters',
            },
          },
        },
        active: {
          type: Sequelize.BOOLEAN,
        },
        isInput: {
          type: Sequelize.BOOLEAN,
        },
      },
      { sequelize, tableName: 'types_transactions' }
    )
    return this
  }
}
export default TypeTransaction
