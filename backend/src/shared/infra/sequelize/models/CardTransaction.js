import Sequelize, { Model } from 'sequelize'

class CardTransaction extends Model {
  static init(sequelize) {
    super.init(
      {
        userId: Sequelize.BIGINT,
        cardIdentifier: Sequelize.STRING,
        description: Sequelize.STRING,
        status: Sequelize.STRING,
        value: Sequelize.DECIMAL,
      },
      { sequelize, tableName: 'cards_transactions' }
    )

    return this
  }
}
export default CardTransaction
