import UserAddress from '../../../shared/infra/sequelize/models/UserAddress'

class CreateUserAddress {
  async execute({
    userId,
    countryId,
    state,
    city,
    neighborhood,
    street,
    number,
    zipCode,
    addressProofLink,
  }) {
    const address = await UserAddress.create({
      userId,
      countryId,
      state,
      city,
      neighborhood,
      street,
      number,
      zipCode,
      addressProofLink,
    })
    return address
  }
}
export default new CreateUserAddress()
