import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'
import CreateUserService from '../../signUp/services/CreateUserService'
import CreateUserBtcPaymentService from './CreateUserBtcPaymentService'
import {
  AWAITING_FOR_FIRST_CONFIRMATION,
  AWAITING_FOR_THIRD_CONFIRMATION,
} from '../../../util/btcTransactionsStatus'
import Bitcoin from '../../../shared/providers/BtcProvider/bitcoinCore'
import ProcessTxidNotificationService from './ProcessTxidNotificationService'

let user

const testWallet = new Bitcoin('test.dat')

const createUser = async (indicatorId = 1) =>
  CreateUserService.execute({
    name: 'parsedName',
    phone: '(84) 99999-9999',
    document: 'parsedDocument',
    email: `${crypto.randomBytes(6).toString('hex')}@${crypto
      .randomBytes(6)
      .toString('hex')}.com`,
    born: new Date(),
    terms: true,
    indicatorId,
    password: '12345678',
    selfieLink: 'selfieLink',
    documentFrontLink: 'selfieLink',
    documentBackLink: 'selfieLink',
  })

describe('CreateUserBtcPaymentService tests', () => {
  beforeAll(async () => {
    user = await createUser()
    await testWallet.generate()
  })
  it('should process a txid notification for the normal flow', async () => {
    const result = await CreateUserBtcPaymentService.execute({
      userId: user.id,
      value: 10,
    })
    const txid = await testWallet.sendToAddress({
      address: result.address,
      amount: result.amount,
      subtractFeeFromAmount: false,
    })

    let btcTransaction = await ProcessTxidNotificationService.handle({
      data: { txid },
    })

    expect(btcTransaction).toHaveProperty('value', '10.00')
    expect(btcTransaction).toHaveProperty('confirmations', 0)
    expect(btcTransaction).toHaveProperty(
      'status',
      AWAITING_FOR_FIRST_CONFIRMATION
    )
    expect(btcTransaction).toHaveProperty('amount', result.amount)
    expect(btcTransaction).toHaveProperty('txid', txid)

    await testWallet.generate(1)

    btcTransaction = await ProcessTxidNotificationService.handle({
      data: { txid },
    })

    expect(btcTransaction).toHaveProperty('value', '10.00')
    expect(btcTransaction).toHaveProperty('confirmations', 1)
    expect(btcTransaction).toHaveProperty(
      'status',
      AWAITING_FOR_THIRD_CONFIRMATION
    )
    expect(btcTransaction).toHaveProperty('amount', result.amount)
    expect(btcTransaction).toHaveProperty('txid', txid)
  })

  it('should process a txid notification when the first notification fails', async () => {
    const result = await CreateUserBtcPaymentService.execute({
      userId: user.id,
      value: 10,
    })
    const txid = await testWallet.sendToAddress({
      address: result.address,
      amount: result.amount,
      subtractFeeFromAmount: false,
    })

    await testWallet.generate(1)

    const btcTransaction = await ProcessTxidNotificationService.handle({
      data: { txid },
    })

    expect(btcTransaction).toHaveProperty('value', '10.00')
    expect(btcTransaction).toHaveProperty('confirmations', 1)
    expect(btcTransaction).toHaveProperty(
      'status',
      AWAITING_FOR_THIRD_CONFIRMATION
    )
    expect(btcTransaction).toHaveProperty('amount', result.amount)
    expect(btcTransaction).toHaveProperty('txid', txid)
  })

  it('should process a txid notification when not paied full value', async () => {
    const result = await CreateUserBtcPaymentService.execute({
      userId: user.id,
      value: 10,
    })

    const wrongAmount = await testWallet.ExchangeToBTC(100)

    const txid = await testWallet.sendToAddress({
      address: result.address,
      amount: wrongAmount,
      subtractFeeFromAmount: false,
    })

    const btcTransaction = await ProcessTxidNotificationService.handle({
      data: { txid },
    })

    expect(btcTransaction).toHaveProperty('value', 100.0)
    expect(btcTransaction).toHaveProperty('confirmations', 0)
    expect(btcTransaction).toHaveProperty(
      'status',
      AWAITING_FOR_FIRST_CONFIRMATION
    )
    expect(btcTransaction).toHaveProperty('amount', wrongAmount)
    expect(btcTransaction).toHaveProperty('txid', txid)
  })

  it('should process a txid notification when it gets too many notifications', async () => {
    const result = await CreateUserBtcPaymentService.execute({
      userId: user.id,
      value: 10,
    })
    const txid = await testWallet.sendToAddress({
      address: result.address,
      amount: result.amount,
      subtractFeeFromAmount: false,
    })

    let btcTransaction = await ProcessTxidNotificationService.handle({
      data: { txid },
    })

    expect(btcTransaction).toHaveProperty('value', '10.00')
    expect(btcTransaction).toHaveProperty('confirmations', 0)
    expect(btcTransaction).toHaveProperty(
      'status',
      AWAITING_FOR_FIRST_CONFIRMATION
    )
    expect(btcTransaction).toHaveProperty('amount', result.amount)
    expect(btcTransaction).toHaveProperty('txid', txid)

    await testWallet.generate(1)

    btcTransaction = await ProcessTxidNotificationService.handle({
      data: { txid },
    })

    expect(btcTransaction).toHaveProperty('value', '10.00')
    expect(btcTransaction).toHaveProperty('confirmations', 1)
    expect(btcTransaction).toHaveProperty(
      'status',
      AWAITING_FOR_THIRD_CONFIRMATION
    )
    expect(btcTransaction).toHaveProperty('amount', result.amount)
    expect(btcTransaction).toHaveProperty('txid', txid)

    btcTransaction = await ProcessTxidNotificationService.handle({
      data: { txid },
    })

    expect(btcTransaction).toBe('Transação ja notificada')
  })
})
