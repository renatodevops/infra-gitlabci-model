import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'
import { subMonths } from 'date-fns'
import { CANCELLED } from 'dns'
import CreateUserService from '../../signUp/services/CreateUserService'
// import { CREDIT } from '../../../util/cardsIdentifiers'
// import CreateCardTransactionService from './CreateCardTransactionService'
// import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import truncate from '../../../../__tests__/utils/truncate'
import CardTransaction from '../../../shared/infra/sequelize/models/CardTransaction'
import GetAllUsersWithDirectIndicatedsMonthlyAportsSumService from './GetAllUsersWithDirectIndicatedsMonthlyAportsSumService'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import { CONFIRMED } from '../../../util/btcTransactionsStatus'
import { ANNUAL_PLAN, APPLIED } from '../../../util/cardsIdentifiers'

let user

const createUser = async (indicatorId = 1) =>
  CreateUserService.execute({
    name: 'parsedName',
    phone: '(84) 99999-9999',
    document: 'parsedDocument',
    email: `${crypto.randomBytes(6).toString('hex')}@${crypto
      .randomBytes(6)
      .toString('hex')}.com`,
    born: new Date(),
    terms: true,
    indicatorId,
    password: '12345678',
    selfieLink: 'selfieLink',
    documentFrontLink: 'selfieLink',
    documentBackLink: 'selfieLink',
  })

describe('GetAllUsersWithDirectIndicatedsMonthlyAportsSumService.spec tests', () => {
  beforeEach(async () => {
    user = await createUser()
    await truncate([CardTransaction])
  })
  it('should return an array with the user id and 0 for indicateds month sum aports', async () => {
    const result = await GetAllUsersWithDirectIndicatedsMonthlyAportsSumService.execute(
      new Date()
    )
    const userResult = result.find(item => item.userId === user.id)
    expect(userResult).toHaveProperty('directsMonthlyAportsSum', 0)
  })
  it('should return an array with the user id and 0 for indicateds month sum aports', async () => {
    await createUser(user.id)
    const result = await GetAllUsersWithDirectIndicatedsMonthlyAportsSumService.execute(
      new Date()
    )
    const userResult = result.find(item => item.userId === user.id)
    expect(userResult).toHaveProperty('directsMonthlyAportsSum', 0)
  })
  it('should return an array with the user id and 100 for indicateds month sum aports', async () => {
    const indicated1 = await createUser(user.id)
    await Promise.all([
      CreateCardTransactionService.execute({
        value: 100,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated1.id,
      }),
    ])
    const result = await GetAllUsersWithDirectIndicatedsMonthlyAportsSumService.execute(
      new Date()
    )
    const userResult = result.find(item => item.userId === user.id)
    expect(userResult).toHaveProperty('directsMonthlyAportsSum', 100)
  })

  it('should return an array with the user id and 200 for indicateds month sum aports', async () => {
    const indicated1 = await createUser(user.id)
    await Promise.all([
      CreateCardTransactionService.execute({
        value: 100,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated1.id,
      }),
      CreateCardTransactionService.execute({
        value: 100,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: ANNUAL_PLAN,
        userId: indicated1.id,
      }),
    ])
    const result = await GetAllUsersWithDirectIndicatedsMonthlyAportsSumService.execute(
      new Date()
    )
    const userResult = result.find(item => item.userId === user.id)
    expect(userResult).toHaveProperty('directsMonthlyAportsSum', 200)
  })
  it('should return an array with the user id and 400 for indicateds month sum aports', async () => {
    const indicated1 = await createUser(user.id)
    const indicated2 = await createUser(user.id)
    await Promise.all([
      CreateCardTransactionService.execute({
        value: 100,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated1.id,
      }),
      CreateCardTransactionService.execute({
        value: 100,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: ANNUAL_PLAN,
        userId: indicated1.id,
      }),
      CreateCardTransactionService.execute({
        value: 100,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated2.id,
      }),
      CreateCardTransactionService.execute({
        value: 100,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: ANNUAL_PLAN,
        userId: indicated2.id,
      }),
    ])
    const result = await GetAllUsersWithDirectIndicatedsMonthlyAportsSumService.execute(
      new Date()
    )
    const userResult = result.find(item => item.userId === user.id)
    expect(userResult).toHaveProperty('directsMonthlyAportsSum', 400)
  })
  it('should return an array with the user id and 300 for indicateds month sum aports', async () => {
    const indicated1 = await createUser(user.id)
    const indicated2 = await createUser(user.id)
    await Promise.all([
      CreateCardTransactionService.execute({
        value: 100,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated1.id,
      }),
      CreateCardTransactionService.execute({
        value: 100,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: ANNUAL_PLAN,
        userId: indicated1.id,
      }),
      CreateCardTransactionService.execute({
        value: 100,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated2.id,
      }),
      CreateCardTransactionService.execute({
        value: 100,
        status: CANCELLED,
        description: 'teste',
        cardIdentifier: ANNUAL_PLAN,
        userId: indicated2.id,
      }),
    ])
    const result = await GetAllUsersWithDirectIndicatedsMonthlyAportsSumService.execute(
      new Date()
    )
    const userResult = result.find(item => item.userId === user.id)
    expect(userResult).toHaveProperty('directsMonthlyAportsSum', 300)
  })
  it('should return an array with the user id and 0 for indicateds month sum aports', async () => {
    const indicated1 = await createUser(user.id)
    const indicated2 = await createUser(user.id)
    await Promise.all([
      CreateCardTransactionService.execute({
        value: 100,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated1.id,
      }),
      CreateCardTransactionService.execute({
        value: 100,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: ANNUAL_PLAN,
        userId: indicated1.id,
      }),
      CreateCardTransactionService.execute({
        value: 100,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: APPLIED,
        userId: indicated2.id,
      }),
      CreateCardTransactionService.execute({
        value: 100,
        status: CONFIRMED,
        description: 'teste',
        cardIdentifier: ANNUAL_PLAN,
        userId: indicated2.id,
      }),
    ])
    const result = await GetAllUsersWithDirectIndicatedsMonthlyAportsSumService.execute(
      subMonths(new Date(), 1)
    )
    const userResult = result.find(item => item.userId === user.id)
    expect(userResult).toHaveProperty('directsMonthlyAportsSum', 0)
  })
})
