/* eslint-disable import/no-cycle */
// import Transaction from '../../../shared/infra/sequelize/models/Transaction'
// import User from '../../../shared/infra/sequelize/models/User'
// import IncomeCardBalanceService from '../../balanceCards/services/IncomeCardBalanceService'
// import BeeQueue from '../../../shared/providers/QueueProvider/Bee'
// import TransferToAvailableService from '../../balanceCards/services/TransferToAvailableService'

// import {
//   TYPE_PROFIT,
//   STATUS_CONFIRMED,
//   TYPE_INCOME_OUT,
//   TYPE_RETURN_TO_INCOME,
//   STATUS_CANCELED,
//   TYPE_ADMINISTRATE_TAX,
// } from '../../../util/typesTransactions'

// import ResidualBonusservice from './ResidualBonusService'

class PayProfitService {
  get key() {
    return 'PayProfitService'
  }

  get schedule() {
    return '0 0 1 * *'
  }

  async handle() {
    // const users = await User.findAll()
    // const usersWithIncome = await Promise.all(
    //   users.map(async user => {
    //     const { balance } = await IncomeCardBalanceService.execute({
    //       userId: user.id,
    //     })
    //     return {
    //       id: user.id,
    //       path: user.path,
    //       incomeBalance: balance,
    //     }
    //   })
    // )
    // const usersWithIncomeGreaterThanZero = usersWithIncome.filter(
    //   user => user.incomeBalance > 0.0
    // )
    // const usersThatPayedResidualBonus = await Promise.all(
    //   usersWithIncomeGreaterThanZero.map(user =>
    //     ResidualBonusservice.execute(user)
    //   )
    // )
    // const ADMINISTRATE_TAX = 0.1
    // const result = await Promise.all(
    //   usersThatPayedResidualBonus.map(async user => {
    //     // cria taxa
    //     await Transaction.create({
    //       userId: user.id,
    //       value: user.incomeBalance * ADMINISTRATE_TAX,
    //       typeId: TYPE_ADMINISTRATE_TAX,
    //       statusId: STATUS_CONFIRMED,
    //       description: `Taxa Administrativa de Rendimento Mensal`,
    //     })
    //     // paga o bonus
    //     const userIncomeValue =
    //       user.incomeBalance * (user.userRate - ADMINISTRATE_TAX)
    //     await BeeQueue.add(TransferToAvailableService.key, {
    //       userId: user.id,
    //       transactionValue: userIncomeValue,
    //       typeId: TYPE_PROFIT,
    //       description: 'Rendimento Mensal',
    //     })
    //     // cria saida do rendimento
    //     await Transaction.create({
    //       userId: user.id,
    //       value: userIncomeValue,
    //       statusId: STATUS_CONFIRMED,
    //       typeId: TYPE_INCOME_OUT,
    //       description: 'Transferência de Rendimento para Disponível',
    //     })
    //     await IncomeCardBalanceService.updateCache(user.id)
    //     return { userIncomeValue, userId: user.id }
    //   })
    // )
    // // verifica se possui retorno de disponivel
    // const returned = await Transaction.findAll({
    //   where: {
    //     statusId: STATUS_CONFIRMED,
    //     typeId: TYPE_RETURN_TO_INCOME,
    //   },
    // })
    // // cancela retorno
    // await Promise.all(
    //   returned.map(async transaction => {
    //     await transaction.update({ statusId: STATUS_CANCELED })
    //     await IncomeCardBalanceService.updateCache(transaction.userId)
    //   })
    // )
    // return result
  }
}
export default new PayProfitService()
