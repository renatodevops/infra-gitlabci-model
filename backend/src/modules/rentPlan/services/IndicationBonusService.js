/* eslint-disable import/no-cycle */
import User from '../../../shared/infra/sequelize/models/User'
import BeeQueue from '../../../shared/providers/QueueProvider/Bee'
import CreateTransferToAvailableCardService from './CreateTransferToAvailableCardService'

class IndicationBonusService {
  async execute({ userId, transferValue }) {
    const user = await User.findByPk(userId)
    const uplinesCount = 5
    const uplines = user.path.reverse().slice(0, uplinesCount)
    const [directId, ...indirects] = uplines

    const indicatorRate = 0.05

    const indicatorBonus = transferValue * indicatorRate

    if (indicatorBonus > 0.0 && directId) {
      await BeeQueue.add(CreateTransferToAvailableCardService.key, {
        userId: directId,
        transferValue: indicatorBonus,
        description: `Bônus direto de ${user.name}`,
      })
    }

    const indirectsRates = [0.006, 0.006, 0.006, 0.006, 0.006]

    const indirectsSerialized = indirects.reduce((acc, curr, index) => {
      if (curr) {
        acc.push({
          id: curr,
          indirectRate: indirectsRates[index],
        })
      }
      return acc
    }, [])

    await Promise.all(
      indirectsSerialized.map(obj =>
        BeeQueue.add(CreateTransferToAvailableCardService.key, {
          userId: obj.id,
          transferValue: obj.indirectRate * transferValue,
          description: `Bônus indireto de ${user.name}`,
        })
      )
    )

    return {
      indirectsSerialized,
      indicatorBonus,
      directId,
    }
  }
}

export default new IndicationBonusService()
