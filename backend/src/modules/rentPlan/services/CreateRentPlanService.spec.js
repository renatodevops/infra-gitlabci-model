import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'
import User from '../../../shared/infra/sequelize/models/User'
import CreateRentPlanService from './CreateRentPlanService'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import { APPLIED, CREDIT, VOUCHER } from '../../../util/cardsIdentifiers'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'

let user

describe('CreateRentPlanService tests', () => {
  beforeEach(async () => {
    user = await User.create({
      name: 'parsedName',
      phone: 'parsedPhone',
      document: 'parsedDocument',
      email: `${crypto.randomBytes(6).toString('hex')}@${crypto
        .randomBytes(6)
        .toString('hex')}.com`,
      born: new Date(),
      terms: true,
      indicatorId: 1,
      passwordHash: crypto.randomBytes(6).toString('hex'),
      selfieLink: 'selfieLink',
      documentFrontLink: 'selfieLink',
      documentBackLink: 'selfieLink',
    })
  })
  it('should return Valor mínimo não alcançado', async () => {
    const result = await CreateRentPlanService.handle({
      data: { userId: user.id },
    })
    expect(result).toBe('Valor mínimo não alcançado')
  })

  it('should return Transferencia realizada com sucesso and update the user cads balance', async () => {
    await CreateCardTransactionService.execute({
      cardIdentifier: CREDIT,
      userId: user.id,
      status: CONFIRMED,
      description: 'teste',
      value: 100,
    })

    const result = await CreateRentPlanService.handle({
      data: { userId: user.id },
    })
    expect(result).toBe('Transferencia realizada com sucesso')

    const [creditBalance, appliedBalance] = await Promise.all([
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: CREDIT,
      }),
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: APPLIED,
      }),
    ])

    expect(creditBalance).toBe(0)
    expect(appliedBalance).toBe(100)
  })

  it('should return Transferencia realizada com sucesso and update the user cads balance', async () => {
    await CreateCardTransactionService.execute({
      cardIdentifier: CREDIT,
      userId: user.id,
      status: CONFIRMED,
      description: 'teste',
      value: 100,
    })

    await CreateCardTransactionService.execute({
      cardIdentifier: VOUCHER,
      userId: user.id,
      status: CONFIRMED,
      description: 'teste',
      value: 100,
    })

    const result = await CreateRentPlanService.handle({
      data: { userId: user.id },
    })
    expect(result).toBe('Transferencia realizada com sucesso')

    const [creditBalance, appliedBalance, voucherBalance] = await Promise.all([
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: CREDIT,
      }),
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: APPLIED,
      }),
      GetUserCardBalanceService.execute({
        userId: user.id,
        cardIdentifier: VOUCHER,
      }),
    ])

    expect(creditBalance).toBe(0)
    expect(appliedBalance).toBe(100)
    expect(voucherBalance).toBe(0)
  })
})
