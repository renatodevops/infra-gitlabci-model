module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('system_logs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      user_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      category: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      address: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      txid: {
        allowNull: true,
        type: DataTypes.STRING,
      },
      amount: {
        allowNull: true,
        type: DataTypes.DECIMAL(10, 8),
      },
      message: {
        allowNull: true,
        type: DataTypes.STRING,
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('system_logs')
  },
}
