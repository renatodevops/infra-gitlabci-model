import Sequelize, { Model } from 'sequelize'

class Role extends Model {
  static init(sequelize) {
    super.init(
      {
        id: {
          primaryKey: true,
          type: Sequelize.BIGINT,
        },
        userId: Sequelize.BIGINT,
        adminId: Sequelize.BIGINT,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
        deletedAt: Sequelize.DATE,
        fileLink: Sequelize.STRING,
        userComment: Sequelize.STRING,
        declaredUsdValue: Sequelize.DECIMAL,
        adminAnswer: Sequelize.STRING,
        fee: Sequelize.DECIMAL,
        net_usd_value: Sequelize.DECIMAL,
        status: Sequelize.STRING,
      },
      { sequelize, tableName: 'users_manual_deposits' }
    )
    return this
  }

  // static associate(models) {
  //   this.hasMany(models.UserRole, { as: 'userRole', foreignKey: 'roleId' })
  // }
}
export default Role
