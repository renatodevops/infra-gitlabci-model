import { factory } from 'factory-girl'

import User from '../src/shared/infra/sequelize/models/User'

factory.define('User', User, {
  code: null,
  name: 'iromann',
  document: '000.000.000-99',
  email: 'iroman@avengers.com',
  password: '12345678',
  born: '2019-01-01',
  terms: true,
  countryId: 1,
  indicatorId: null,
})

export default factory
