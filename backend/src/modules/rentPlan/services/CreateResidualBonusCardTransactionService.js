/* eslint-disable import/no-cycle */
import User from '../../../shared/infra/sequelize/models/User'
import BeeQueue from '../../../shared/providers/QueueProvider/Bee'

// import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
// import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
// import { INCOME } from '../../../util/cardsIdentifiers'
import CreateTransferToAvailableCardService from './CreateTransferToAvailableCardService'

class CreateResidualBonusCardTransactionService {
  async execute({ userId, totalDailyIncome }) {
    const user = await User.findByPk(userId)
    const uplines = await User.findAll({
      where: {
        id: user.path,
      },
      limit: 5,
      order: [['id', 'DESC']],
    })

    const bonusRates = [0.04, 0.04, 0.04, 0.04, 0.04]

    const uplinesSerialized = uplines.reduce((acc, curr, index) => {
      if (curr) {
        acc.push({
          id: curr.id,
          uplineRate: bonusRates[index],
        })
      }
      return acc
    }, [])

    const totalUplinesRates = uplinesSerialized.reduce((total, upline) => {
      total += upline.uplineRate
      return total
    }, 0.0)

    await Promise.all(
      uplinesSerialized.map(obj =>
        BeeQueue.add(CreateTransferToAvailableCardService.key, {
          userId: obj.id,
          transferValue: obj.uplineRate * totalDailyIncome,
          description: `Bônus residual de ${user.name}`,
        })
      )
    )

    const userRate = 1.0 - totalUplinesRates
    const userDailyIncome = totalDailyIncome * userRate

    return { userId, userDailyIncome }
  }
}
export default new CreateResidualBonusCardTransactionService()
