import { cacheProvider } from '../../../shared/container'
import User from '../../../shared/infra/sequelize/models/User'

class GetAllUsersService {
  async execute() {
    const key = `all_users`

    let value = await cacheProvider.recover(key)

    if (!value) {
      value = await User.findAll()
      await cacheProvider.save(key, value)
    }
    return value
  }
}
export default new GetAllUsersService()
