module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface
      .createTable('transactions', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.BIGINT,
        },
        user_id: {
          type: DataTypes.BIGINT,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id',
          },
        },
        type_id: {
          type: DataTypes.BIGINT,
          allowNull: false,
          references: {
            model: 'types_transactions',
            key: 'id',
          },
          unique: 'uniqueTag',
        },
        status_id: {
          type: DataTypes.BIGINT,
          allowNull: false,
          references: {
            model: 'status_transactions',
            key: 'id',
          },
        },
        created_at: {
          allowNull: false,
          type: DataTypes.DATE,
          defaultValue: new Date(),
        },
        updated_at: {
          allowNull: false,
          type: DataTypes.DATE,
          defaultValue: new Date(),
        },
        deleted_at: {
          allowNull: true,
          type: DataTypes.DATE,
        },
        value: {
          allowNull: false,
          type: DataTypes.DECIMAL(10, 2),
        },
        fee: {
          allowNull: true,
          type: DataTypes.DECIMAL,
        },
        txid: {
          allowNull: true,
          type: DataTypes.STRING,
          unique: 'uniqueTag',
        },
      })
      .then(() => {
        return queryInterface.addConstraint(
          'transactions',
          ['txid', 'type_id'],
          {
            type: 'unique',
            name: 'uniqueTag',
          }
        )
      })
  },

  down: queryInterface => {
    return queryInterface.dropTable('transactions')
  },
}
