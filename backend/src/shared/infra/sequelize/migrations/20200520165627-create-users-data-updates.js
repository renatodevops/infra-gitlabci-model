module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('users_data_updates', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      user_id: {
        allowNull: false,
        type: DataTypes.BIGINT,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      type_id: {
        allowNull: false,
        type: DataTypes.BIGINT,
        references: {
          model: 'types_updates',
          key: 'id',
        },
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      expiration: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      code: {
        allowNull: false,
        type: DataTypes.STRING(60),
      },
      active: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('users_data_updates')
  },
}
