import { subHours } from 'date-fns'

export default function convertUTCtoLocalTZ(now = new Date()) {
  return subHours(now, 3)
}
