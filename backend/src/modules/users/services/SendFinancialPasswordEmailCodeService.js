import crypto from 'crypto'
import { cacheProvider } from '../../../shared/container'
import MailDevelopment from '../../../shared/providers/MailProvider/development'
import MailProduction from '../../../shared/providers/MailProvider/production'
import User from '../../../shared/infra/sequelize/models/User'

class SendFinancialPasswordEmailCodeService {
  async execute({ userId }) {
    const user = await User.findByPk(userId)
    if (!user) throw new Error('Usuário não encontrado')

    const key = `user_financial_password:${user.id}`

    let value = await cacheProvider.recover(key)
    if (!value) {
      value = crypto
        .randomBytes(16)
        .toString('hex')
        .slice(0, 10)

      await cacheProvider.save(key, value)
    }

    const data = {
      to: `${user.name} <${user.email}>`,
      subject: 'Solicitação de criação de senha financeira',
      template: 'financial_password_creation_request',
      context: {
        title: 'Solicitação de criação de senha financeira',
        name: user.name,
        code: value,
      },
    }

    if (process.env.DEVELOPMENT === 'true') await MailDevelopment.sendMail(data)
    else await MailProduction.sendMail(data)

    return `Código enviado com sucesso ao email: ${user.email}`
  }
}

export default new SendFinancialPasswordEmailCodeService()
