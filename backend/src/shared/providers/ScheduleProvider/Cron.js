import { CronJob } from 'cron'

import scheduleConfig from '../../../config/schedule'

import BeeQueue from '../QueueProvider/Bee'

// import AccumulateProfit from '../../../modules/profit/services/AccumulateProfitService'
// import PayProfit from '../../../modules/profit/services/PayProfitService'
// import CarrerPlanService from '../../../modules/carrerPlan/services/CarrerPlanService'
// import ReturnProfitToIncomService from '../../../modules/profit/services/ReturnProfitToIncomeService'
import CreateDailyIncomeCardTransactionService from '../../../modules/rentPlan/services/CreateDailyIncomeCardTransactionService'
import CreateMonthlyIncomeCardTransactionService from '../../../modules/rentPlan/services/CreateMonthlyIncomeCardTransactionService'
import CreateMonthlyIncomeAnnualPlanService from '../../../modules/annnualPlan/services/CreateMonthlyIncomeAnnualPlanService'
import CreateMonthlyCareearPathPaymentsService from '../../../modules/careearPath/services/CreateMonthlyCareearPathPaymentsService'

const jobs = [
  CreateDailyIncomeCardTransactionService,
  CreateMonthlyIncomeCardTransactionService,
  CreateMonthlyIncomeAnnualPlanService,
  CreateMonthlyCareearPathPaymentsService,
  // AccumulateProfit,
  // PayProfit,
  // CarrerPlanService,
  // ReturnProfitToIncomService,
]

class Schedule {
  constructor() {
    this.tasks = {}

    this.init()
  }

  init() {
    jobs.forEach(({ key, schedule, start = true, runOnInit = false }) => {
      this.tasks[key] = new CronJob({
        context: scheduleConfig.context,
        timeZone: scheduleConfig.timezone,
        cronTime: schedule,
        onTick: () => BeeQueue.add(key),
        runOnInit,
        start,
      })
    })
  }

  processTasks() {
    return this.tasks
  }
}

export default new Schedule()
