module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'users_unilevel_nodes',
      [
        {
          user_id: 1,
          indicator_id: 1,
          user_code: 'LOCKA',
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.dropTable('users_unilevel_nodes')
  },
}
