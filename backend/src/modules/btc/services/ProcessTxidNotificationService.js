import { Op } from 'sequelize'
import { depositBtcWallet } from '../../../shared/container'
import BtcTransaction from '../../../shared/infra/sequelize/models/BtcTransaction'
import {
  AWAITING_FOR_FIRST_CONFIRMATION,
  AWAITING_FOR_PAYMENT,
  AWAITING_FOR_THIRD_CONFIRMATION,
} from '../../../util/btcTransactionsStatus'

class ProcessTxidNotificationService {
  get key() {
    return 'ProcessTxidNotificationService'
  }

  async handle({ data }) {
    const { txid } = data
    const info = await depositBtcWallet.getTransaction(txid)
    if (!info) return 'Sistema indisponível no momento'

    const alreadyDone = await BtcTransaction.findOne({
      where: { txid, confirmations: { [Op.gt]: 0 } },
    })
    if (alreadyDone) return 'Transação ja notificada'

    const { details, blockhash, confirmations, amount } = info

    if (amount < 0.0) return 'Transação de saída da carteira'

    const walletDetails = details.find(detail => detail.category === 'receive')

    const { address, amount: transactionAmount } = walletDetails

    const block = await depositBtcWallet.getBlock({ blockhash })
    const height = block && block.height

    const btcTransaction = await BtcTransaction.findOne({ where: { address } })

    let receivedValue = btcTransaction.value

    if (btcTransaction.status === AWAITING_FOR_PAYMENT) {
      const timeSinceSolicitation = new Date() - btcTransaction.createdAt
      const FIVE_MIN = 5 * 60 * 1000
      const timeOk = timeSinceSolicitation <= FIVE_MIN
      const amountOk = btcTransaction.amount === transactionAmount.toFixed(8)
      receivedValue =
        timeOk && amountOk
          ? btcTransaction.value
          : await depositBtcWallet.ExchangeFromBTC(transactionAmount.toFixed(8))
    }

    return btcTransaction.update({
      value: receivedValue,
      confirmations,
      status:
        confirmations === 0
          ? AWAITING_FOR_FIRST_CONFIRMATION
          : AWAITING_FOR_THIRD_CONFIRMATION,
      blockHeight: height,
      amount: transactionAmount,
      txid,
    })
  }
}
export default new ProcessTxidNotificationService()
