module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('groups_transactions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      name: {
        unique: true,
        allowNull: false,
        type: DataTypes.STRING(40),
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('groups_transactions')
  },
}
