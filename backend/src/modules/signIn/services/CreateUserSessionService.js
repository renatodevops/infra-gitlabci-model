import * as jwt from 'jsonwebtoken'
import User from '../../../shared/infra/sequelize/models/User'
import BCryptHashProvider from '../../../shared/providers/HashProvider/BCryptHashProvider'
import authConfig from '../../../config/auth'

class CreateUserSessionService {
  async execute({ email, password }) {
    const user = await User.findOne({ where: { email } })
    if (!user) throw Error('E-mail ou senha incorretos.')

    const passwordMatched = await BCryptHashProvider.compareHash(
      password,
      user.passwordHash
    )
    if (!passwordMatched) throw Error('E-mail ou senha incorretos.')

    const token = jwt.sign({ id: user.id }, authConfig.secret)

    return { user, token }
  }
}
export default new CreateUserSessionService()
