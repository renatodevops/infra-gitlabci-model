import UserRole from '../../sequelize/models/UserRole'
import GetUserFinancialPasswordService from '../../../../modules/users/services/GetUserFinancialPasswordService'
import BCryptHashProvider from '../../../providers/HashProvider/BCryptHashProvider'

export function compose(...fcns) {
  if (fcns.length === 0) return o => o
  if (fcns.length === 1) return fcns[0]

  return resolve =>
    fcns.reduceRight((fnPrevious, fn) => fn(fnPrevious), resolve)
}

export function authenticated(fn) {
  return (parent, args, ctx, info) => {
    if (!ctx.user) throw Error('Acesso não permitido.')

    return fn(parent, args, ctx, info)
  }
}

export function hasAllowed(roleId) {
  return resolve => async (parent, args, ctx, info) => {
    const max = await UserRole.findOne({
      where: {
        userId: ctx.user.id,
        roleId,
        active: true,
      },
    })

    if (!max) throw Error('Acesso não permitido.')
    return resolve(parent, args, ctx, info)
  }
}

export function verifyFinancialPassword(resolve) {
  return async (parent, args, ctx, info) => {
    const userFinancialpassword = await GetUserFinancialPasswordService.execute(
      {
        userId: ctx.user.id,
      }
    )
    if (!userFinancialpassword) throw Error('Senha financeira não cadastrada')

    const passwordMatched = await BCryptHashProvider.compareHash(
      args.financialPassword,
      userFinancialpassword
    )

    if (!passwordMatched) throw Error('Senha financeira incorreta')

    return resolve(parent, args, ctx, info)
  }
}
