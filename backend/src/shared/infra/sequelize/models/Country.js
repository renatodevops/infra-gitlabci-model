import Sequelize, { Model } from 'sequelize'

class Country extends Model {
  static init(sequelize) {
    super.init(
      {
        ibgeCode: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'The ibgeCode can not be empty',
            },
            len: {
              args: [3, 3],
              msg: 'The ibgeCode must contain 3 characters',
            },
          },
        },
        phoneCode: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'The phoneCode can not be empty',
            },
            len: {
              args: [1, 4],
              msg: 'The phoneCode must contain less than 5 characters',
            },
          },
        },
        iso: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'The iso can not be empty',
            },
            len: {
              args: [2, 2],
              msg: 'The iso must contain 2 characters',
            },
          },
        },
        iso3: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'The iso3 can not be empty',
            },
            len: {
              args: [3, 3],
              msg: 'The iso3 must contain 3 characters',
            },
          },
        },
        lang: {
          type: Sequelize.STRING,
          validate: {
            len: {
              args: [0, 2],
              msg: 'The lang must contain less than 3 characters',
            },
          },
        },
        name: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'The name can not be empty',
            },
            len: {
              args: [1, 45],
              msg: 'The name must contain less than 45 characters',
            },
          },
        },
        nameFormal: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'The nameFormal can not be empty',
            },
            len: {
              args: [1, 80],
              msg: 'The nameFormal must contain less than 80 characters',
            },
          },
        },
        nationality: {
          type: Sequelize.STRING,
          validate: {
            len: {
              args: [0, 40],
              msg: 'The nationality must contain less than 41 characters',
            },
          },
        },
      },
      { sequelize, tableName: 'countries' }
    )
    return this
  }

  static associate() {}
}
export default Country
