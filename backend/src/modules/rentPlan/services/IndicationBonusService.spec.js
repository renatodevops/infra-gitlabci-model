import '../../../shared/infra/sequelize/index'
import crypto from 'crypto'

import CreateUserService from '../../signUp/services/CreateUserService'
import IndicationBonusService from './IndicationBonusService'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import { APPLIED } from '../../../util/cardsIdentifiers'
import UserAutoRenovationSetting from '../../../shared/infra/sequelize/models/UserAutoRenovationSetting'
import UserFinancialPassword from '../../../shared/infra/sequelize/models/UserFinancialPassword'
import BCryptHashProvider from '../../../shared/providers/HashProvider/BCryptHashProvider'

let user

const createUser = async (indicatorId = 1) => {
  const newUser = await CreateUserService.execute({
    name: 'parsedName',
    phone: '(84) 99999-9999',
    document: 'parsedDocument',
    email: `${crypto.randomBytes(3).toString('hex')}@${crypto
      .randomBytes(3)
      .toString('hex')}.com`,
    born: new Date(),
    terms: true,
    indicatorId,
    password: '12345678',
    selfieLink: 'selfieLink',
    documentFrontLink: 'selfieLink',
    documentBackLink: 'selfieLink',
  })
  await CreateCardTransactionService.execute({
    userId: newUser.id,
    description: 'teste',
    value: 100,
    status: CONFIRMED,
    cardIdentifier: APPLIED,
  })
  await UserAutoRenovationSetting.create({ userId: newUser.id, isActive: true })
  await UserFinancialPassword.create({
    userId: newUser.id,
    financialPassword: await BCryptHashProvider.generateHash('11111111'),
  })
  return newUser
}

describe('GetUserCardBalanceService tests', () => {
  beforeAll(async () => {
    user = await createUser()
  })
  it('should return 5 uplines and rates', async () => {
    const upline6 = await createUser(user.id)
    const upline5 = await createUser(upline6.id)
    const upline4 = await createUser(upline5.id)
    const upline3 = await createUser(upline4.id)
    const upline2 = await createUser(upline3.id)
    const direct = await createUser(upline2.id)
    const investor = await createUser(direct.id)

    const {
      indirectsSerialized,
      indicatorBonus,
      directId,
    } = await IndicationBonusService.execute({
      userId: investor.id,
      transferValue: 100.0,
    })

    expect(indicatorBonus).toBe(5.0)
    expect(directId).toBe(direct.id)

    expect(indirectsSerialized.length).toBe(4)

    expect(indirectsSerialized[0]).toHaveProperty('id', upline2.id)
    expect(indirectsSerialized[0]).toHaveProperty('indirectRate', 0.006)

    expect(indirectsSerialized[1]).toHaveProperty('id', upline3.id)
    expect(indirectsSerialized[1]).toHaveProperty('indirectRate', 0.006)

    expect(indirectsSerialized[2]).toHaveProperty('id', upline4.id)
    expect(indirectsSerialized[2]).toHaveProperty('indirectRate', 0.006)

    expect(indirectsSerialized[3]).toHaveProperty('id', upline5.id)
    expect(indirectsSerialized[3]).toHaveProperty('indirectRate', 0.006)

    // expect(indirectsSerialized[4]).toHaveProperty('id', upline6.id)
    // expect(indirectsSerialized[4]).toHaveProperty('indirectRate', 0.007)
  })
  it.skip('should return 3 uplines and rates', async () => {
    const direct = await createUser(user.id)
    const investor = await createUser(direct.id)

    const {
      indirectsSerialized,
      indicatorBonus,
      directId,
    } = await IndicationBonusService.execute({
      userId: investor.id,
      transferValue: 100.0,
    })

    expect(indicatorBonus).toBe(5.0)
    expect(directId).toBe(direct.id)

    expect(indirectsSerialized.length).toBe(2)

    expect(indirectsSerialized[0]).toHaveProperty('id', user.id)
    expect(indirectsSerialized[0]).toHaveProperty('indirectRate', 0.007)

    expect(indirectsSerialized[1]).toHaveProperty('id', '1')
    expect(indirectsSerialized[1]).toHaveProperty('indirectRate', 0.007)
  })
})
