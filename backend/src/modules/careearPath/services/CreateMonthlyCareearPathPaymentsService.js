import { subMonths, format } from 'date-fns'
import GetAllUsersWithDirectIndicatedsMonthlyAportsSumService from './GetAllUsersWithDirectIndicatedsMonthlyAportsSumService'
import GetSystemInputsByMonthService from './GetSystemInputsByMonthService'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import { AVAILABLE } from '../../../util/cardsIdentifiers'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'

class CreateMonthlyCareearPathPaymentsService {
  get key() {
    return 'CreateMonthlyCareearPathPaymentsService'
  }

  get schedule() {
    return '10 0 1 * *'
  }

  async handle() {
    const lastMonth = subMonths(new Date(), 1)
    const lastMonthSystemInput = await GetSystemInputsByMonthService.execute(
      lastMonth
    )

    const supervisorsBonus = lastMonthSystemInput * 0.005
    const managersBonus = lastMonthSystemInput * 0.015

    const usersWithdirectsMonthlyAportsSum = await GetAllUsersWithDirectIndicatedsMonthlyAportsSumService.execute(
      lastMonth
    )

    const supervisors1 = usersWithdirectsMonthlyAportsSum.filter(
      obj =>
        obj.directsMonthlyAportsSum >= 1000 &&
        obj.directsMonthlyAportsSum < 2000
    )
    const supervisors2 = usersWithdirectsMonthlyAportsSum.filter(
      obj =>
        obj.directsMonthlyAportsSum >= 2000 &&
        obj.directsMonthlyAportsSum < 3000
    )
    const supervisors3 = usersWithdirectsMonthlyAportsSum.filter(
      obj =>
        obj.directsMonthlyAportsSum >= 3000 &&
        obj.directsMonthlyAportsSum < 5000
    )
    const supervisors4 = usersWithdirectsMonthlyAportsSum.filter(
      obj =>
        obj.directsMonthlyAportsSum >= 5000 &&
        obj.directsMonthlyAportsSum < 6000
    )
    const managers1 = usersWithdirectsMonthlyAportsSum.filter(
      obj =>
        obj.directsMonthlyAportsSum >= 6000 &&
        obj.directsMonthlyAportsSum < 7000
    )
    const managers2 = usersWithdirectsMonthlyAportsSum.filter(
      obj =>
        obj.directsMonthlyAportsSum >= 7000 &&
        obj.directsMonthlyAportsSum < 8000
    )
    const managers3 = usersWithdirectsMonthlyAportsSum.filter(
      obj =>
        obj.directsMonthlyAportsSum >= 8000 &&
        obj.directsMonthlyAportsSum < 10000
    )
    const managers4 = usersWithdirectsMonthlyAportsSum.filter(
      obj => obj.directsMonthlyAportsSum >= 10000
    )

    const supervisors1BonusValue = supervisorsBonus * 0.1
    const supervisors2BonusValue = supervisorsBonus * 0.2
    const supervisors3BonusValue = supervisorsBonus * 0.3
    const supervisors4BonusValue = supervisorsBonus * 0.4

    const managers1BonusValue = managersBonus * 0.1
    const managers2BonusValue = managersBonus * 0.2
    const managers3BonusValue = managersBonus * 0.3
    const managers4BonusValue = managersBonus * 0.4

    const supervisors1Length = supervisors1.length
    const supervisors2Length = supervisors2.length
    const supervisors3Length = supervisors3.length
    const supervisors4Length = supervisors4.length

    if (supervisors1Length > 0) {
      const supervisors1IndividualBonus =
        supervisors1BonusValue / supervisors1Length
      await Promise.all(
        supervisors1.map(obj =>
          CreateCardTransactionService.execute({
            userId: obj.userId,
            cardIdentifier: AVAILABLE,
            value: supervisors1IndividualBonus,
            status: CONFIRMED,
            description: `Bônus Supervisor 1 - ${format(lastMonth, 'MM/yyyy')}`,
          })
        )
      )
    }
    if (supervisors2Length > 0) {
      const supervisors2IndividualBonus =
        supervisors2BonusValue / supervisors2Length
      await Promise.all(
        supervisors2.map(obj =>
          CreateCardTransactionService.execute({
            userId: obj.userId,
            cardIdentifier: AVAILABLE,
            value: supervisors2IndividualBonus,
            status: CONFIRMED,
            description: `Bônus Supervisor 2 - ${format(lastMonth, 'MM/yyyy')}`,
          })
        )
      )
    }
    if (supervisors3Length > 0) {
      const supervisors3IndividualBonus =
        supervisors3BonusValue / supervisors3Length
      await Promise.all(
        supervisors3.map(obj =>
          CreateCardTransactionService.execute({
            userId: obj.userId,
            cardIdentifier: AVAILABLE,
            value: supervisors3IndividualBonus,
            status: CONFIRMED,
            description: `Bônus Supervisor 3 - ${format(lastMonth, 'MM/yyyy')}`,
          })
        )
      )
    }
    if (supervisors4Length > 0) {
      const supervisors4IndividualBonus =
        supervisors4BonusValue / supervisors4Length
      await Promise.all(
        supervisors4.map(obj =>
          CreateCardTransactionService.execute({
            userId: obj.userId,
            cardIdentifier: AVAILABLE,
            value: supervisors4IndividualBonus,
            status: CONFIRMED,
            description: `Bônus Supervisor 3 - ${format(lastMonth, 'MM/yyyy')}`,
          })
        )
      )
    }

    const managers1Length = managers1.length
    const managers2Length = managers2.length
    const managers3Length = managers3.length
    const managers4Length = managers4.length

    if (managers1Length > 0) {
      const managers1IndividualBonus = managers1BonusValue / managers1Length
      await Promise.all(
        managers1.map(obj =>
          CreateCardTransactionService.execute({
            userId: obj.userId,
            cardIdentifier: AVAILABLE,
            value: managers1IndividualBonus,
            status: CONFIRMED,
            description: `Bônus Gerente 1 - ${format(lastMonth, 'MM/yyyy')}`,
          })
        )
      )
    }
    if (managers2Length > 0) {
      const managers2IndividualBonus = managers2BonusValue / managers2Length
      await Promise.all(
        managers2.map(obj =>
          CreateCardTransactionService.execute({
            userId: obj.userId,
            cardIdentifier: AVAILABLE,
            value: managers2IndividualBonus,
            status: CONFIRMED,
            description: `Bônus Gerente 2 - ${format(lastMonth, 'MM/yyyy')}`,
          })
        )
      )
    }
    if (managers3Length > 0) {
      const managers3IndividualBonus = managers3BonusValue / managers3Length
      await Promise.all(
        managers3.map(obj =>
          CreateCardTransactionService.execute({
            userId: obj.userId,
            cardIdentifier: AVAILABLE,
            value: managers3IndividualBonus,
            status: CONFIRMED,
            description: `Bônus Gerente 3 - ${format(lastMonth, 'MM/yyyy')}`,
          })
        )
      )
    }
    if (managers4Length > 0) {
      const managers4IndividualBonus = managers4BonusValue / managers4Length
      await Promise.all(
        managers4.map(obj =>
          CreateCardTransactionService.execute({
            userId: obj.userId,
            cardIdentifier: AVAILABLE,
            value: managers4IndividualBonus,
            status: CONFIRMED,
            description: `Bônus Gerente 4 - ${format(lastMonth, 'MM/yyyy')}`,
          })
        )
      )
    }

    return {
      supervisorsBonus,
      managersBonus,
      supervisors1,
      supervisors2,
      supervisors3,
      supervisors4,
      managers1,
      managers2,
      managers3,
      managers4,
      supervisors1BonusValue,
      supervisors2BonusValue,
      supervisors3BonusValue,
      supervisors4BonusValue,
      managers1BonusValue,
      managers2BonusValue,
      managers3BonusValue,
      managers4BonusValue,
      supervisors1Length,
      supervisors2Length,
      supervisors3Length,
      supervisors4Length,
      managers1Length,
      managers2Length,
      managers3Length,
      managers4Length,
    }
  }
}
export default new CreateMonthlyCareearPathPaymentsService()
