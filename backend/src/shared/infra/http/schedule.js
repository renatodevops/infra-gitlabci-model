import 'dotenv/config'

import '../sequelize'

import Schedule from '../../providers/ScheduleProvider/Cron'

Schedule.processTasks()

console.log(`Schedule on.`)
