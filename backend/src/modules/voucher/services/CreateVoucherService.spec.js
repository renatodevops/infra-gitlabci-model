import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'
import CreateUserService from '../../signUp/services/CreateUserService'

import CreateVoucherService from './CreateVoucherService'
import { CREATED } from '../../../util/VoucherStatus'

let user

const createUser = async (indicatorId = 1) =>
  CreateUserService.execute({
    name: 'parsedName',
    phone: '(84) 99999-9999',
    document: 'parsedDocument',
    email: `${crypto.randomBytes(6).toString('hex')}@${crypto
      .randomBytes(6)
      .toString('hex')}.com`,
    born: new Date(),
    terms: true,
    indicatorId,
    password: '12345678',
    selfieLink: 'selfieLink',
    documentFrontLink: 'selfieLink',
    documentBackLink: 'selfieLink',
  })

describe('CreateVoucherService tests', () => {
  beforeAll(async () => {
    user = await createUser()
  })
  it('should create five voucher ans role for leader', async () => {
    const { vouchers, leaderRole } = await CreateVoucherService.execute({
      leaderId: user.id,
      quantity: 5,
      value: 100,
    })
    expect(vouchers.length).toBe(5)
    expect(vouchers[0]).toHaveProperty('status', CREATED)
    expect(vouchers[0]).toHaveProperty('activatedAt', null)
    expect(vouchers[0]).toHaveProperty('hash')
    expect(vouchers[0]).toHaveProperty('leaderId', user.id)

    expect(leaderRole).toHaveProperty('userId', user.id)
    expect(leaderRole).toHaveProperty('roleId', '3')
  })
})
