module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'status_voucher',
      [
        {
          // 1
          name: 'Válido',
          active: true,
        },
        {
          // 2
          name: 'Inválido',
          active: true,
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('status_voucher', null, {})
  },
}
