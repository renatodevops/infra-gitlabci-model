module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'vouchers',
      [
        {
          // 1
          creator_id: 1,
          leader_id: 2,
          owner_id: 2,
          value: 10000000,
          hash:
            '65bb095e8a352dda6aa2fa6ff37acf0ca4f91b891eba0211d6ce1b17f3186f09',
          type_id: 2,
          status_id: 1,
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('vouchers', null, {})
  },
}
