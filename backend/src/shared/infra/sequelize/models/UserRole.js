import Sequelize, { Model } from 'sequelize'

class UserRole extends Model {
  static init(sequelize) {
    super.init(
      {
        userId: {
          type: Sequelize.BIGINT,
          primaryKey: true,
        },
        roleId: {
          type: Sequelize.BIGINT,
          primaryKey: true,
        },
        active: Sequelize.BOOLEAN,
      },
      { sequelize, tableName: 'users_roles' }
    )
    return this
  }

  static associate(models) {
    this.belongsTo(models.Role, {
      onDelete: 'CASCADE',
    })
    this.belongsTo(models.User)
  }
}
export default UserRole
