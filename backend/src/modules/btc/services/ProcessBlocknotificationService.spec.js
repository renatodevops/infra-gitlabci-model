import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'
import truncate from '../../../../__tests__/utils/truncate'
import CreateUserService from '../../signUp/services/CreateUserService'
import CreateUserBtcPaymentService from './CreateUserBtcPaymentService'
import {
  AWAITING_FOR_FIRST_CONFIRMATION,
  AWAITING_FOR_THIRD_CONFIRMATION,
} from '../../../util/btcTransactionsStatus'
import Bitcoin from '../../../shared/providers/BtcProvider/bitcoinCore'
import ProcessTxidNotificationService from './ProcessTxidNotificationService'
import ProcessBlocknotificationService from './ProcessBlocknotificationService'
import BtcTransaction from '../../../shared/infra/sequelize/models/BtcTransaction'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'

let user

const testWallet = new Bitcoin('test.dat')

const createUser = async (indicatorId = 1) =>
  CreateUserService.execute({
    name: 'parsedName',
    phone: '(84) 99999-9999',
    document: 'parsedDocument',
    email: `${crypto.randomBytes(6).toString('hex')}@${crypto
      .randomBytes(6)
      .toString('hex')}.com`,
    born: new Date(),
    terms: true,
    indicatorId,
    password: '12345678',
    selfieLink: 'selfieLink',
    documentFrontLink: 'selfieLink',
    documentBackLink: 'selfieLink',
  })

describe('CreateUserBtcPaymentService tests', () => {
  beforeEach(async () => {
    user = await createUser()
    await testWallet.generate()
    await truncate([BtcTransaction])
  })
  it('should chabge btc transaction confirmations from 1 to 3 for the normal flow', async () => {
    const result = await CreateUserBtcPaymentService.execute({
      userId: user.id,
      value: 10,
    })
    const txid = await testWallet.sendToAddress({
      address: result.address,
      amount: result.amount,
      subtractFeeFromAmount: false,
    })

    let btcTransaction = await ProcessTxidNotificationService.handle({
      data: { txid },
    })

    expect(btcTransaction).toHaveProperty('value', '10.00')
    expect(btcTransaction).toHaveProperty('confirmations', 0)
    expect(btcTransaction).toHaveProperty(
      'status',
      AWAITING_FOR_FIRST_CONFIRMATION
    )
    expect(btcTransaction).toHaveProperty('amount', result.amount)
    expect(btcTransaction).toHaveProperty('txid', txid)

    await testWallet.generate(1)

    btcTransaction = await ProcessTxidNotificationService.handle({
      data: { txid },
    })

    expect(btcTransaction).toHaveProperty('value', '10.00')
    expect(btcTransaction).toHaveProperty('confirmations', 1)
    expect(btcTransaction).toHaveProperty(
      'status',
      AWAITING_FOR_THIRD_CONFIRMATION
    )
    expect(btcTransaction).toHaveProperty('amount', result.amount)
    expect(btcTransaction).toHaveProperty('txid', txid)

    const [blockhash] = await testWallet.generate(1)

    let btcTransactions = await ProcessBlocknotificationService.handle({
      data: { blockhash },
    })

    expect(btcTransactions.length).toBe(1)
    expect(btcTransactions[0]).toHaveProperty(
      'status',
      AWAITING_FOR_THIRD_CONFIRMATION
    )
    expect(btcTransactions[0]).toHaveProperty('confirmations', 2)

    const [blockhash2] = await testWallet.generate(1)

    btcTransactions = await ProcessBlocknotificationService.handle({
      data: { blockhash: blockhash2 },
    })

    expect(btcTransactions.length).toBe(1)
    expect(btcTransactions[0]).toHaveProperty('status', CONFIRMED)
    expect(btcTransactions[0]).toHaveProperty('confirmations', 3)

    const [blockhash3] = await testWallet.generate(1)

    btcTransactions = await ProcessBlocknotificationService.handle({
      data: { blockhash: blockhash3 },
    })

    expect(btcTransactions.length).toBe(0)
  })
  it('should chabge btc transaction confirmations from 1 to 3  when the second block notification miss', async () => {
    const result = await CreateUserBtcPaymentService.execute({
      userId: user.id,
      value: 10,
    })
    const txid = await testWallet.sendToAddress({
      address: result.address,
      amount: result.amount,
      subtractFeeFromAmount: false,
    })

    let btcTransaction = await ProcessTxidNotificationService.handle({
      data: { txid },
    })

    expect(btcTransaction).toHaveProperty('value', '10.00')
    expect(btcTransaction).toHaveProperty('confirmations', 0)
    expect(btcTransaction).toHaveProperty(
      'status',
      AWAITING_FOR_FIRST_CONFIRMATION
    )
    expect(btcTransaction).toHaveProperty('amount', result.amount)
    expect(btcTransaction).toHaveProperty('txid', txid)

    await testWallet.generate(1)

    btcTransaction = await ProcessTxidNotificationService.handle({
      data: { txid },
    })

    expect(btcTransaction).toHaveProperty('value', '10.00')
    expect(btcTransaction).toHaveProperty('confirmations', 1)
    expect(btcTransaction).toHaveProperty(
      'status',
      AWAITING_FOR_THIRD_CONFIRMATION
    )
    expect(btcTransaction).toHaveProperty('amount', result.amount)
    expect(btcTransaction).toHaveProperty('txid', txid)

    await testWallet.generate(1)
    const [blockhash] = await testWallet.generate(1)

    let btcTransactions = await ProcessBlocknotificationService.handle({
      data: { blockhash },
    })

    expect(btcTransactions.length).toBe(1)
    expect(btcTransactions[0]).toHaveProperty('status', CONFIRMED)
    expect(btcTransactions[0]).toHaveProperty('confirmations', 3)

    const [blockhash2] = await testWallet.generate(1)

    btcTransactions = await ProcessBlocknotificationService.handle({
      data: { blockhash: blockhash2 },
    })

    expect(btcTransactions.length).toBe(0)
  })
  it('should chabge btc transaction confirmations from 1 to 3 when the third block notification miss', async () => {
    const result = await CreateUserBtcPaymentService.execute({
      userId: user.id,
      value: 10,
    })
    const txid = await testWallet.sendToAddress({
      address: result.address,
      amount: result.amount,
      subtractFeeFromAmount: false,
    })

    let btcTransaction = await ProcessTxidNotificationService.handle({
      data: { txid },
    })

    expect(btcTransaction).toHaveProperty('value', '10.00')
    expect(btcTransaction).toHaveProperty('confirmations', 0)
    expect(btcTransaction).toHaveProperty(
      'status',
      AWAITING_FOR_FIRST_CONFIRMATION
    )
    expect(btcTransaction).toHaveProperty('amount', result.amount)
    expect(btcTransaction).toHaveProperty('txid', txid)

    await testWallet.generate(1)

    btcTransaction = await ProcessTxidNotificationService.handle({
      data: { txid },
    })

    expect(btcTransaction).toHaveProperty('value', '10.00')
    expect(btcTransaction).toHaveProperty('confirmations', 1)
    expect(btcTransaction).toHaveProperty(
      'status',
      AWAITING_FOR_THIRD_CONFIRMATION
    )
    expect(btcTransaction).toHaveProperty('amount', result.amount)
    expect(btcTransaction).toHaveProperty('txid', txid)

    const [blockhash] = await testWallet.generate(1)

    let btcTransactions = await ProcessBlocknotificationService.handle({
      data: { blockhash },
    })

    expect(btcTransactions.length).toBe(1)
    expect(btcTransactions[0]).toHaveProperty(
      'status',
      AWAITING_FOR_THIRD_CONFIRMATION
    )
    expect(btcTransactions[0]).toHaveProperty('confirmations', 2)

    await testWallet.generate(1)

    const [blockhash2] = await testWallet.generate(1)

    btcTransactions = await ProcessBlocknotificationService.handle({
      data: { blockhash: blockhash2 },
    })

    expect(btcTransactions.length).toBe(1)
    expect(btcTransactions[0]).toHaveProperty('status', CONFIRMED)
    expect(btcTransactions[0]).toHaveProperty('confirmations', 4)

    const [blockhash3] = await testWallet.generate(1)

    btcTransactions = await ProcessBlocknotificationService.handle({
      data: { blockhash: blockhash3 },
    })

    expect(btcTransactions.length).toBe(0)
  })
})
