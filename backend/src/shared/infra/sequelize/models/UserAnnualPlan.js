import Sequelize, { Model } from 'sequelize'

class AnnualPlanRate extends Model {
  static init(sequelize) {
    super.init(
      {
        userId: Sequelize.INTEGER,
        indicatorId: Sequelize.INTEGER,
        cardTransactionId: Sequelize.INTEGER,
        userRate: Sequelize.DECIMAL,
        indicatorRate: Sequelize.DECIMAL,
        value: Sequelize.DECIMAL,
        status: Sequelize.STRING,
        createdAt: Sequelize.DATE,
      },
      { sequelize, tableName: 'users_annual_plans' }
    )

    return this
  }
}
export default AnnualPlanRate
