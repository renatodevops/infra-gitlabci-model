import Sequelize, { Model } from 'sequelize'

class AnnualPlanRate extends Model {
  static init(sequelize) {
    super.init(
      {
        rate: Sequelize.DECIMAL,
        year: Sequelize.INTEGER,
        month: Sequelize.INTEGER,
        hasBeenApplied: Sequelize.BOOLEAN,
      },
      { sequelize, tableName: 'anunual_plan_rate' }
    )

    return this
  }
}
export default AnnualPlanRate
