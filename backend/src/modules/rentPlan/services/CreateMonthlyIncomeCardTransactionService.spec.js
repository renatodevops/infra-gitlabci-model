import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import { INCOME } from '../../../util/cardsIdentifiers'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'
import CreateMonthlyIncomeCardTransactionService from './CreateMonthlyIncomeCardTransactionService'
import CreateUserService from '../../signUp/services/CreateUserService'

let user

const createUser = async (indicatorId = 1) =>
  CreateUserService.execute({
    name: 'parsedName',
    phone: '(84) 99999-9999',
    document: 'parsedDocument',
    email: `${crypto.randomBytes(6).toString('hex')}@${crypto
      .randomBytes(6)
      .toString('hex')}.com`,
    born: new Date(),
    terms: true,
    indicatorId,
    password: '12345678',
    selfieLink: 'selfieLink',
    documentFrontLink: 'selfieLink',
    documentBackLink: 'selfieLink',
  })

describe('CreateMonthlyIncomeCardTransactionService tests', () => {
  beforeAll(async () => {
    user = await createUser()
  })
  it('should update the income balance to zero and return the 90 for profit', async () => {
    await CreateCardTransactionService.execute({
      cardIdentifier: INCOME,
      userId: user.id,
      value: 100.0,
      status: CONFIRMED,
      description: 'teste',
    })

    const result = await CreateMonthlyIncomeCardTransactionService.handle()

    const incomeBalance = await GetUserCardBalanceService.execute({
      userId: user.id,
      cardIdentifier: INCOME,
    })
    expect(incomeBalance).toBe(0.0)

    const userProfit = result.find(item => item.userId === user.id)
    expect(userProfit).toHaveProperty('userIncomeValue', 90.0)
  })
  it('should update the income balance to sero and return the 82 for profit', async () => {
    const investor = await createUser(user.id)

    await CreateCardTransactionService.execute({
      cardIdentifier: INCOME,
      userId: investor.id,
      value: 100.0,
      status: CONFIRMED,
      description: 'teste',
    })

    const result = await CreateMonthlyIncomeCardTransactionService.handle()

    const incomeBalance = await GetUserCardBalanceService.execute({
      userId: investor.id,
      cardIdentifier: INCOME,
    })
    expect(incomeBalance).toBe(0.0)

    const userProfit = result.find(item => item.userId === investor.id)
    expect(userProfit).toHaveProperty('userIncomeValue', 90.0)
  })
  it('should update the income balance to sero and return the 70 for profit', async () => {
    const upline5 = await createUser(user.id)
    const upline4 = await createUser(upline5.id)
    const upline3 = await createUser(upline4.id)
    const upline2 = await createUser(upline3.id)
    const upline1 = await createUser(upline2.id)
    const direct = await createUser(upline1.id)
    const investor = await createUser(direct.id)

    await CreateCardTransactionService.execute({
      cardIdentifier: INCOME,
      userId: investor.id,
      value: 100.0,
      status: CONFIRMED,
      description: 'teste',
    })

    const result = await CreateMonthlyIncomeCardTransactionService.handle()

    const incomeBalance = await GetUserCardBalanceService.execute({
      userId: investor.id,
      cardIdentifier: INCOME,
    })
    expect(incomeBalance).toBe(0.0)

    const userProfit = result.find(item => item.userId === investor.id)
    expect(userProfit).toHaveProperty('userIncomeValue', 90.0)
  })
})
