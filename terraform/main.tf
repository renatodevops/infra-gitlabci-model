provider "digitalocean" {
  token = var.do_token

  spaces_access_id = var.access_id
  spaces_secret_key = var.secret_key
}

resource "digitalocean_ssh_key" "id_rsa" {
  name = "id_rsa"
  public_key = file("${path.module}/keys/id_rsa.pub")
}

resource "digitalocean_tag" "infragitlabci" {
  name = "infragitlabci"
}
