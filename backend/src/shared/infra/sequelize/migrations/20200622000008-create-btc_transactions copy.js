module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('btc_transactions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      user_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      address: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      txid: {
        type: DataTypes.STRING,
        allowNull: true,
        defaultValue: null,
      },
      category: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      status: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      value: {
        allowNull: false,
        type: DataTypes.DECIMAL(10, 2),
      },
      amount: {
        allowNull: false,
        type: DataTypes.DECIMAL(10, 8),
      },
      confirmations: {
        allowNull: false,
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      block_height: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('btc_transactions')
  },
}
