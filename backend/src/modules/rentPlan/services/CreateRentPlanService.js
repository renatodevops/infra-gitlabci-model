/* eslint-disable import/no-cycle */
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'
import { APPLIED, CREDIT, VOUCHER } from '../../../util/cardsIdentifiers'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import IndicationBonusService from './IndicationBonusService'

class CreateRentPlanService {
  get key() {
    return 'CreateRentPlanService'
  }

  async handle({ data }) {
    const { userId } = data

    const creditBalance = await GetUserCardBalanceService.execute({
      userId,
      cardIdentifier: CREDIT,
    })

    const MAX_CREDIT_BALANCE = 100.0

    if (creditBalance < MAX_CREDIT_BALANCE) return 'Valor mínimo não alcançado'

    const voucherBalance = await GetUserCardBalanceService.execute({
      cardIdentifier: VOUCHER,
      userId,
    })
    if (voucherBalance > 0.0)
      await CreateCardTransactionService.execute({
        cardIdentifier: VOUCHER,
        userId,
        status: CONFIRMED,
        description: 'Cancelamento de voucher ',
        value: voucherBalance * -1,
      })

    await Promise.all([
      CreateCardTransactionService.execute({
        cardIdentifier: CREDIT,
        userId,
        status: CONFIRMED,
        description: 'Saída para locação de cripto ativo',
        value: creditBalance * -1,
      }),
      CreateCardTransactionService.execute({
        cardIdentifier: APPLIED,
        userId,
        status: CONFIRMED,
        description: 'Locação de cripto ativo',
        value: creditBalance,
      }),
      IndicationBonusService.execute({
        userId,
        transferValue: creditBalance,
      }),
    ])

    return 'Transferencia realizada com sucesso'
  }
}

export default new CreateRentPlanService()
