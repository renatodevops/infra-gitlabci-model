/* eslint-disable import/no-cycle */
// import { Op } from 'sequelize'
// import { format } from 'date-fns-tz'

// import StrategyPlanning from '../../../shared/infra/sequelize/models/StrategyPlanning'
// import User from '../../../shared/infra/sequelize/models/User'

// import scheduleConfig from '../../../config/schedule'

// import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'
// import CreateResidualBonusCardTransactionService from './CreateResidualBonusCardTransactionService'
// import { APPLIED, INCOME } from '../../../util/cardsIdentifiers'
// import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
// import { CONFIRMED } from '../../../util/cardsTransactionsStatus'

class CreateDailyIncomeCardTransactionService {
  get key() {
    return 'CreateDailyIncomeCardTransactionService'
  }

  get schedule() {
    return '0 20 * * *'
  }

  async handle() {
    // const [todayPlanning] = await StrategyPlanning.findAll({
    //   where: {
    //     date: format(new Date(), 'yyyy-MM-dd', {
    //       timeZone: scheduleConfig.timezone,
    //     }),
    //     confirmed: false,
    //     dailyPlanned: { [Op.gt]: 0.0 },
    //   },
    //   // include: ['strategy'],
    // })
    // const users = await User.findAll()
    // const balances = await Promise.all(
    //   users.map(async user => {
    //     const balance = await GetUserCardBalanceService.execute({
    //       userId: user.id,
    //       cardIdentifier: APPLIED,
    //     })
    //     return { id: user.id, balance }
    //   })
    // )
    // const balancesGreaterThanZero = balances.filter(user => user.balance > 0.0)
    // const dailyPlanned = Number(todayPlanning.dailyPlanned)
    // const usersThatPayedResidualBonus = await Promise.all(
    //   balancesGreaterThanZero.map(user => {
    //     return CreateResidualBonusCardTransactionService.execute({
    //       userId: user.id,
    //       totalDailyIncome: dailyPlanned * user.balance,
    //     })
    //   })
    // )
    // await todayPlanning.update({ isEditable: false, confirmed: true })
    // return Promise.all(
    //   usersThatPayedResidualBonus.map(({ userId, userDailyIncome }) =>
    //     CreateCardTransactionService.execute({
    //       cardIdentifier: INCOME,
    //       description: 'Rendimento diário',
    //       status: CONFIRMED,
    //       userId,
    //       value: userDailyIncome,
    //     })
    //   )
    // )
  }
}

export default new CreateDailyIncomeCardTransactionService()
