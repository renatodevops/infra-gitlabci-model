/* eslint-disable no-unused-vars */
import {
  authenticated,
  compose,
  hasAllowed,
} from '../../../shared/infra/http/composers/composables'

import StrategyPlanning from '../../../shared/infra/sequelize/models/StrategyPlanning'

export default {
  Plannings: {
    monthlyPlanned: obj => (obj.monthlyPlanned * 100.0).toFixed(4),
    dailyPlanned: obj => (obj.dailyPlanned * 100.0).toFixed(4),
  },

  Query: {
    getStrategyPlanning: compose(
      authenticated,
      hasAllowed([5, 36])
    )(async (parent, { strategyId = 1, month, year }, ctx, info) => {
      return StrategyPlanning.getPlannings({ strategyId, month, year })
    }),
  },

  Mutation: {
    UpdateStrategyPlannig: compose(
      authenticated,
      hasAllowed([5, 35, 37])
    )(async (parent, { plannings, monthlyPlanned }, ctx, info) => {
      const promisses = plannings.map(async ({ id, dailyPlanned }) => {
        const plan = await StrategyPlanning.findByPk(id)
        return plan.update({
          monthlyPlanned: monthlyPlanned * 0.01,
          dailyPlanned: plan.isEditable
            ? dailyPlanned * 0.01
            : plan.dailyPlanned,
        })
      })

      return Promise.all(promisses)
    }),
  },
}
