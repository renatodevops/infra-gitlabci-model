module.exports = {
  apps: [
    {
      name: 'api',
      script: 'sucrase-node src/shared/infra/http/server.js',
      env: {
        NODE_ENV: 'development',
      },
      env_production: {
        NODE_ENV: 'production',
      },
    },
    {
      name: 'queue',
      script: 'sucrase-node src/shared/infra/http/queue.js',
    },
    {
      name: 'schedule',
      script: 'sucrase-node src/shared/infra/http/schedule.js',
    },
  ],
}
