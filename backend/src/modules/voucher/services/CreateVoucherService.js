import crypto from 'crypto'
import UserRole from '../../../shared/infra/sequelize/models/UserRole'
import Voucher from '../../../shared/infra/sequelize/models/Voucher'
import { CREATED } from '../../../util/VoucherStatus'

class CreateVoucherService {
  async execute({ quantity, leaderId, value }) {
    const vouchers = await Promise.all(
      new Array(quantity).fill('').map(async () =>
        Voucher.create({
          hash: crypto.randomBytes(16).toString('hex'),
          value,
          leaderId,
          status: CREATED,
        })
      )
    )
    const ROLE_LEADER_ID = 3
    let leaderRole = await UserRole.findOne({
      where: {
        userId: leaderId,
        roleId: ROLE_LEADER_ID,
      },
    })
    if (!leaderRole)
      leaderRole = await UserRole.create({
        userId: leaderId,
        roleId: ROLE_LEADER_ID,
      })

    return { vouchers, leaderRole }
  }
}
export default new CreateVoucherService()
