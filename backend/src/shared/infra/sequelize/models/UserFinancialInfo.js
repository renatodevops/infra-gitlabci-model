import Sequelize, { Model } from 'sequelize'
import { compareSync } from 'bcryptjs'

class FinancialInfo extends Model {
  static init(sequelize) {
    super.init(
      {
        userId: {
          primaryKey: true,
          type: Sequelize.BIGINT,
        },
        typeId: {
          primaryKey: true,
          type: Sequelize.BIGINT,
        },
        value: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'The value can not be empty',
            },
            len: {
              args: [1, 100],
              msg: 'value must contain less than 100 characters',
            },
          },
        },
      },
      { sequelize, tableName: 'users_financial_infos' }
    )

    this.addHook('beforeCreate', async ({ userId, typeId }) => {
      if (await FinancialInfo.findOne({ where: { userId, typeId } })) {
        throw Error('User already have this type of financial info created')
      }
    })

    return this
  }

  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'userId',
      as: 'user',
    })
  }

  checkFinancialPassword(password) {
    return compareSync(password, this.value)
  }
}

export default FinancialInfo
