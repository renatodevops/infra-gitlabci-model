import User from '../../../shared/infra/sequelize/models/User'
import UserUnilevelNode from '../../../shared/infra/sequelize/models/UserUnilevelNode'
import CreateUserUnilevelNodeService from '../../signUp/services/CreateUserUnilevelNodeService'
// import GetValidIndicatorService from '../../signUp/services/GetValidIndicatorService'
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'
import { APPLIED, VOUCHER } from '../../../util/cardsIdentifiers'

class GetUserIndicationlinkService {
  async execute({ userId }) {
    let node = await UserUnilevelNode.findOne({ where: { userId } })

    if (!node) {
      const user = await User.findByPk(userId)
      node = await CreateUserUnilevelNodeService.execute({
        userId,
        indicatorId: user.indicatorId,
      })
    }
    const userBalance = await GetUserCardBalanceService.execute({
      userId,
      cardIdentifier: APPLIED,
    })
    const voucherBalance = await GetUserCardBalanceService.execute({
      userId,
      cardIdentifier: VOUCHER,
    })
    return userBalance + voucherBalance > 0
      ? `${process.env.FRONT_URL}/register/${node.userCode}`
      : null
  }
}
export default new GetUserIndicationlinkService()
