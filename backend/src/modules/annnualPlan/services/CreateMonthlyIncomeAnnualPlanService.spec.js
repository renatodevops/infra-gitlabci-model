import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'

import { addDays, startOfMonth, subMonths } from 'date-fns'
import truncate from '../../../../__tests__/utils/truncate'

import User from '../../../shared/infra/sequelize/models/User'
import UserAnnualPlan from '../../../shared/infra/sequelize/models/UserAnnualPlan'
import CreateMonthlyIncomeAnnualPlanService from './CreateMonthlyIncomeAnnualPlanService'
import { ANNUAL_PLAN, AVAILABLE } from '../../../util/cardsIdentifiers'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'

import CardTransaction from '../../../shared/infra/sequelize/models/CardTransaction'
import AnnualPlanRate from '../../../shared/infra/sequelize/models/AnnualPlanRate'
import convertUTCtoLocalTZ from '../../../util/convertUTCtoLOCALTZ'

let user

describe('CreateMonthlyIncomeAnnualPlanService tests', () => {
  beforeEach(async () => {
    await truncate([CardTransaction, AnnualPlanRate, UserAnnualPlan])
    user = await User.create({
      name: 'parsedName',
      phone: 'parsedPhone',
      document: 'parsedDocument',
      email: `${crypto.randomBytes(6).toString('hex')}@${crypto
        .randomBytes(6)
        .toString('hex')}.com`,
      born: new Date(),
      terms: true,
      indicatorId: 1,
      passwordHash: crypto.randomBytes(6).toString('hex'),
      selfieLink: 'selfieLink',
      documentFrontLink: 'selfieLink',
      documentBackLink: 'selfieLink',
    })
  })
  it('should return a user 1 income transaction', async () => {
    const now = convertUTCtoLocalTZ()
    const lastMonth = subMonths(now, 1)
    // await AnnualPlanRate.create({
    //   year: lastMonth.getUTCFullYear(),
    //   month: lastMonth.getUTCMonth(),
    //   rate: 0.055,
    // })

    const aport = await CardTransaction.create({
      cardIdentifier: ANNUAL_PLAN,
      userId: user.id,
      status: CONFIRMED,
      description: 'teste',
      value: 100,
      createdAt: addDays(startOfMonth(lastMonth), 15),
    })

    await UserAnnualPlan.create({
      cardTransactionId: aport.id,
      userId: user.id,
      indicatorId: user.indicatorId,
      userRate: 0.055,
      indicatorRate: 0.0,
      value: 100,
      status: 'AGUARDANDO ANÁLISE',
    })
    const [transaction] = await CreateMonthlyIncomeAnnualPlanService.handle()

    expect(transaction.indicatorComission).toBe(null)
    expect(transaction.userIncome).toHaveProperty('userId', user.id)
    expect(transaction.userIncome).toHaveProperty('cardIdentifier', AVAILABLE)
    expect(transaction.userIncome).toHaveProperty('status', CONFIRMED)
    expect(transaction.userIncome).toHaveProperty(
      'value',
      ((((31 - 15) * 0.055) / 31) * 100.0).toFixed(2)
    )
  })

  it('should return a user 2 income transaction', async () => {
    const now = convertUTCtoLocalTZ()
    const lastMonth = subMonths(now, 1)
    const monthBefore = subMonths(now, 2)
    // await AnnualPlanRate.create({
    //   year: lastMonth.getUTCFullYear(),
    //   month: lastMonth.getUTCMonth(),
    //   rate: 0.055,
    // })

    // await AnnualPlanRate.create({
    //   year: monthBefore.getUTCFullYear(),
    //   month: monthBefore.getUTCMonth(),
    //   rate: 0.08,
    // })

    let aport = await CardTransaction.create({
      cardIdentifier: ANNUAL_PLAN,
      userId: user.id,
      status: CONFIRMED,
      description: 'teste',
      value: 100,
      createdAt: addDays(startOfMonth(lastMonth), 15),
    })
    await UserAnnualPlan.create({
      cardTransactionId: aport.id,
      userId: user.id,
      indicatorId: user.indicatorId,
      userRate: 0.055,
      indicatorRate: 0.0,
      value: 100,
      status: 'AGUARDANDO ANÁLISE',
    })

    aport = await CardTransaction.create({
      cardIdentifier: ANNUAL_PLAN,
      userId: user.id,
      status: CONFIRMED,
      description: 'teste',
      value: 1000,
      createdAt: addDays(startOfMonth(monthBefore), 15),
    })
    await UserAnnualPlan.create({
      cardTransactionId: aport.id,
      userId: user.id,
      indicatorId: user.indicatorId,
      userRate: 0.08,
      indicatorRate: 0.0,
      value: 1000,
      status: 'AGUARDANDO ANÁLISE',
    })

    const [
      transaction1,
      transaction2,
    ] = await CreateMonthlyIncomeAnnualPlanService.handle()

    expect(transaction1.indicatorComission).toBe(null)
    expect(transaction1.userIncome).toHaveProperty('userId', user.id)
    expect(transaction1.userIncome).toHaveProperty('cardIdentifier', AVAILABLE)
    expect(transaction1.userIncome).toHaveProperty('status', CONFIRMED)
    expect(transaction1.userIncome).toHaveProperty(
      'value',
      ((((31 - 15) * 0.055) / 31) * 100.0).toFixed(2)
    )

    expect(transaction2.indicatorComission).toBe(null)
    expect(transaction2.userIncome).toHaveProperty('userId', user.id)
    expect(transaction2.userIncome).toHaveProperty('cardIdentifier', AVAILABLE)
    expect(transaction2.userIncome).toHaveProperty('status', CONFIRMED)
    expect(transaction2.userIncome).toHaveProperty('value', '80.00')
  })

  it('should return a user  and his indicator 1 income transaction', async () => {
    const now = convertUTCtoLocalTZ()
    const lastMonth = subMonths(now, 1)
    // await AnnualPlanRate.create({
    //   year: lastMonth.getUTCFullYear(),
    //   month: lastMonth.getUTCMonth(),
    //   rate: 0.055,
    // })

    const aport = await CardTransaction.create({
      cardIdentifier: ANNUAL_PLAN,
      userId: user.id,
      status: CONFIRMED,
      description: 'teste',
      value: 100,
      createdAt: addDays(startOfMonth(lastMonth), 15),
    })

    await UserAnnualPlan.create({
      cardTransactionId: aport.id,
      userId: user.id,
      indicatorId: user.indicatorId,
      userRate: 0.045,
      indicatorRate: 0.01,
      value: 100,
      status: 'AGUARDANDO ANÁLISE',
    })
    const [transaction] = await CreateMonthlyIncomeAnnualPlanService.handle()

    expect(transaction.userIncome).toHaveProperty('userId', user.id)
    expect(transaction.userIncome).toHaveProperty('cardIdentifier', AVAILABLE)
    expect(transaction.userIncome).toHaveProperty('status', CONFIRMED)
    expect(transaction.userIncome).toHaveProperty(
      'value',
      ((((31 - 15) * 0.045) / 31) * 100.0).toFixed(2)
    )

    expect(transaction.indicatorComission).toHaveProperty(
      'userId',
      user.indicatorId
    )
    expect(transaction.indicatorComission).toHaveProperty(
      'cardIdentifier',
      AVAILABLE
    )
    expect(transaction.indicatorComission).toHaveProperty('status', CONFIRMED)
    expect(transaction.indicatorComission).toHaveProperty(
      'value',
      ((((31 - 15) * 0.01) / 31) * 100.0).toFixed(2)
    )
  })
})
