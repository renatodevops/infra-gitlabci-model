import { depositBtcWallet } from '../../../shared/container'

import BtcTransaction from '../../../shared/infra/sequelize/models/BtcTransaction'
import {
  AWAITING_FOR_THIRD_CONFIRMATION,
  CONFIRMED,
} from '../../../util/btcTransactionsStatus'
import { CREDIT } from '../../../util/cardsIdentifiers'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'

class ProcessBlockNotificationService {
  get key() {
    return 'ProcessBlockNotificationService'
  }

  async handle({ data }) {
    const { blockhash } = data

    const block = await depositBtcWallet.getBlock({ blockhash })
    const height = block && block.height

    if (!height) return `Erro na aquisição da altura do bloco`

    const btcDeposits = await BtcTransaction.findAll({
      where: {
        status: AWAITING_FOR_THIRD_CONFIRMATION,
      },
    })

    const MIN_CONFIRMATIONS = 3

    return Promise.all(
      btcDeposits.map(async btcDeposit => {
        const confirmations = height - Number(btcDeposit.blockHeight) + 1

        const status =
          confirmations >= MIN_CONFIRMATIONS
            ? CONFIRMED
            : AWAITING_FOR_THIRD_CONFIRMATION

        const updated = await btcDeposit.update({
          confirmations,
          status,
        })

        if (status === CONFIRMED)
          await CreateCardTransactionService.execute({
            userId: btcDeposit.userId,
            value: btcDeposit.value,
            cardIdentifier: CREDIT,
            status: CONFIRMED,
            description: 'Depósito em Bitcoin',
          })

        return updated
      })
    )
  }
}
export default new ProcessBlockNotificationService()
