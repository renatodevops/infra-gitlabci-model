import Sequelize, { Model } from 'sequelize'

class SystemBtcWallet extends Model {
  static init(sequelize) {
    super.init(
      {
        name: {
          type: Sequelize.STRING,
        },
        label: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'The name can not be empty',
            },
            len: {
              args: [1, 45],
              msg: 'The name field must contain less than 45 characters',
            },
          },
        },
        isOperable: {
          type: Sequelize.BOOLEAN,
          validate: {
            notEmpty: {
              msg: 'The value active can not be empty',
            },
          },
        },
        active: {
          type: Sequelize.BOOLEAN,
          validate: {
            notEmpty: {
              msg: 'The value active can not be empty',
            },
          },
        },
      },
      { sequelize, tableName: 'system_btc_wallets' }
    )

    this.addHook('beforeCreate', async systemBtcWallets => {
      if (
        await SystemBtcWallet.findOne({
          where: { label: systemBtcWallets.label },
        })
      ) {
        throw Error('SystemBtcWallet already exists')
      }
    })
    return this
  }
}

export default SystemBtcWallet
