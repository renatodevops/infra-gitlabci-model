// import crypto from 'crypto'
import {
  authenticated,
  compose,
  hasAllowed,
} from '../../../shared/infra/http/composers/composables'
import Transaction from '../../../shared/infra/sequelize/models/Transaction'

import User from '../../../shared/infra/sequelize/models/User'
// import getAdminsBalanceService from '../services/GetAdminsBalanceService'

import {
  STATUS_CONFIRMED,
  TYPE_MANUAL_ACTIVATION,
} from '../../../util/typesTransactions'

import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import { CREDIT } from '../../../util/cardsIdentifiers'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'

export default {
  // ManualActivationBalance: {
  //   owner: obj => User.findByPk(obj.ownerId),
  // },

  ManualActivations: {
    user: obj => User.findByPk(obj.userId),
    activator: async obj => {
      const activatorId = obj.txid.split('#')[0].replace('activador_id=', '')
      return User.findByPk(activatorId)
    },
  },

  Query: {
    getManualActivationsFilterOptions: compose(
      authenticated,
      hasAllowed([5])
    )(async () => {
      // const transactions = await Transaction.findAll({
      //   where: {
      //     typeId: TYPE_MANUAL_ACTIVATION,
      //     statusId: STATUS_CONFIRMED,
      //   },
      //   include: [
      //     {
      //       model: User,
      //       as: 'user',
      //     },
      //   ],
      // })

      // const filterOptions = transactions.reduce((acc, curr) => {
      //   return acc
      // }, {})

      return JSON.stringify([])
    }),
    getManualActivations: compose(
      authenticated,
      hasAllowed([5])
    )(async (_, args) => {
      const {
        limit = 10,
        offset = 1,
        sortField = 'createdAt',
        sortDirection = 'DESC',
        where = JSON.stringify({}),
      } = args
      const [model, field] = sortField.split('.')

      const formattedWhere = JSON.parse(where)

      const serializedWhere = Object.keys(formattedWhere).reduce(
        (result, key) => {
          if (key.includes('.')) result[`$${key}$`] = formattedWhere[key]
          else result[key] = formattedWhere[key]

          return result
        },
        {}
      )

      const order =
        model === 'user'
          ? [[{ model: User, as: 'user' }, field, sortDirection]]
          : [[model, sortDirection]]

      const {
        count: totalCount,
        rows: node,
      } = await Transaction.findAndCountAll({
        where: {
          ...serializedWhere,
          typeId: TYPE_MANUAL_ACTIVATION,
          statusId: STATUS_CONFIRMED,
        },
        limit,
        offset: (offset - 1) * limit,
        order,
        include: [
          {
            model: User,
            as: 'user',
          },
        ],
      })

      return {
        edge: {
          node,
        },
        pageInfo: {
          totalCount,
          hasPrevPage: offset > 1,
          hasNextPage: totalCount > limit * offset,
        },
      }
    }),

    getUsersAvailableManualActivationBalance: compose(
      authenticated,
      hasAllowed([5])
    )(async () => {
      // return getAdminsBalanceService.execute()
      // const user = await User.findByPk(2)
      // console.log(user)
      return [{ name: 'admin', id: '2', balance: 100000 }]
    }),
  },

  Mutation: {
    CreateManualActivation: compose(
      authenticated,
      hasAllowed([5])
    )(async (parent, { userId, value, activatorId }, ctx) => {
      if (ctx.user.id !== activatorId) throw new Error('Não Autorizado')
      const floatValue = Number(value)
      if (floatValue < 0.0) throw new Error('Valor não pode ser inferior a $0')

      await CreateCardTransactionService.execute({
        userId,
        value: floatValue,
        cardIdentifier: CREDIT,
        status: CONFIRMED,
        description: 'Ativação de saldo por Administrador',
      })

      // const hash = crypto.randomBytes(16).toString('hex')
      // await Transaction.create({
      //   userId,
      //   value: floatValue,
      //   typeId: TYPE_MANUAL_ACTIVATION,
      //   statusId: STATUS_CONFIRMED,
      //   txid: `activador_id=${activatorId}#${hash}`,
      // })

      return 'Ativação realizada com sucesso'
    }),
  },
}
