import crypto from 'crypto'
import UserUnilevelNode from '../../../shared/infra/sequelize/models/UserUnilevelNode'

class CreateUserUnilevelNodeService {
  async execute({ userId, indicatorId }) {
    const nodeExists = await UserUnilevelNode.findOne({
      where: {
        userId,
      },
    })
    if (nodeExists) throw new Error('Usuário ja possui rede')

    const userCode = crypto
      .randomBytes(16)
      .toString('hex')
      .slice(0, 10)

    const node = await UserUnilevelNode.create({
      userId,
      indicatorId,
      userCode,
    })

    return node
  }
}
export default new CreateUserUnilevelNodeService()
