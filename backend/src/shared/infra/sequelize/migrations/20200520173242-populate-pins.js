module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'pins',
      [
        {
          // 1
          name: 'Prata',
          image_link: 'Prata',
          pontuation: 5000,
          premiation: 150.0,
        },
        {
          // 2
          name: 'Ouro',
          image_link: 'Ouro',
          pontuation: 15000,
          premiation: 350.0,
        },
        {
          // 3
          name: 'Esmeralda',
          image_link: 'Esmeralda',
          pontuation: 35000,
          premiation: 1000.0,
        },
        {
          // 4
          name: 'Diamante',
          image_link: 'Diamante',
          pontuation: 70000,
          premiation: 2000.0,
        },
        {
          // 5
          name: 'Blu-Diamond',
          image_link: 'Blu-Diamond',
          pontuation: 200000,
          premiation: 5000.0,
        },
        {
          // 6
          name: 'Red-Diamond',
          image_link: 'Red-Diamond',
          pontuation: 500000,
          premiation: 20000.0,
        },
        {
          // 7
          name: 'Black-Diamond',
          image_link: 'Black-Diamond',
          pontuation: 1000000,
          premiation: 50000.0,
        },
        {
          // 8
          name: 'Cherman',
          image_link: 'Cherman',
          pontuation: 2500000,
          premiation: 100000.0,
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('pins', null, {})
  },
}
