module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('types_transactions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      name: {
        unique: true,
        allowNull: false,
        type: DataTypes.STRING(80),
      },
      description: {
        allowNull: false,
        type: DataTypes.STRING(80),
      },
      active: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
      },
      is_input: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('types_transactions')
  },
}
