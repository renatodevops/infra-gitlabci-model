import { depositBtcWallet } from '../../../shared/container/index'
import BtcTransaction from '../../../shared/infra/sequelize/models/BtcTransaction'
import { AWAITING_FOR_PAYMENT } from '../../../util/btcTransactionsStatus'

class CreateUserBtcPaymentService {
  async execute({ value, userId }) {
    const [address, amount] = await Promise.all([
      depositBtcWallet.getNewAddress(),
      depositBtcWallet.ExchangeToBTC(value),
    ])

    const message = 'User%20Payment%20Request'
    const label = ''
    const uri = `bitcoin:${address}?amount=${amount.toFixed(
      8
    )}&label=${label}&message=${message}`

    await BtcTransaction.create({
      userId,
      address,
      category: 'receive',
      status: AWAITING_FOR_PAYMENT,
      value,
      amount,
    })

    return { address, value, uri, amount }
  }
}
export default new CreateUserBtcPaymentService()
