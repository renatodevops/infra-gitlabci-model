import { cacheProvider } from '../../../shared/container'
import UserFinancialPassword from '../../../shared/infra/sequelize/models/UserFinancialPassword'
import BCryptHashProvider from '../../../shared/providers/HashProvider/BCryptHashProvider'
import GetUserFinancialPasswordService from './GetUserFinancialPasswordService'

class CreateUserFinancialPasswordService {
  async execute({ userId, password, code }) {
    const key = `user_financial_password:${userId}`
    const emailCode = await cacheProvider.recover(key)
    if (!emailCode) throw new Error('Código Inválido')
    if (emailCode !== code) throw new Error('Código Inválido')

    const hasFinancialpassword = await GetUserFinancialPasswordService.execute({
      userId,
    })
    if (hasFinancialpassword)
      throw new Error('usuário ja possui senha financeira cadastrada')

    const financialPassword = await BCryptHashProvider.generateHash(password)

    await UserFinancialPassword.create({
      userId,
      financialPassword,
    })

    await cacheProvider.invalidate(key)
    return 'Senha financeira criada com sucesso'
  }
}
export default new CreateUserFinancialPasswordService()
