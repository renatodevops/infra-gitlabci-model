<h1 style="text-align:center">Shieldlabs React Frontend Template</h1>

## 💻 Project

Shieldlabs Frontend its a template for ReactJS.

## 🔥 How to start the project

1. Install the dependencies: `yarn`.
2. Start aplication: `yarn start`.
3. View application `http://localhost:3000`.
