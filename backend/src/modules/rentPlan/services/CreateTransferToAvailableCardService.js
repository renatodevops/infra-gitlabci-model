/* eslint-disable import/no-cycle */
import { Op } from 'sequelize'
import CardTransaction from '../../../shared/infra/sequelize/models/CardTransaction'
import { APPLIED, AVAILABLE, VOUCHER } from '../../../util/cardsIdentifiers'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import CreateUserAutoRenovationService from './CreateUserAutoRenovationService'
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'
import GetUserAutoRenovationSetting from './GetUserAutoRenovationSetting'
import LogAvailableInput from '../../../shared/infra/sequelize/models/LogAvailableInput'

class CreateTransferToAvailableCardService {
  get key() {
    return 'CreateTransferToAvailableCardService'
  }

  async handle({ data }) {
    const { userId, transferValue, description } = data

    const hasVoucherEnded = await CardTransaction.findOne({
      where: {
        cardIdentifier: VOUCHER,
        userId,
        status: CONFIRMED,
        value: { [Op.lt]: 0.0 },
      },
    })
    const dateVoucherEnded = hasVoucherEnded
      ? hasVoucherEnded.createdAt
      : new Date('01-01-2020')

    const [totalAvailableInput, totalAppliedInput] = await Promise.all([
      CardTransaction.sum('value', {
        where: {
          cardIdentifier: AVAILABLE,
          userId,
          status: CONFIRMED,
          value: { [Op.gt]: 0.0 },
          createdAt: { [Op.gt]: dateVoucherEnded },
        },
      }),
      CardTransaction.sum('value', {
        where: {
          cardIdentifier: APPLIED,
          userId,
          status: CONFIRMED,
          value: { [Op.gt]: 0.0 },
          createdAt: { [Op.gt]: dateVoucherEnded },
        },
      }),
    ])

    const PROFIT_LIMIT_RATE = 2.5

    const voucherBalance = await GetUserCardBalanceService.execute({
      cardIdentifier: VOUCHER,
      userId,
    })

    const limit = (totalAppliedInput + voucherBalance) * PROFIT_LIMIT_RATE

    const capacity = limit - totalAvailableInput

    const value = capacity > transferValue ? transferValue : capacity

    if (value <= 0.0) return 'Limite de recebimento excedido'

    await CreateCardTransactionService.execute({
      cardIdentifier: AVAILABLE,
      userId,
      status: CONFIRMED,
      description,
      value,
    })

    await LogAvailableInput.create({
      userId,
      appliedInput: totalAppliedInput,
      availableInput: totalAvailableInput,
      limit,
      capacity,
      bonusValue: transferValue,
      paiedValue: value,
    })

    if (capacity <= transferValue) {
      const appliedBalance = await GetUserCardBalanceService.execute({
        cardIdentifier: APPLIED,
        userId,
      })

      if (appliedBalance > 0.0)
        await CreateCardTransactionService.execute({
          cardIdentifier: APPLIED,
          userId,
          status: CONFIRMED,
          description: 'Finalização de locação de cripto ativo',
          value: appliedBalance * -1,
        })
      if (voucherBalance > 0.0)
        await CreateCardTransactionService.execute({
          cardIdentifier: VOUCHER,
          userId,
          status: CONFIRMED,
          description: 'Finalização de voucher',
          value: voucherBalance * -1,
        })

      const isAutoRenovationActive = await GetUserAutoRenovationSetting.execute(
        {
          userId,
        }
      )
      if (isAutoRenovationActive)
        await CreateUserAutoRenovationService.execute({
          userId,
          surplusValue: transferValue - capacity,
          description,
        })
    }

    return value
  }
}

export default new CreateTransferToAvailableCardService()
