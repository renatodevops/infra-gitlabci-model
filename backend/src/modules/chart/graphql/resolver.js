import { Op, col, fn, literal } from 'sequelize'
import { endOfMonth, format, parseISO, startOfMonth } from 'date-fns'
import { formatSlug } from '../../../util/format'

import {
  authenticated,
  compose,
  hasAllowed,
} from '../../../shared/infra/http/composers/composables'

import Transaction from '../../../shared/infra/sequelize/models/Transaction'
import StrategyPlanning from '../../../shared/infra/sequelize/models/StrategyPlanning'
import Strategy from '../../../shared/infra/sequelize/models/Strategy'
import CardTransaction from '../../../shared/infra/sequelize/models/CardTransaction'
import { APPLIED, AVAILABLE, VOUCHER } from '../../../util/cardsIdentifiers'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'

// import AppliedCardBalanceService from '../../balanceCards/services/AppliedCardBalanceService'
// import AvailableCardBalanceService from '../../balanceCards/services/AvailableCardBalanceService'

export default {
  StrategyOperations: {
    strategy: obj => Strategy.findByPk(obj.strategyId),
  },

  Query: {
    getInvestmentProgress: compose(
      authenticated,
      hasAllowed([1, 5])
    )(async (_, args, ctx) => {
      const userId = ctx.user.id

      const PROFIT_LIMIT_RATE = 2.5
      const voucherBalance = await GetUserCardBalanceService.execute({
        userId,
        cardIdentifier: VOUCHER,
      })
      // if (voucherBalance > 0.0) limit = voucherBalance * PROFIT_LIMIT_RATE

      // if(voucherBalance)

      const cardBalance =
        voucherBalance > 0.0
          ? voucherBalance
          : await GetUserCardBalanceService.execute({
              userId,
              cardIdentifier: APPLIED,
            })

      const limit = cardBalance * PROFIT_LIMIT_RATE

      const [lastClosed] = await CardTransaction.findAll({
        where: {
          cardIdentifier: APPLIED,
          status: CONFIRMED,
          userId,
          value: { [Op.gt]: 0.0 },
        },
        order: [['id', 'desc']],
      })

      const lastClosedDate = lastClosed
        ? lastClosed.createdAt
        : new Date('01-01-2020')

      const gainsSinceLastClosed = await CardTransaction.sum('value', {
        where: {
          cardIdentifier: AVAILABLE,
          userId,
          status: CONFIRMED,
          value: { [Op.gt]: 0.0 },
          createdAt: { [Op.gt]: lastClosedDate },
        },
      })

      const progress = (gainsSinceLastClosed * PROFIT_LIMIT_RATE) / limit

      return {
        progress: limit > 0.0 ? (progress * 100).toFixed(2) : 0,
        limit: 250,
        percentage: 0,
      }
    }),

    getOperations: compose(
      authenticated,
      hasAllowed([1, 5])
    )(async (_, { strategyId = 1 }) => {
      const plans = await StrategyPlanning.findAll({
        order: [['date', 'ASC']],
        where: {
          confirmed: true,
          strategyId,
        },
      })

      const operations = plans.reduce((acc, curr, index) => {
        const { date, dailyPlanned } = curr
        if (index === 0) {
          acc.push({
            date,
            value: Number(dailyPlanned),
            accumulated: Number(dailyPlanned),
          })
        } else {
          const { accumulated } = acc[index - 1]
          acc.push({
            date,
            value: Number(dailyPlanned),
            accumulated: accumulated + Number(dailyPlanned),
          })
        }
        return acc
      }, [])

      return {
        strategyId,
        operations,
      }
    }),

    getUserGainsByDate: compose(
      authenticated,
      hasAllowed([3, 4, 5, 1])
    )(async (parent, args) => {
      const { userId } = args

      const today = new Date()
      const startDate = startOfMonth(today)
      const finalDate = endOfMonth(today)

      const transactions = await Transaction.findAll({
        where: {
          [Op.and]: {
            userId,
            typeId: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 16, 17, 19],
            statusId: 2,
            createdAt: {
              [Op.between]: [startDate, finalDate],
            },
          },
        },
        attributes: [
          'type.id',
          'type.name',
          [fn('DATE_TRUNC', 'day', col('Transaction.created_at')), 'date'],
          [fn('sum', col('value')), 'total'],
        ],
        include: [
          {
            association: 'type',
            attributes: ['id', 'name'],
          },
        ],
        group: ['date', 'type.id'],
        order: literal('date ASC'),
      })

      const transactionsMapped = transactions.map(transaction => {
        return {
          name: transaction.type.name,
          slug: formatSlug(transaction.type.name),
          date: format(parseISO(transaction.date.toISOString()), 'yyyy-MM-dd'),
          value: transaction.total,
        }
      })

      const transactonsIndexed = transactionsMapped.reduce((accu, curr) => {
        const { slug, name, value } = curr

        ;(accu[curr.date] = accu[curr.date] || []).push({ slug, name, value })

        return accu
      }, {})

      const transactionsFormatted = Object.keys(transactonsIndexed).map(
        date => ({
          date,
          data: transactonsIndexed[date],
        })
      )

      return transactionsFormatted
    }),

    getGainsByDate: compose(
      authenticated,
      hasAllowed([3, 4, 5])
    )(async (parent, args) => {
      const { typeId, month } = args

      const startDate = new Date(2020, month, 1)
      const finalDate = endOfMonth(startDate)

      const transactions = await Transaction.findAll({
        where: {
          [Op.and]: {
            typeId,
            statusId: 2,
            createdAt: {
              [Op.between]: [startDate, finalDate],
            },
          },
        },
        attributes: [
          [fn('date_trunc', 'day', col('created_at')), 'date'],
          [fn('sum', col('value')), 'total'],
        ],
        group: ['date'],
        order: literal('date ASC'),
      })

      return transactions
    }),
  },
}
