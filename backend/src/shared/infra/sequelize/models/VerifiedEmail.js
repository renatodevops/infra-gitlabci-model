import Sequelize, { Model } from 'sequelize'

class VerifiedEmail extends Model {
  static init(sequelize) {
    super.init(
      {
        email: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'Email field can not be empty',
            },
            isEmail: {
              msg: 'Email field must contain a valid email address',
            },
          },
        },
        code: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'Code field can not be empty',
            },
          },
        },
        checked: Sequelize.BOOLEAN,
        isAdmin: Sequelize.BOOLEAN,
      },
      { sequelize, tableName: 'verified_emails' }
    )
    return this
  }
}
export default VerifiedEmail
