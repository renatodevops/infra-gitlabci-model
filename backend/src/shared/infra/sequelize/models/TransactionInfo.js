import Sequelize, { Model } from 'sequelize'

class TransactionInfo extends Model {
  static init(sequelize) {
    super.init(
      {
        transactionId: {
          type: Sequelize.BIGINT,
          primaryKey: true,
        },
        confirmations: {
          type: Sequelize.INTEGER,
        },
        amount: {
          type: Sequelize.DECIMAL,
        },
        address: {
          type: Sequelize.STRING,
        },
        blockHeight: {
          type: Sequelize.INTEGER,
        },
      },
      { sequelize, tableName: 'transactions_infos' }
    )
    return this
  }

  static associate(models) {
    this.belongsTo(models.Transaction, {
      as: 'transaction',
      foreignKey: 'transactionId',
    })
  }
}
export default TransactionInfo
