import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'
import CreateUserService from '../../signUp/services/CreateUserService'
import CreateResidualBonusCardTransactionService from './CreateResidualBonusCardTransactionService'
// import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'
// import { INCOME } from '../../../util/cardsIdentifiers'

let user

const createUser = async (indicatorId = 1) =>
  CreateUserService.execute({
    name: 'parsedName',
    phone: '(84) 99999-9999',
    document: 'parsedDocument',
    email: `${crypto.randomBytes(6).toString('hex')}@${crypto
      .randomBytes(6)
      .toString('hex')}.com`,
    born: new Date(),
    terms: true,
    indicatorId,
    password: '12345678',
    selfieLink: 'selfieLink',
    documentFrontLink: 'selfieLink',
    documentBackLink: 'selfieLink',
  })

describe('CreateResidualBonusCardTransactionService tests', () => {
  beforeAll(async () => {
    user = await createUser()
  })
  it('should update the income balance to and return 9.6 for user daily income', async () => {
    const result = await CreateResidualBonusCardTransactionService.execute({
      totalDailyIncome: 10,
      userId: user.id,
    })
    expect(result).toHaveProperty('userId', user.id)
    expect(result).toHaveProperty('userDailyIncome', 9.6)
  })

  it('should update the income balance to and return 0.80 for user rate', async () => {
    const upline5 = await createUser(user.id)
    const upline4 = await createUser(upline5.id)
    const upline3 = await createUser(upline4.id)
    const upline2 = await createUser(upline3.id)
    const upline1 = await createUser(upline2.id)
    const direct = await createUser(upline1.id)
    const investor = await createUser(direct.id)

    const result = await CreateResidualBonusCardTransactionService.execute({
      totalDailyIncome: 10,
      userId: investor.id,
    })
    expect(result).toHaveProperty('userId', investor.id)
    expect(result).toHaveProperty('userDailyIncome', 8.0)
  })
})
