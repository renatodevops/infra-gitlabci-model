import { resolve } from 'path'

const tmpFolder = resolve(__dirname, '..', '..', 'tmp')
export default {
  tmpFolder,
  uploadsFolder: resolve(tmpFolder, 'uploads'),
  aws: {
    bucket: process.env.AWS_BUCKET_NAME,
    region: 'us-east-2',
  },
}
