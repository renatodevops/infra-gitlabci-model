import '../../../shared/infra/sequelize/index'
import '../../../shared/providers/QueueProvider/Bee'
import crypto from 'crypto'
import CreateUserService from '../../signUp/services/CreateUserService'
import { CREDIT } from '../../../util/cardsIdentifiers'
import CreateCardTransactionService from './CreateCardTransactionService'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'

let user

const createUser = async (indicatorId = 1) =>
  CreateUserService.execute({
    name: 'parsedName',
    phone: '(84) 99999-9999',
    document: 'parsedDocument',
    email: `${crypto.randomBytes(6).toString('hex')}@${crypto
      .randomBytes(6)
      .toString('hex')}.com`,
    born: new Date(),
    terms: true,
    indicatorId,
    password: '12345678',
    selfieLink: 'selfieLink',
    documentFrontLink: 'selfieLink',
    documentBackLink: 'selfieLink',
  })

describe('CreateResidualBonusCardTransactionService tests', () => {
  beforeAll(async () => {
    user = await createUser()
  })
  it('should not create a transaction and return null', async () => {
    const result = await CreateCardTransactionService.execute({
      cardIdentifier: CREDIT,
      userId: user.id,
      description: 'teste',
      status: CONFIRMED,
      value: undefined,
    })
    expect(result).toBe(null)
  })

  it('should not create a transaction and return null', async () => {
    const result = await CreateCardTransactionService.execute({
      cardIdentifier: CREDIT,
      userId: user.id,
      description: 'teste',
      status: CONFIRMED,
      value: 0,
    })
    expect(result).toBe(null)
  })
  it('should create and return transaction', async () => {
    const result = await CreateCardTransactionService.execute({
      cardIdentifier: CREDIT,
      userId: user.id,
      description: 'teste',
      status: CONFIRMED,
      value: 10,
    })
    expect(result).toHaveProperty('id')
  })
})
