module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'types_financial_infos',
      [
        {
          name: 'Senha Financeira',
          description: 'Senha para transacoes Fiananceiras',
          active: true,
        },
        {
          name: 'Carteira Bitcoin',
          description: 'Bitcoin-Wallet address',
          active: true,
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('types_financial_infos', null, {})
  },
}
