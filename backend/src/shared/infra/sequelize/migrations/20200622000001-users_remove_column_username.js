module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.removeColumn('users', 'username')
    await queryInterface.removeColumn('users', 'country_id')
    await queryInterface.addColumn('users_addresses', 'country_id', {
      allowNull: false,
      type: DataTypes.BIGINT,
      references: {
        model: 'countries',
        key: 'id',
      },
    })
    await queryInterface.addColumn('users', 'selfie_link', {
      allowNull: true,
      type: DataTypes.STRING,
    })
    await queryInterface.addColumn('users', 'document_front_link', {
      allowNull: true,
      type: DataTypes.STRING,
    })
    await queryInterface.addColumn('users', 'document_back_link', {
      allowNull: true,
      type: DataTypes.STRING,
    })
    await queryInterface.addColumn('users_addresses', 'address_proof_link', {
      allowNull: true,
      type: DataTypes.STRING,
    })
  },

  down: async (queryInterface, DataTypes) => {
    await queryInterface.addColumn('users', 'username', {
      allowNull: true,
      type: DataTypes.STRING(24),
      unique: true,
    })
  },
}
