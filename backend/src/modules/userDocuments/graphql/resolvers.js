/* eslint-disable no-unused-vars */
import { Op } from 'sequelize'
import UploadFileService from '../../../shared/providers/StorageProvider/UploadFileService'

import {
  authenticated,
  compose,
  hasAllowed,
  verifyFinancialPassword,
} from '../../../shared/infra/http/composers/composables'

import User from '../../../shared/infra/sequelize/models/User'
import CreateUserDocumentService from '../services/CreateUserDocumentService'
import EvaluateUserDocumentService from '../services/EvaluateUserDocumentService'
import UserDocument from '../../../shared/infra/sequelize/models/UserDocument'
import upload from '../../../config/upload'
import { ADDRESS, DOCUMENTS, SELFIE } from '../../../util/DocumentTypes'

export default {
  Query: {
    GetAllUsersDocumentsToApprove: compose(
      authenticated,
      hasAllowed([3, 4, 5])
    )(async (_, __, ctx) => {
      const { count: totalCount, rows: node } = await User.findAndCountAll({
        order: [['id', 'asc']],
        where: {
          id: { [Op.notIn]: [ctx.user.id, 1] },
        },
        include: [
          {
            association: 'userDocument',
          },
          {
            association: 'address',
          },
        ],
      })

      return node
    }),

    GetUserDocuments: compose(authenticated)(async (_, __, ctx) => {
      const userId = ctx.user.id

      const [
        selfie,
        documentFront,
        documentBack,
        addressProof,
      ] = await Promise.all([
        UserDocument.findOne({ where: { userId, documentType: SELFIE } }),
        UserDocument.findOne({
          where: { userId, documentType: DOCUMENTS.front },
        }),
        UserDocument.findOne({
          where: { userId, documentType: DOCUMENTS.back },
        }),
        UserDocument.findOne({ where: { userId, documentType: ADDRESS } }),
      ])

      return { selfie, documentFront, documentBack, addressProof }
    }),

    GetDocumentsStatus: compose(authenticated)(async (_, __, ctx) => {
      const userId = ctx.user.id

      const documents = await UserDocument.findAndCountAll({
        where: { userId },
      })

      return documents.count < 4
        ? 'missing'
        : documents.rows.some(document => document.approved === false)
        ? 'disapproved'
        : 'approved'
    }),
  },

  Mutation: {
    CreateUserDocuments: compose(
      authenticated,
      hasAllowed([1, 2, 3])
    )(async (_, args, ctx) => {
      const { selfie, documentFront, documentBack, addressProof } = args
      const userId = ctx.user.id

      const [
        selfieLink,
        documentFrontLink,
        documentBackLink,
        addressProofLink,
      ] = await Promise.all([
        UploadFileService.execute(selfie),
        UploadFileService.execute(documentFront),
        UploadFileService.execute(documentBack),
        UploadFileService.execute(addressProof),
      ])

      return CreateUserDocumentService.execute({
        userId,
        selfie: selfieLink,
        documentFront: documentFrontLink,
        documentBack: documentBackLink,
        addressProof: addressProofLink,
      })
    }),

    EvaluateUserDocument: compose(
      authenticated,
      hasAllowed([3, 4, 5])
    )(async (_, args) => {
      const { documentId, approved, message } = args

      return EvaluateUserDocumentService.execute({
        documentId,
        approved,
        message,
      })
    }),
  },
}
