#!/bin/bash

curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash

sudo apt install gitlab-runner

sudo gitlab-runner register
# settings >> CI/CD >> Runners >> disabled shared runners
# COPY https://gitlab.com/
# COPY token
# description = anything
# tag = main
# docker-ssh-machine = SHELL
# Add gitlab-runner in SUDOERS
# sudo vim /etc/sudoers
gitlab-runner  ALL=(ALL)  NOPASSWD: ALL

