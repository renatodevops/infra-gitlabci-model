import BitcoinServer from 'bitcoin-core'
import BigNumber from 'bignumber.js'
import BitcoinUtil from 'blockchain.info'
import { validate } from 'wallet-address-validator'

import bitcoinConfig from '../../../config/bitcoin'

export default class Bitcoin {
  constructor(wallet = null) {
    this.application_name = bitcoinConfig.application
    this.account_name = bitcoinConfig.name
    this.api_key = bitcoinConfig.apikey
    this.min_confirmations = bitcoinConfig.minConfirmations || 3
    if (wallet) bitcoinConfig.connection.wallet = wallet

    this.client = new BitcoinServer(bitcoinConfig.connection)
    this.dollarToRealRate = 1
  }

  async estimateSmartFee(confTarget = 1, estimateMode = 'ECONOMICAL') {
    return this.client.estimateSmartFee(confTarget, estimateMode)
  }

  async generate() {
    return this.client.generate(1)
  }

  async getEstimatedBtcFee(
    txBytesSize = 500.0,
    confTarget = 1,
    estimateMode = 'ECONOMICAL'
  ) {
    const { feerate } = await this.client.estimateSmartFee(
      confTarget,
      estimateMode
    )

    const amount = feerate
      ? ((feerate * txBytesSize) / 1000.0).toFixed(8)
      : '0.00000000'
    return Number(amount)
  }

  getNewAddress() {
    return this.client.getNewAddress()
  }

  getBalance() {
    return this.client.getBalance('*')
  }

  getAddressInfo(input) {
    try {
      return this.client.getAddressInfo(input)
    } catch (err) {
      throw Error(`${err.message}`)
    }
  }

  async getBlock({ blockhash }) {
    try {
      const { hash, confirmations, height } = await this.client.getBlock(
        blockhash
      )
      return {
        hash,
        confirmations,
        height,
      }
    } catch (err) {
      return null
    }
  }

  sendToAddress({
    address,
    amount,
    comment = '',
    commentTo = '',
    subtractFeeFromAmount = true,
    replaceable = false,
    confTarget = 1,
    estimateMode = 'ECONOMICAL',
  }) {
    return this.client.sendToAddress(
      address,
      amount,
      comment,
      commentTo,
      subtractFeeFromAmount,
      replaceable,
      confTarget,
      estimateMode
    )
  }

  async getTransaction(inputTxid) {
    try {
      const {
        amount,
        confirmations,
        txid,
        details,
        blockhash,
      } = await this.client.getTransaction(inputTxid)
      return {
        amount,
        confirmations,
        txid,
        details,
        blockhash,
      }
    } catch (err) {
      return null
    }
  }

  sendMany({
    dummy = '',
    amounts = {},
    minConf = 1,
    comment = '',
    subtractFeeFrom = [],
    replaceable = false,
    confTarget = 1,
    estimateMode = 'ECONOMICAL',
  }) {
    return this.client.sendMany(
      dummy,
      amounts,
      minConf,
      comment,
      subtractFeeFrom,
      replaceable,
      confTarget,
      estimateMode
    )
  }

  ExchangeToBTC(amount) {
    const brlAmount = Number(amount) * this.dollarToRealRate
    return BitcoinUtil.exchange.toBTC(brlAmount, 'USD', {
      apiCode: this.api_key,
    })
  }

  async ExchangeFromBTC(amount) {
    amount = new BigNumber(amount)
    const satoshis = new BigNumber('100000000')

    amount = amount.multipliedBy(satoshis)
    amount = amount.toString()

    const brl = await BitcoinUtil.exchange.fromBTC(amount, 'USD', {
      apiCode: this.api_key,
    })
    return Number(brl) / this.dollarToRealRate
  }

  verifyAddress(address) {
    const valid = validate(address, 'BTC')

    return !!valid
  }
}
