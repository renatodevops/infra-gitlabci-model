/* eslint-disable import/no-cycle */
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'
import { ANNUAL_PLAN, CREDIT } from '../../../util/cardsIdentifiers'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import UserAnnualPlan from '../../../shared/infra/sequelize/models/UserAnnualPlan'
import IndicationBonusService from '../../rentPlan/services/IndicationBonusService'

class CreateAnnualPlanService {
  get key() {
    return 'CreateAnnualPlanService'
  }

  async handle({ data }) {
    const { userAnnualPlanId } = data

    const userPlan = await UserAnnualPlan.findByPk(userAnnualPlanId)

    if (!userPlan) return 'Solicitação não encontrada'

    const { userId, value } = userPlan

    if (userPlan.status !== 'AGUARDANDO ATIVAÇÂO')
      return 'Ativação não pode ser realizada'

    const creditBalance = await GetUserCardBalanceService.execute({
      userId,
      cardIdentifier: CREDIT,
    })
    if (creditBalance < Number(value)) return 'Saldo insuficiente'

    await CreateCardTransactionService.execute({
      cardIdentifier: CREDIT,
      userId,
      status: CONFIRMED,
      description: 'Saída para Plano Anual',
      value: Number(value) * -1,
    })
    const aport = await CreateCardTransactionService.execute({
      cardIdentifier: ANNUAL_PLAN,
      userId,
      status: CONFIRMED,
      description: 'Aporte em Plano Anual',
      value: Number(value),
    })

    await userPlan.update({ cardTransactionId: aport.id })

    IndicationBonusService.execute({
      userId,
      transferValue: Number(value),
    })

    return 'Aporte realizado com sucesso'
  }
}

export default new CreateAnnualPlanService()
