import Sequelize, { Model } from 'sequelize'

class TypeSetting extends Model {
  static init(sequelize) {
    super.init(
      {
        name: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'The name can not be empty',
            },
            len: {
              args: [1, 40],
              msg: 'The name field must contain less than 40 characters',
            },
          },
        },
        description: {
          type: Sequelize.STRING,
          validate: {
            notEmpty: {
              msg: 'The description can not be empty',
            },
            len: {
              args: [1, 80],
              msg: 'The description field must contain less than 80 characters',
            },
          },
        },
        active: Sequelize.BOOLEAN,
      },
      { sequelize, tableName: 'settings' }
    )

    return this
  }
}
export default TypeSetting
