import { createWriteStream } from 'fs'
import crypto from 'crypto'

import uploadConfig from '../../../config/upload'

import S3StorageProvider from './S3StorageProvider'

class UploadFileService {
  async execute(file) {
    if (!file) return null

    const { filename, mimetype, createReadStream } = await file

    if (
      !mimetype.split('/').includes('image') &&
      !mimetype.split('/').includes('pdf')
    )
      throw new Error('Formato inválido de arquivo')

    const fileHash = crypto.randomBytes(10).toString('hex')
    const fileName = `${fileHash}-${filename}`
    await new Promise(res => {
      createReadStream()
        .pipe(createWriteStream(`${uploadConfig.tmpFolder}/${fileName}`))
        .on('close', res)
    })
    const fileLink = await S3StorageProvider.saveFile(fileName)
    return fileLink
  }
}
export default new UploadFileService()
