module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'cards',
      [
        {
          name: 'Voucher',
          identifier: 'VOUCHER',
          is_visible: true,
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('cards', null, {})
  },
}
