resource "digitalocean_droplet" "gitlabci-Frontend" {
  name = "gitlabci-Frontend"
  size = "s-2vcpu-4gb"
  image = var.machine
  region = var.region
  private_networking = true
  vpc_uuid = digitalocean_vpc.vpcgitlabci.id
  ssh_keys = [
    digitalocean_ssh_key.id_rsa.fingerprint
  ]

  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.pvt_key)
    timeout = "2m"
  }

  user_data = file("./scripts/updateServer.sh")
  tags = [digitalocean_tag.infragitlabci.id]
}

# resource "digitalocean_droplet" "gitlabci-Backend" {
#   name = "gitlabci-Backend"
#   size = "s-1vcpu-2gb"
#   image = var.machine
#   region = var.region
#   private_networking = true
#   vpc_uuid = digitalocean_vpc.vpcgitlabci.id
#   ssh_keys = [
#     digitalocean_ssh_key.id_rsa.fingerprint
#   ]

#   connection {
#     host = self.ipv4_address
#     user = "root"
#     type = "ssh"
#     private_key = file(var.pvt_key)
#     timeout = "2m"
#   }

#   user_data = file("./scripts/updateServer.sh")
#   tags = [digitalocean_tag.infragitlabci.id]
# }

# resource "digitalocean_droplet" "gitlabci-Database" {
#   name = "gitlabci-Database"
#   size = "s-1vcpu-1gb"
#   image = var.machine
#   region = var.region
#   private_networking = true
#   vpc_uuid = digitalocean_vpc.vpcgitlabci.id
#   ssh_keys = [
#     digitalocean_ssh_key.id_rsa.fingerprint
#   ]

#   connection {
#     host = self.ipv4_address
#     user = "root"
#     type = "ssh"
#     private_key = file(var.pvt_key)
#     timeout = "2m"
#   }

#   user_data = file("./scripts/updateServer.sh")
#   tags = [digitalocean_tag.infragitlabci.id]
# }

# resource "digitalocean_droplet" "gitlabci-BitcoinCore" {
#   name = "gitlabci-BitcoinCore"
#   size = "s-1vcpu-2gb"
#   image = var.machine
#   region = var.region
#   private_networking = true
#   vpc_uuid = digitalocean_vpc.vpcgitlabci.id
#   ssh_keys = [
#     digitalocean_ssh_key.id_rsa.fingerprint
#   ]

#   connection {
#     host = self.ipv4_address
#     user = "root"
#     type = "ssh"
#     private_key = file(var.pvt_key)
#     timeout = "2m"
#   }

#   user_data = file("./scripts/updateServer.sh")
#   tags = [digitalocean_tag.infragitlabci.id]
# }