module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'settings',
      [
        {
          // 1
          name: 'profile_photo',
          description: 'Foto do perfil do usuário',
          active: true,
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('settings', null, {})
  },
}
