import { endOfMonth, startOfMonth } from 'date-fns'
import { Op } from 'sequelize'
import CardTransaction from '../../../shared/infra/sequelize/models/CardTransaction'
import User from '../../../shared/infra/sequelize/models/User'
import { CONFIRMED } from '../../../util/btcTransactionsStatus'
import { ANNUAL_PLAN, APPLIED } from '../../../util/cardsIdentifiers'
import convertUTCtoLocalTZ from '../../../util/convertUTCtoLOCALTZ'

class GetAllUsersWithDirectIndicatedsMonthlyAportsSumService {
  async execute(month) {
    const users = await User.findAll()

    const usersIds = users.map(user => user.id)

    const usersWithDirects = await Promise.all(
      usersIds.map(async userId => {
        const directs = await User.findAll({
          where: { indicatorId: userId },
        })
        return {
          userId,
          directsIds: directs.map(direct => direct.id),
        }
      })
    )

    const usersWithDirectsMonthlyAportsSum = await Promise.all(
      usersWithDirects.map(async obj => {
        const directsMonthlyAportsSum = await CardTransaction.sum('value', {
          where: {
            cardIdentifier: [APPLIED, ANNUAL_PLAN],
            userId: obj.directsIds,
            status: CONFIRMED,
            createdAt: {
              [Op.and]: [
                { [Op.gte]: convertUTCtoLocalTZ(startOfMonth(month)) },
                { [Op.lt]: convertUTCtoLocalTZ(endOfMonth(month)) },
              ],
            },
          },
        })

        return {
          ...obj,
          directsMonthlyAportsSum,
        }
      })
    )

    return usersWithDirectsMonthlyAportsSum
  }
}
export default new GetAllUsersWithDirectIndicatedsMonthlyAportsSumService()
