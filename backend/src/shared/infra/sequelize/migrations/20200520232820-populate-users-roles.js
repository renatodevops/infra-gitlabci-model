module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'users_roles',
      [
        {
          user_id: 1,
          role_id: 1,
          active: true,
        },
        {
          user_id: 2,
          role_id: 5,
          active: true,
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('users_roles', null, {})
  },
}
