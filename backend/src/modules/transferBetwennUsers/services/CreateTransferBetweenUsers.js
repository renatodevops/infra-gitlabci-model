import User from '../../../shared/infra/sequelize/models/User'

import { AVAILABLE, CREDIT } from '../../../util/cardsIdentifiers'
import { CONFIRMED } from '../../../util/cardsTransactionsStatus'
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'
import CreateCardTransactionService from '../../cards/services/CreateCardTransactionService'

class CreateTransferBetweenUsersService {
  async execute({ sendingUserId, receivingUserId, value }) {
    if (sendingUserId === receivingUserId)
      throw new Error('Não é possivel tranferir para si mesmo')

    const MIN_VALUE = 0.0

    if (value < MIN_VALUE)
      throw new Error(`Valor mínimo para transferência é de ${MIN_VALUE}`)

    const [sendingUser, receivingUser] = await Promise.all([
      User.findByPk(sendingUserId),
      User.findByPk(receivingUserId),
    ])

    if (!sendingUser) throw new Error('Usuário não encontrado')
    if (!receivingUser) throw new Error('Usuário não encontrado')

    // const { balance } = await AvailableCardBalanceService.execute({
    //   userId: sendingUserId,
    // })
    const balance = await GetUserCardBalanceService.execute({
      userId: sendingUserId,
      cardIdentifier: AVAILABLE,
    })

    const TRANSFER_FEE_RATE = 0.0

    const feeValue = TRANSFER_FEE_RATE * value
    const totalValue = value + feeValue

    if (balance < totalValue) throw new Error(`Saldo Insuficiente`)

    // await Transaction.create({
    //   userId: sendingUserId,
    //   value,
    //   typeId: TYPE_TRANSFER_TO_USER,
    //   statusId: STATUS_CONFIRMED,
    // })
    // await AvailableCardBalanceService.updateCache(sendingUserId)
    await Promise.all([
      CreateCardTransactionService.execute({
        cardIdentifier: AVAILABLE,
        userId: sendingUserId,
        description: `Transferência enviada para ${receivingUser.name}`,
        status: CONFIRMED,
        value: value * -1,
      }),
      CreateCardTransactionService.execute({
        userId: receivingUserId,
        value,
        cardIdentifier: CREDIT,
        status: CONFIRMED,
        description: `Transferência recebida de ${sendingUser.name}`,
      }),
    ])

    // await Transaction.create({
    //   userId: receivingUserId,
    //   value,
    //   typeId: TYPE_TRANSFER_FROM_USER,
    //   statusId: STATUS_CONFIRMED,
    // })
    // await CreditCardBalanceService.updateCache(receivingUserId)

    return value
  }
}

export default new CreateTransferBetweenUsersService()
