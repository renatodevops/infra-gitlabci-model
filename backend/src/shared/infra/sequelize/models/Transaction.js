import Sequelize, { Model } from 'sequelize'

class Transaction extends Model {
  static init(sequelize) {
    super.init(
      {
        userId: Sequelize.BIGINT,
        description: Sequelize.STRING,
        typeId: {
          type: Sequelize.BIGINT,
          unique: 'uniqueTag',
        },
        statusId: Sequelize.BIGINT,
        value: Sequelize.DECIMAL,
        fee: Sequelize.DECIMAL,
        txid: {
          type: Sequelize.STRING,
          unique: 'uniqueTag',
        },
        createdAt: Sequelize.DATE,
        date: {
          type: Sequelize.VIRTUAL,
          get() {
            const value = this.getDataValue('date')
            return value
          },
        },
        month: {
          type: Sequelize.VIRTUAL,
          get() {
            const value = this.getDataValue('month')
            return value
          },
        },
        total: {
          type: Sequelize.VIRTUAL,
          get() {
            const value = this.getDataValue('total')
            return Number(value)
          },
        },
        totalBTC: {
          type: Sequelize.VIRTUAL,
          get() {
            const value = this.getDataValue('totalBTC')
            return Number(value)
          },
        },
      },
      { sequelize, tableName: 'transactions' }
    )

    return this
  }

  static associate(models) {
    this.belongsTo(models.TypeTransaction, {
      foreignKey: 'typeId',
      as: 'type',
    })
    this.belongsTo(models.User, { foreignKey: 'userId', as: 'user' })
    this.hasOne(models.TransactionInfo, {
      as: 'info',
      foreignKey: 'transactionId',
    })
  }
}
export default Transaction
