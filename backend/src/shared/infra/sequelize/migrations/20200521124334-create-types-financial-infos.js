module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('types_financial_infos', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: new Date(),
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      name: {
        unique: true,
        allowNull: false,
        type: DataTypes.STRING(40),
      },
      description: {
        allowNull: false,
        type: DataTypes.STRING(80),
      },
      active: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
    })
  },

  down: queryInterface => {
    return queryInterface.dropTable('types_financial_infos')
  },
}
