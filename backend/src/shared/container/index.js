import Bitcoin from '../providers/BtcProvider/bitcoinCore'
import RedisCache from '../providers/CashProvider/RedisCacheProvider'

// export const paymentBtcWallet = new Bitcoin('lokacrypto_payment')
export const depositBtcWallet = new Bitcoin('lokacrypto_deposit')
export const cacheProvider = new RedisCache()
