module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'types_transactions',
      [
        {
          // 28
          name: 'Pagamento de Bônus Residual',
          description: 'some description',
          active: true,
          is_input: false,
        },
        {
          // 29
          name: 'Entrada de Reaplicação',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 30
          name: 'Taxa Administrativa de Rendimento',
          description: 'some description',
          active: true,
          is_input: false,
        },
        {
          // 31
          name: 'Transferência de usuário',
          description: 'some description',
          active: true,
          is_input: true,
        },
        {
          // 32
          name: 'Transferência para usuário',
          description: 'some description',
          active: true,
          is_input: false,
        },
      ],
      {}
    )
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('types_transactions', null, {})
  },
}
