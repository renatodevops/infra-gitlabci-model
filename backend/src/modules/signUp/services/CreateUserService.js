import { cacheProvider } from '../../../shared/container'
import User from '../../../shared/infra/sequelize/models/User'
import UserRole from '../../../shared/infra/sequelize/models/UserRole'
import BCryptHashProvider from '../../../shared/providers/HashProvider/BCryptHashProvider'

const ROLE_USER_ID = 1

class CreateUserService {
  async execute({
    phone,
    born,
    document,
    email,
    name,
    terms,
    password,
    indicatorId,
    selfieLink,
    documentFrontLink,
    documentBackLink,
  }) {
    const parsedName = name.trim()
    if (parsedName.length < 2)
      throw new Error(`Nome deve possuir no mínimo 2 caracteres`)

    const parsedPhone = phone.trim()
    if (parsedPhone.length !== 14 && parsedPhone.length !== 15)
      throw new Error(`Telefone deve possuir 10 ou 11 digitos`)

    const parsedDocument = document.trim()
    if (parsedDocument.length < 5)
      throw new Error(`Documento deve possuir no mínimo 5 caracteres`)

    const parsedEmail = email.trim().toLowerCase()
    const emailExists = await User.findOne({ where: { email: parsedEmail } })
    if (emailExists) throw new Error(`E-mail já esta sendo utilizado`)

    if (password.length < 8)
      throw new Error(`Senha deve ter no mínimo 8 caracteres`)

    const passwordHash = await BCryptHashProvider.generateHash(password)

    const userCreated = await User.create({
      name: parsedName,
      phone: parsedPhone,
      document: parsedDocument,
      email: parsedEmail,
      born,
      terms,
      indicatorId,
      passwordHash,
      selfieLink,
      documentFrontLink,
      documentBackLink,
    })

    await UserRole.create({
      userId: userCreated.id,
      roleId: ROLE_USER_ID,
      active: true,
    })

    const indicator = await User.findByPk(indicatorId)
    const userPath = indicator
      ? `${indicator.userPath}${userCreated.id}.`
      : `.${userCreated.id}.`
    await userCreated.update({ userPath })

    const key = `all_users`
    await cacheProvider.invalidate(key)

    return userCreated
  }
}

export default new CreateUserService()
