import Sequelize, { Model } from 'sequelize'

class GroupsTransaction extends Model {
  static init(sequelize) {
    super.init(
      {
        name: {
          type: Sequelize.STRING,
        },
      },
      { sequelize, tableName: 'groups_transactions' }
    )
    return this
  }
}
export default GroupsTransaction
