import UserRole from '../../../shared/infra/sequelize/models/UserRole'
import Role from '../../../shared/infra/sequelize/models/Role'
import SendFinancialPasswordEmailCodeService from '../services/SendFinancialPasswordEmailCodeService'

import {
  authenticated,
  compose,
  hasAllowed,
} from '../../../shared/infra/http/composers/composables'
import GetUserFinancialPasswordService from '../services/GetUserFinancialPasswordService'
import CreateUserFinancialPasswordService from '../services/CreateUserFinancialPasswordService'
import UserFinancialPassword from '../../../shared/infra/sequelize/models/UserFinancialPassword'
import GetUserIndicationlinkService from '../services/GetUserIndicationlinkService'
import User from '../../../shared/infra/sequelize/models/User'
import UserDocument from '../../../shared/infra/sequelize/models/UserDocument'
import { ADDRESS, DOCUMENTS, SELFIE } from '../../../util/DocumentTypes'
import UserAddress from '../../../shared/infra/sequelize/models/UserAddress'
import Country from '../../../shared/infra/sequelize/models/Country'
import { cacheProvider } from '../../../shared/container'
// import AppliedCardBalanceService from '../../balanceCards/services/AppliedCardBalanceService'

export default {
  UserRole: {
    role: obj => Role.findByPk(obj.roleId),
  },
  UserAddress: {
    country: async obj => {
      const country = await Country.findByPk(obj.countryId)
      return country.name
    },
  },
  User: {
    appliedBalance: async () => {
      return 0
      // const { balance } = await AppliedCardBalanceService.execute({
      //   userId: obj.id,
      // })
      // return balance
    },
    indicator: obj => User.findByPk(obj.indicatorId),
    userRole: obj =>
      UserRole.findAll({
        where: { userId: obj.id, active: true },
      }),
    acceptedDocuments: async obj => {
      const documents = await UserDocument.findAndCountAll({
        where: { userId: obj.id, approved: true },
      })

      if (documents.count < 4) return false

      return true
    },
    address: obj => UserAddress.findOne({ where: { userId: obj.id } }),
    documents: async obj => {
      const [
        selfie,
        documentFront,
        documentBack,
        addressProof,
      ] = await Promise.all([
        UserDocument.findOne({
          where: { userId: obj.id, documentType: SELFIE },
        }),
        UserDocument.findOne({
          where: { userId: obj.id, documentType: DOCUMENTS.front },
        }),
        UserDocument.findOne({
          where: { userId: obj.id, documentType: DOCUMENTS.back },
        }),
        UserDocument.findOne({
          where: { userId: obj.id, documentType: ADDRESS },
        }),
      ])

      return { selfie, documentFront, documentBack, addressProof }
    },
  },
  Mutation: {
    SendFinancialPasswordEmailCode: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) => {
      const userId = ctx.user.id

      return SendFinancialPasswordEmailCodeService.execute({ userId })
    }),
    CreateFinancialPassword: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, { code, batata }, ctx) => {
      const userId = ctx.user.id
      return CreateUserFinancialPasswordService.execute({
        userId,
        code,
        password: batata,
      })
    }),

    resetMyFinancialPassword: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) => {
      const financialPassword = await UserFinancialPassword.findOne({
        where: { userId: ctx.user.id },
      })
      if (financialPassword) await financialPassword.destroy()

      return true
    }),
    ResetCache: async () => {
      await cacheProvider.resetServerCache()

      return 'ok'
    },
  },
  Query: {
    hasFinancialPassword: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) => {
      const userId = ctx.user.id
      const financialPassword = await GetUserFinancialPasswordService.execute({
        userId,
      })
      const hasFinancialPassword = await GetUserFinancialPasswordService.execute(
        { userId }
      )

      if (!hasFinancialPassword)
        SendFinancialPasswordEmailCodeService.execute({
          userId,
        })
      return !!financialPassword
    }),

    GetMyIndicationlink: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) => {
      const userId = ctx.user.id
      const link = await GetUserIndicationlinkService.execute({ userId })

      return link
    }),
  },
}
