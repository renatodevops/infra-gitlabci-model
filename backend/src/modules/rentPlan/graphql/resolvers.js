import {
  authenticated,
  compose,
  hasAllowed,
} from '../../../shared/infra/http/composers/composables'

import BeeQueue from '../../../shared/providers/QueueProvider/Bee'

import UserAutoRenovationSetting from '../../../shared/infra/sequelize/models/UserAutoRenovationSetting'
import { cacheProvider } from '../../../shared/container'
import GetUserCardBalanceService from '../../cards/services/GetUserCardBalanceService'
import { APPLIED, CREDIT, INCOME } from '../../../util/cardsIdentifiers'
import GetUserCardTransactions from '../../cards/services/GetUserCardTransactions'
import GetUserAutoRenovationSetting from '../services/GetUserAutoRenovationSetting'
import CreateRentPlanService from '../services/CreateRentPlanService'

export default {
  Query: {
    getRentCardIncome: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) =>
      GetUserCardBalanceService.execute({
        userId: ctx.user.id,
        cardIdentifier: INCOME,
      })
    ),

    getMyCryptoRentsTransactions: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, args, ctx) => {
      const userId = ctx.user.id

      const userCardsTransactions = await GetUserCardTransactions.execute({
        userId,
      })

      const node = userCardsTransactions.filter(
        transaction => transaction.cardIdentifier === APPLIED
      )

      return {
        edge: {
          node,
        },
        pageInfo: {
          totalCount: userCardsTransactions.length,
          hasPrevPage: false,
          hasNextPage: true,
        },
      }
    }),

    getMyAutoRenovationSetting: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) => {
      const isActive = await GetUserAutoRenovationSetting.execute({
        userId: ctx.user.id,
      })

      return isActive
    }),
  },
  Mutation: {
    CreateCryptoRent: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, __, ctx) => {
      const creditBalance = await GetUserCardBalanceService.execute({
        userId: ctx.user.id,
        cardIdentifier: CREDIT,
      })
      if (creditBalance < 100.0)
        throw new Error('Valor mínimo de locação é 100 LCT')

      await BeeQueue.add(CreateRentPlanService.key, {
        userId: ctx.user.id,
      })
      return 'Verifique seu extrato em instantes para confirmar a operação'
    }),

    UpdateMyAutoRenovationSetting: compose(
      authenticated,
      hasAllowed([1])
    )(async (_, { isActive }, ctx) => {
      const mySetting = await UserAutoRenovationSetting.findOne({
        where: {
          userId: ctx.user.id,
        },
      })
      if (!mySetting) throw new Error('Configuração não existe')

      const updated = await mySetting.update({ isActive })
      const key = `user_renovation_setting:${ctx.user.id}`
      await cacheProvider.invalidate(key)

      return `Renovação automática ${
        updated.isActive ? 'ativada' : 'desativada'
      } com sucesso`
    }),
  },
}
