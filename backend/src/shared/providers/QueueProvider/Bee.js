/* eslint-disable import/no-cycle */
import Bee from 'bee-queue'
import redisConfig from '../../../config/redis'

import CreateRentPlanService from '../../../modules/rentPlan/services/CreateRentPlanService'
import CreateAnnualPlanService from '../../../modules/annnualPlan/services/CreateAnnualPlanService'
import CreateAnnualPlanWithdrawalService from '../../../modules/annnualPlan/services/CreateAnnualPlanWithdrawalService'
import CreateMonthlyIncomeAnnualPlanService from '../../../modules/annnualPlan/services/CreateMonthlyIncomeAnnualPlanService'
import CreateTransferToAvailableCardService from '../../../modules/rentPlan/services/CreateTransferToAvailableCardService'
import CreateDailyIncomeCardTransactionService from '../../../modules/rentPlan/services/CreateDailyIncomeCardTransactionService'
import CreateMonthlyIncomeCardTransactionService from '../../../modules/rentPlan/services/CreateMonthlyIncomeCardTransactionService'
import ActivateVoucher from '../../../modules/voucher/services/ActivateVoucherService'
import ProcessBlocknotificationService from '../../../modules/btc/services/ProcessBlocknotificationService'
import ProcessTxidNotificationService from '../../../modules/btc/services/ProcessTxidNotificationService'
import CreateMonthlyCareearPathPaymentsService from '../../../modules/careearPath/services/CreateMonthlyCareearPathPaymentsService'
import ProcessUsersBtcWithdrawalsService from '../../../modules/btc/services/ProcessUsersBtcWithdrawalsService'

const jobs = [
  ProcessBlocknotificationService,
  ProcessTxidNotificationService,
  ActivateVoucher,
  CreateRentPlanService,
  CreateTransferToAvailableCardService,
  CreateDailyIncomeCardTransactionService,
  CreateMonthlyIncomeCardTransactionService,
  CreateAnnualPlanService,
  CreateAnnualPlanWithdrawalService,
  CreateMonthlyIncomeAnnualPlanService,
  CreateMonthlyCareearPathPaymentsService,
  ProcessUsersBtcWithdrawalsService,
]

class BeeQueue {
  constructor() {
    this.queues = {}

    this.init()
  }

  init() {
    jobs.forEach(({ key, handle }) => {
      this.queues[key] = {
        bee: new Bee(key, {
          redis: redisConfig,
        }),
        handle,
      }
    })
  }

  add(queue, job) {
    return this.queues[queue].bee
      .createJob(job)
      .retries(5)
      .save()
  }

  processQueue() {
    jobs.forEach(job => {
      const { bee, handle } = this.queues[job.key]
      bee
        .on('failed', this.handleFailure)
        .on('succeeded', this.handleSuccess)
        .process(handle)
    })
  }

  handleFailure(job, err) {
    console.log(`Queue ${job.queue.name}: FAILED`, err)
  }

  handleSuccess(job) {
    console.log(`Job ${job.id} succeeded with result: ${job.queue.name}`)
  }
}

export default new BeeQueue()
