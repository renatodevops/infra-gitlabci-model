module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('transactions', 'description', {
      type: Sequelize.STRING,
      allowNull: true,
    })
  },

  down: queryInterface => {
    return queryInterface.removeColumn('transactions', 'description')
  },
}
